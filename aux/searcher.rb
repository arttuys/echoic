require 'open3'

module Searcher

  VALID_SUFFIXES = %w(.mp3 .ogg .m4a .aac)

  $FOUND_TITLES = {}

  def self.process_file(full_fname, output_file)
      puts "Evaluating file #{full_fname}"
      found_title = nil
      found_artist = nil
      stdout, status = Open3.capture2("exiftool", full_fname)

      if status != 0
        puts "Nonzero exit status, skipping"
        output_file.puts "# #{full_fname} - skipped due to exiftool failing to return data"
        return
      end

      title_regex = /\ATitle +: (.+)\z/
      artist_regex = /\AArtist +: (.+)\z/

      stdout.each_line do |line|
        line = line.strip
        #puts "\t\t\tChecking line: #{line}"
        # Break if we already have both
        break if (found_artist != nil && found_title != nil)
        # Test if title matches
        title_match = title_regex.match line
        if title_match != nil
          puts "\t\t\t\tFound title '#{title_match[1]}'!"
          found_title = title_match[1]
          next
        end
        artist_match = artist_regex.match line
        if artist_match != nil
          puts "\t\t\t\tFound artist '#{artist_match[1]}'!"
          found_artist = artist_match[1]
          next
        end
      end

      # No match?
      if found_title == nil || found_artist == nil
        puts "\tSkipping, no metadata"
        output_file.puts "# #{full_fname} - skipped due to missing or incomplete metadata"
        return
      end

      # Check if this name exists already
      compr = found_title + ":" + found_artist
      extdata = $FOUND_TITLES[compr]
      if extdata == nil
        $FOUND_TITLES[compr] = full_fname
      else
        puts "\tMarked as a possible duplicate"
        output_file.puts "\n# #{full_fname} - possible duplicate due to matching title and artist, see file '#{extdata}'"
        output_file.puts "# #{full_fname}|#{found_title}|#{found_artist}\n"
        return
      end

      # Output
      output_file.puts "#{full_fname}|#{found_title}|#{found_artist}"
  end

  def self.enumerate_path(folder, output_file)
    abs_path = File.absolute_path(folder)
    output_file.puts "# #{abs_path}\n"

    puts("Enumerating #{abs_path}")

    found_dirs = []
    Dir.foreach(abs_path) do |np|
      next if np == "." || np == ".."

      joined_path = File.join(abs_path, np)
      if File.directory?(joined_path)
        found_dirs << joined_path
      elsif VALID_SUFFIXES.any? {|sfx| joined_path.downcase.end_with?(sfx)}
        self.process_file(joined_path, output_file)
      end
    end

    found_dirs.each do |found_dir|
      self.enumerate_path(found_dir, output_file)
    end

  end

  def self.main
    if ARGV.length != 2
      raise "Please provide both a start folder and a file"
    end

    start_folder = ARGV[0]
    puts "Enumerating folder #{start_folder}"
    unless File.directory?(start_folder)
      raise "Start directory is not a directory!"
    end

    enum_filename = ARGV[1]
    # Attempt to open a file
    File.open(enum_filename, "w") do |f|
      self.enumerate_path(start_folder, f)
    end
  end

end

Searcher.main