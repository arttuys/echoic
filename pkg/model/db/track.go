package db

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Core track database models and functionality
 */
import (
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/consts"
)

const TrackInsertQueryTemplate = `
	INSERT INTO tracks VALUES (?1, ?2);
`

const FingerprintInsertTemplate = `
	INSERT INTO fingerprints VALUES (?, ?, ?);
`

const TrackSelectTemplate = `
	SELECT DISTINCT t.name, t.artist FROM tracks AS t WHERE t.rowid = ?;
`

// FindTrackForID attempts to locate the applying track from the DB.
// Information presented may be incomplete if error is non-nil
func (sql *FPSQLConn) FindTrackForID(id uint64) (found bool, name string, artist string, err error) {
	// Set defaults
	found = false
	name = ""
	artist = ""
	err = nil
	// First, convert ID to signed
	signedID := UnsignedToSignedInt64(id)
	// Query
	rows, err := sql.Connection.Query(TrackSelectTemplate, signedID)
	if (err != nil) {
		// Error found, return
		return
	}

	defer rows.Close()
	if (!rows.Next()) {
		// Didn't find anything, return
		return
	}

	if err = rows.Scan(&name, &artist); err != nil {
		// Something went wrong whilst scanning, return an error
		return
	}

	// Okay? Mark as found and be done
	found = true
	return
}

// InsertNewTrack() attempts to insert a new track and fingerprints to the database, allowing them to be respectively searched
func (sql *FPSQLConn) InsertNewTrack(name string, artist string, fps map[uint64]([]uint64)) (uint64, error) {

	// First, attempt to begin a transaction
	transaction, err := sql.Connection.Begin()
	if (err != nil) {
		return 0, err
	}

	// First, attempt to insert our basic track value
	insRes, err := transaction.Exec(TrackInsertQueryTemplate, name, artist)
	if (err != nil) {
		// Something went wrong inserting, fail
		transaction.Rollback()
		return 0, err
	}

	rawTrackID, err := insRes.LastInsertId()
	if (err != nil) {
		// Something went wrong getting the inserted ID, roll back
		transaction.Rollback()
		return 0, err
	}

	// Attempt to register fingerprints, one block at a time
	for unsignBlock, unsignFingerprints := range fps {
		timeSlot := UnsignedToSignedInt64(uint64(float64(unsignBlock) * consts.StandardTimeSegmentPerFPBlock * 1000)) // Remember, we store in miliseconds, so multiply by thousand
		for _, fp := range unsignFingerprints {
			signedFp := UnsignedToSignedInt64(fp)
			_, err := transaction.Exec(FingerprintInsertTemplate, signedFp, timeSlot, rawTrackID)
			if (err != nil) {
				transaction.Rollback()
				return 0, err
			}
		}

	}

	// All good? Commit!
	err = transaction.Commit()
	if (err != nil) {
		return 0, err
	}

	return SignedToUnsignedInt64(rawTrackID), nil
}
