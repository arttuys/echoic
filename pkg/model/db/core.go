package db

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Core database models
 */

import "database/sql"
import (
	_ "github.com/mattn/go-sqlite3"
	"log"
)

type FPSQLConn struct {
	Connection *sql.DB
}

/**
	Important observations related to the SQLite3 tables
		- rowids are used, and they need not be declared explicitly. This is a SQLite3-only thing, and needs to be changed if porting to some other database system
		- SQLite3 does NOT support unsigned integer values; therefore, we explicitly translate them into signed values by assuming that the high bit is an integer sign. Be warned that this must be accounted for when doing queries manually; provided wrapper translates them automatically.
			- Fields where this caveat applies are marked on the schema

	Other observations about the data model
		- track numbers are not required to be unique; ideally they should be, but it is perhaps possible to find scenarios where this is too limiting. A zero or negative number should not be rendered
		- track positions are to be stored in miliseconds, effectively multiplying the raw second value with one thousand
 */
const TableInitQuery = `
	CREATE TABLE IF NOT EXISTS track_images(
		track_id INTEGER NOT NULL REFERENCES tracks ON UPDATE CASCADE ON DELETE CASCADE, -- Always positive
		img_str STRING NOT NULL
	);

	CREATE TABLE IF NOT EXISTS tracks(
		name STRING NOT NULL,
		artist STRING NOT NULL
	);

	CREATE TABLE IF NOT EXISTS fingerprints(
		hash INTEGER NOT NULL,  -- Always positive
		track_pos INTEGER NOT NULL,  -- Always positive, stored in miliseconds!
		track_id INTEGER NOT NULL REFERENCES tracks ON UPDATE CASCADE ON DELETE CASCADE  -- Always positive
	);

	-- Indexes

	CREATE INDEX IF NOT EXISTS fingerprints_hash_index ON fingerprints(hash);
	CREATE UNIQUE INDEX IF NOT EXISTS fingerprints_trinity_unique_index ON fingerprints(hash, track_pos, track_id); 
	CREATE INDEX IF NOT EXISTS fingerprints_track_index ON fingerprints(track_id);
	CREATE UNIQUE INDEX IF NOT EXISTS track_image_unique_index on track_images(track_id); 
`

// Returns an signed int out of an unsigned int, without losing information.
// For now, a simple type conversion will do, but this has been separated as a function to avoid ambiguity
func UnsignedToSignedInt64(i uint64) int64 {
	return int64(i)
}

// Returns an unsigned int out of an signed int, without losing information.
// For now, a simple type conversion will do, but this has been separated as a function to avoid ambiguity
func SignedToUnsignedInt64(i int64) uint64 {
	return uint64(i)
}

// OpenAndPrepareSQLiteDB() attempts to open a SQLite database, and if successful, returns a connection object. Raw connection is indeed accessible inside from this object, but one should prefer to use the provided wrapper methods.
func OpenAndPrepareSQLiteDB(filename string) (*FPSQLConn, error) {
	db, err := sql.Open("sqlite3", filename)

	if (err != nil) {
		return nil, err
	}

	// Check that the DB is indeed functional
	if err := db.Ping(); err != nil {
		log.Fatal(err)
		db.Close()
		return nil, err
	}

	// Initiate a transaction that, if necessary, initializes the database schema
	tx, err := db.Begin()
	if (err != nil) {
		db.Close()
		return nil, err
	}

	// Execute the initializing query
	_, err = tx.Exec(TableInitQuery)
	if (err != nil) {
		tx.Rollback()
		db.Close()
		return nil, err
	}

	// Commit..
	err = tx.Commit()
	if (err != nil) {
		db.Close()
		return nil, err
	}

	// Alright, we've now committed a valid transaction, and our DB is certainly in a valid state. Go!
	return &FPSQLConn{db}, nil
}

// Terminates a database Connection. This object is no longer valid to use after this.
func (sqldn *FPSQLConn) Close() error {
	return sqldn.Connection.Close()
}