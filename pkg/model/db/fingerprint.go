package db

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Core fingerprint models and functionality
 */

import (
	"fmt"
	"gitlab.com/arttuys/echoic/pkg/recognizer/linear_correlation"
)

// A template for multi-hash retrieving queries, using string interpolation
const TableQueryTracksForFPTemplate = `
	SELECT DISTINCT fp.track_id, fp.track_pos FROM fingerprints AS fp WHERE fp.hash IN (%v);
`

// Due to the limitations of the SQL language, we must use string interpolation to manually build queries with the required number of parameters. This should be perfectly safe, as we only interpolate question marks and nothing else; there's no chance of a SQL injection taking place.
func generateMultiHashQuery(length uint) string {
	baseStr := ""
	for i := uint(1); i <= length; i++ {
		if (baseStr == "") {
			baseStr = "?"
		} else {
			baseStr += ", ?"
		}
	}

	return fmt.Sprintf(TableQueryTracksForFPTemplate, baseStr)
}


// HashListToMLCPoints() translates a list of found hashes to a list of MLC points, by querying them from the database and generating points for them
func (sql *FPSQLConn) HashListToMLCPoints(x float64, hashes []uint64) ([]linear_correlation.MLCPoints, error) {
	if len(hashes) == 0 {
		return []linear_correlation.MLCPoints{}, nil
	}

	pointMap := make(map[uint64]([]linear_correlation.F64AB))
	generatedQuery := generateMultiHashQuery(uint(len(hashes)))

	// Convert the hashes to an interface array
	intfHashes := make([]interface{}, len(hashes))
	for i, v := range hashes {
		intfHashes[i] = UnsignedToSignedInt64(v)
	}
	// Attempt to first execute the query
	rows, err := sql.Connection.Query(generatedQuery, intfHashes...)
	if (err != nil) {
		// Something happened when querying, return the error
		return nil, err
	}
	defer rows.Close()

	// Now, ingest data per row
	for rows.Next() {
		var trackID int64
		var trackPos int64

		if err := rows.Scan(&trackID, &trackPos); err != nil {
			// Something went wrong whilst scanning, return an error
			return nil, err
		}

		var unsignedTrackID = SignedToUnsignedInt64(trackID)
		var unsignedTrackPos = SignedToUnsignedInt64(trackPos)

		// Good! We have something
		pArr := pointMap[unsignedTrackID]
		if (pArr == nil) {
			pArr = make([]linear_correlation.F64AB, 0)
		}

		pArr = append(pArr, linear_correlation.F64AB{x, float64(unsignedTrackPos) / 1000})
		pointMap[unsignedTrackID] = pArr
	}

	// Alright, we've acquired everything now. Return our results
	resArr := make([]linear_correlation.MLCPoints, 0)
	for k, v := range pointMap {
		resArr = append(resArr, linear_correlation.MLCPoints{k, v})
	}

	return resArr, nil
}
