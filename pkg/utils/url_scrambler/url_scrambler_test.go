package url_scrambler

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	URL scrambler tests

 */

import (
	"testing"
	"math/rand"
)

func TestAll(t *testing.T) {
	for range [100]int{} {
		key := [4]uint32{rand.Uint32(), rand.Uint32(), rand.Uint32(), rand.Uint32() % (1 << 12)}
		invalidKey := [4]uint32{rand.Uint32(), rand.Uint32(), rand.Uint32(), (1 << 13) + (rand.Uint32() % (1 << 12))}

		if (key == invalidKey) {
			t.Fatal("Test for URLScrambler broken, two random keys equivalent")
		}

		id := rand.Uint64()

		// Generate a string
		str := IdentifierToScrambledString(key, id)
		// And back again, both with a valid and an invalid key
		id1, err1 := ScrambledStringToIdentifier(key, str)
		id2, err2 := ScrambledStringToIdentifier(invalidKey, str)

		if (err1 != nil || err2 != nil) {
			t.Fatal("Valid strings errored")
		}

		if (id != id1 || id == id2) {
			t.Fatal("Decryption did not work properly")
		}

	}

	// Finally, attempt a faulty decode
	_, err := ScrambledStringToIdentifier([4]uint32{0, 1, 2, 3}, "Totally Invalid!")
	if (err == nil) {
		t.Fatal("Invalid string did not error!")
	}
}
