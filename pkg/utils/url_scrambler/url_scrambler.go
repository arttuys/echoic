package url_scrambler

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*

	 URL Scrambler - decode and encode given identifiers. For speed, this
	 is implemented using the XTEA algorithm, which should provide sufficient
	 security for this use-case; it should prevent against casual
	 onlookers only, not from motivated attackers. And be fast!

 */

import (
	"strconv"
	"strings"
)

// NumRounds specifies the amount of XTEA rounds done
const NumRounds = 32

// Delta specifies the delta constant required by XTEA
const Delta uint32 = 0x9E3779B9

// NumRoundsDelta specifies a helper constant for XTEA decryption, calculated as follows: (NumRounds * Delta) % (2 pow 32)
const NumRoundsDelta uint32 = 3337565984

// IdentifierToScrambledString transforms an unsigned 64bit identifier into a base 36 string, encrypted using XTEA
func IdentifierToScrambledString(key [4]uint32, identifier uint64) (string) {
	// Disjoint the identifier into two 32-bit units, little-endian order
	var i0, i1, sum uint32 = uint32(identifier), uint32(identifier>>32), 0
	for i := 0; i < NumRounds; i++ {
		// This is safe, Golang language specification indicates that wrap-around is allowed and ordinary behavior
		i0 += (((i1 << 4) ^ (i1 >> 5)) + i1) ^ (sum + key[sum&3])
		sum += Delta
		i1 += (((i0 << 4) ^ (i0 >> 5)) + i0) ^ (sum + key[(sum>>11)&3])
	}

	return strings.ToUpper(strconv.FormatUint(uint64(i1)<<32+uint64(i0), 36))
}

// ScrambledStringToIdentifier attempts to decode a scrambled string into an identifier using a provided key; it may fail if the string does not decode into a valid 64bit unsigned integer
func ScrambledStringToIdentifier(key [4]uint32, scrambled string) (uint64, error) {
	// Try to first decode our string
	encrypted, decodeErr := strconv.ParseUint(strings.ToLower(scrambled), 36, 64)
	if decodeErr != nil {
		return 0, decodeErr
	}

	var i0, i1, sum = uint32(encrypted), uint32(encrypted>>32), NumRoundsDelta
	for i := 0; i < NumRounds; i++ {
		i1 -= (((i0 << 4) ^ (i0 >> 5)) + i0) ^ (sum + key[(sum>>11)&3])
		sum -= Delta
		i0 -= (((i1 << 4) ^ (i1 >> 5)) + i1) ^ (sum + key[sum&3])
	}

	return uint64(i1)<<32 + uint64(i0), nil
}
