package tempfile

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	TempFileWithExtension() - similar as to the ioutil function, but with the specific feature of allowing extensions to be selected, along with some smaller changes as well
 */

import (
	"os"
	"sync"
	"time"
	"strconv"
	"path/filepath"
)

var rnd uint32
var rngMutex sync.Mutex


func reseedRng() uint32 {

	return uint32(time.Now().UnixNano() + int64(os.Getpid()))

}

func NextID() string {
	// Lock the RNG mutex
	rngMutex.Lock()

	// Prepare a temporary variable for a new random
	nr := rnd
	if (nr == 0) {
		nr = reseedRng()
	}

	// See "Numerical Recipes in C", chapter 7, page 284; this has been examined and tested, and should be safe to use as a fast, non-cryptographic LCG RNG
	nr = nr*1664525 + 1013904223
	rnd = nr
	rngMutex.Unlock()
	return strconv.FormatUint(uint64(rnd), 36) // Remove minus sign if applicable
}

// TempFileWithExtension works similarly as the ioutil TempFile() - and is admittedly very similar code-wise as well; the only substantial difference is that
// this method allows an extension to be also specified, and some changed constants
func TempFileWithExtension(dir string, prefix string, ext string) (f *os.File, err error) {
	// No dir specified, add OS temp dir
	if (dir == "") {
		dir = os.TempDir()
	}

	// Extension specified, add it
	if (ext != "") {
		ext = "." + ext
	}

	conflict := 0
	for i := 0; i < 5000; i++ { // This should be fairly enough; if we can't get it to work even with this amount, then something is broken!
		name := filepath.Join(dir, prefix+NextID()+ext)
		f, err = os.OpenFile(name, os.O_RDWR | os.O_CREATE | os.O_EXCL, 0600) // Observe that this is an octal literal!
		if (os.IsExist(err)) {
			// This exists already. Let's try again, but before..
			if conflict++; conflict > 20 { // Reseeding is expensive, avoid unless it starts to seem like we can't get an open file this way
				rngMutex.Lock()
				reseedRng()
				rngMutex.Unlock()
			}
			continue
		}

		// Either we have a result, or we have an error. Break out of the loop
		break
	}
	return
}