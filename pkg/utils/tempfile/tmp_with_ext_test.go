package tempfile

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Tests for temporary files with extensions
 */

import (
	"testing"
	"fmt"
	"os"
	"strings"
	"path/filepath"
)

// Test that the RNG does not repeat itself at least for 1000 items / a.k.a: at least 1000 different items can exist
func TestNextID(t *testing.T) {
	foundMap := make(map[string]bool)
	for i := 0; i < 1000; i++ {
		id := NextID()
		if (foundMap[id] == true) {
			t.Fatal("ID already found!")
		} else {
			foundMap[id] = true
		}
	}

	fmt.Printf("Sample: '%v'\n", NextID())
}

// Attempt to create a file
func TestTempFileWithExtension(t *testing.T) {
	f, err := TempFileWithExtension("", "abc", "dat")
	if (err != nil) {
		t.Fatal("Failed to create a file!")
	}

	fmt.Printf("Found file: %v", f.Name())
	// Erase it once it is done
	defer os.Remove(f.Name())

	// Retrieve the filename without path
	basename := filepath.Base(f.Name())
	if !(strings.HasPrefix(basename, "abc")) || !(strings.HasSuffix(basename, ".dat")) {
		t.Fatal("Improperly named file!")
	}
}