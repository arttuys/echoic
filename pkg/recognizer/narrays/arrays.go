package narrays

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Simple complex N-dimensional array library, intended to be used with the FFTW libraries but perhaps for other generic storage too
 */

import (
	"reflect"
	"errors"
)

// An N-dimensional array; from 1 to N dimensions
type ComplexArrayN struct {
	N		 []uint
	Elements []complex128
}

// A simple, whole number XY position
type XY struct {
	X uint
	Y uint
}

// Len() returns the product of lengths of all dimensions; this is a simple length in case of 1-dimensional arrays
func (a *ComplexArrayN) Len() uint {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}

	var tsize uint

	tsize = 1
	for _, dimsize := range a.N {
		tsize = SafeMult(tsize, dimsize)
	}

	return tsize
}

// Dim() returns the amount of dimensions in this array
func (a *ComplexArrayN) Dims() int {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}

	return len(a.N)
}

// Set() sets a value at a given set of indexes, or errors if the indexes do not resolve into a valid location. An incomplete index may be given, in which case the rest are assumed to be zero
func (a* ComplexArrayN) Set(indexes []uint, val complex128) error {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}

	rawInd, err := a.RawIndex(indexes)
	if (err != nil) {
		return err
	}
	a.Elements[rawInd] = val
	return nil
}

// Get() gets a value at a given set of indexes, or errors if they do not resolve into a valid location. An incomplete index may be given, in which case rest of the indexes are assumed to be zero
func (a* ComplexArrayN) Get(indexes []uint) (complex128, error) {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}

	rawInd, err := a.RawIndex(indexes)
	if (err != nil) {
		return 0, err
	}
	return a.Elements[rawInd], nil
}

// DifferentialN() calculates the length of the dimensions; that is, how many elements of the array one of these indexes will contain. By consequence, the last dimension will always have length of 1, as it indicates a single item.
func (a *ComplexArrayN) DifferentialN() []uint {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}

	var diffInds = make([]uint, len(a.N))
	diffInds[len(diffInds)-1] = 1

	// Start from the second to last
	for i := len(diffInds) - 2; i >= 0; i-- {
		diffInds[i] = SafeMult(diffInds[i+1], a.N[i+1])
	}

	return diffInds
}

// Join() joins several complex arrays to a N+1 array, where the new first dimension's size matches the length of the provided list. All arrays must be of the same exact shape, otherwise it is not possible to join them
func Join(arrays []*ComplexArrayN) (*ComplexArrayN, error) {
	if (arrays == nil || (len(arrays) == 0)) {
		return nil, errors.New("arrays to join must be provided")
	}

	first := arrays[0]
	if (first == nil) {
		return nil, errors.New("no array may be nil")
	}
	newDim := append([]uint{uint(len(arrays))}, first.N...)
	var resultArr []complex128
	resultArr = append(resultArr, first.Elements...)
	for i := 1; i < len(arrays); i++ {
		if (arrays[i] == nil) {
			return nil, errors.New("no array may be nil")
		}
		if !reflect.DeepEqual(first.N, arrays[i].N) {
			return nil, errors.New("arrays to be joined must be of same shape")
		}

		resultArr = append(resultArr, arrays[i].Elements...)
	}

	// All our arrays have a valid shape
	var narr ComplexArrayN
	narr.N = newDim
	narr.Elements = resultArr

	return &narr, nil
}
// RawIndex() returns the raw index in the array, which contains the desired item
func (a *ComplexArrayN) RawIndex(indexes []uint) (uint, error) {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}

	if !a.indexArrValid(indexes) {
		return 0, errors.New("indexes nonexistent, too long or out of range")
	}

	var index uint = 0

	// Determine the differential indexes
	var diffIndexes = a.DifferentialN()

	for i := 0; i < len(indexes); i++ {
		index += indexes[i] * diffIndexes[i]
	}

	return index, nil
}


func (a *ComplexArrayN) checkValidOneDim() bool {
	return len(a.N) == 1
}

// Append() is the more complicated relative of AppendSimple(). It expects a pointer to some unknown data structure, count of items, and a translation function
// The translation function is expected to provide complex values for whatever values the data structure contains

// This function is all-or-nothing; if the translation function panics, the data structure will not be modified in any way.
func (a *ComplexArrayN) Append(data interface{}, n uint, translt func(interface{}, uint) complex128) error {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}
	if (!a.checkValidOneDim()) {
		return errors.New("the dataset must be one-dimensional for applying data directly!")
	}
	if (data == nil || translt == nil) {
		return errors.New("cannot append with a nil array!")
	}
	if (n < 1) {
		return nil
	}

	// Special case, we  should perhaps use append() here, as it will automatically allocate excess capacity, and make this operation much faster
	if (n == 1) {
		newData := append(a.Elements, translt(data, 0))
		a.Elements = newData
		a.N[0] += 1
		return nil
	}

	// Right, let's first create the new structure
	offset := uint(len(a.Elements))
	newData := make([]complex128, offset + n)

	// Paste our existing data

	for i, v := range a.Elements {
		newData[i] = v
	}

	// Ingest new data
	for i := uint(0); i < n; i++ {
		newData[i + offset] = translt(data, i)
	}

	// OK? Assign what we want
	newLen := uint(len(newData))
	a.Elements = newData
	a.N[0] = newLen

	return nil
}

// If our data array is one-dimensional, we can append it to the end of the datastream all nicely. This simply pastes the data to the end of the stream
func (a *ComplexArrayN) AppendSimple(data *[]complex128) error {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}
	if (!a.checkValidOneDim()) {
		return errors.New("the dataset must be one-dimensional for applying data directly!")
	}
	if (data == nil) {
		return errors.New("cannot append with a nil array!")
	}
	if (len(*data) < 1) {
		return nil
	}

	// Since our data is already in a complex128 format, it is relatively easy to do this.
	a.Elements = append(a.Elements, (*data)...)
	a.N[0] += uint(len(*data))

	return nil
}

// indexArrValid checks if the subarray of indexes could be potentially valid; must not be too long, and none of the indexes can be too large
func (a *ComplexArrayN) indexArrValid(indexes []uint) bool {
	if (indexes == nil) {
		return false // Nil indexes are obviously not valid
	}

	// Check all indexes are valid
	if len(indexes) > len(a.N) {
		return false
	}

	for i := 0; i < len(indexes); i++ {
		if indexes[i] >= a.N[i] {
			return false // Too large
		}
	}

	return true
}

// Slice1D() is a simplified slice operation, which, instead of dimensional slicing, takes a subslice of some 1D array.
func (a *ComplexArrayN) Slice1D(start uint, len uint) (*ComplexArrayN, error) {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}
	if (!a.checkValidOneDim()) {
		return nil, errors.New("the dataset must be one-dimensional for slicing!")
	}
	if (len == 0) {
		return nil, errors.New("must select a nonzero length")
	}
	if (start+len > a.N[0]) {
		return nil, errors.New("slice would go out of range")
	}

	// Ok, this should work
	var newElems []complex128 = make([]complex128, len)
	for i, _ := range newElems {
		newElems[i] = a.Elements[start + uint(i)]
	}

	var narr ComplexArrayN
	narr.Elements = newElems
	narr.N = []uint{len}

	return &narr, nil
}


// Slice() returns a slice of the array. The type of array returned is determined by the list of indexes; each given index slices a single part from the respective dimension, in row-major order. If the amount of given indexes is zero, a copy of the array is returned
// If the amount of indexes match to the number of dimensions, an one-dimensional, N=1 unit will be returned.
//
// If any of the indexes are larger than their respective dimensions or there are more indexes than dimensions, this function will panic
func (a *ComplexArrayN) Slice(indexes []uint) (*ComplexArrayN, error) {
	if (a == nil) {
		panic("Attempted to use a method on a nil array")
	}

	if !a.indexArrValid(indexes) {
		return nil, errors.New("index array too long, or some of the indexes go outside their respective dimensions")
	}

	if len(indexes) == 0 {
		var narr ComplexArrayN
		narr.N = make([]uint, len(a.N))
		copy(narr.N, a.N)
		narr.Elements = make([]complex128, len(a.Elements))
		copy(narr.Elements, a.Elements)
		return &narr,nil
	}

	// If we have provided all indexes, create a single index element
	if len(indexes) == len(a.N) {
		narr, _ := NewArrayN([]uint{1}) // Shoud not fail, perfectly valid
		finalInd, _ := a.Get(indexes)   // If the index arr is valid as verified above, this should be OK as well
		narr.Elements[0] = finalInd
		return narr, nil
	}

	// OK, we only want a part. Get the differential N's
	diffN := a.DifferentialN()
	startIndex, _ := a.RawIndex(indexes) // Indexes have been already verified as valid, should not fail

	length := diffN[len(indexes)-1]
	endIndex := startIndex + length - 1

	// Slice a part of the dimensions
	dimSlice := append([]uint{}, a.N[(len(indexes)):len(a.N)]...)

	// Since we take a slice of valid dimensions, this should always pass
	narr, _ := NewArrayN(dimSlice)

	copy(narr.Elements, a.Elements[startIndex:endIndex+1])
	return narr, nil
}

// NewArrayN() initializes a new ComplexArrayN, essentially creating the correctly sized data structures
func NewArrayN(n []uint) (*ComplexArrayN, error) {
	var arr ComplexArrayN
	var tsize uint
	// Calculate the appropriate size
	tsize = 1
	if (n == nil || len(n) == 0) {
		return nil, errors.New("must specify a nonempty dimension")
	}
	for _, dimsize := range n {
		if (dimsize == 0) {
			return nil, errors.New("dimension size 0 given, not valid")
		}
		tsize = SafeMult(tsize, dimsize)
	}
	arr.Elements = make([]complex128, tsize)
	arr.N = make([]uint, len(n))
	copy(arr.N, n)
	return &arr, nil
}

// SafeMult() checks if a multiplication is possible without overflow; if not, it will panic
func SafeMult(a, b uint) uint {
	if a <= 1 || b <= 1 {
		return a * b // Unable to overflow
	}

	c := a * b
	if (c / b != a) {
		panic("Multiplication overflowed in safeMult")
	}

	return c
}