package narrays

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Tests for complex N-dimensional arrays
 */

import (
	"testing"
	"reflect"
	"fmt"
)

// Basic operations
func TestArrayBasics(t *testing.T) {
	invalidValues := []([]uint){nil, {}, {2, 4, 2, 0}}
	for _, v := range invalidValues {
		_, err1 := NewArrayN(v)

		if (err1 == nil) {
			t.Fatal("Errors not thrown properly for invalid creation")
		}
	}

	basicArray, _ := NewArrayN([]uint{4,6,8,5,3,1,1,2})
	if (basicArray.Len() != 5760 || basicArray.Dims() != 8) {
		t.Fatal("Array instantiated with bad dimensions")
	}
}

// Join
func TestArrayJoin(t *testing.T) {
	basicArray, _ := NewArrayN([]uint{2,2})
	// Attempt basic invalid joins
	invalidJoins := []([]*ComplexArrayN){nil, {nil}, {}, {basicArray, nil}}
	for _, v := range invalidJoins {
		res, err := Join(v)
		if (res != nil || err == nil) {
			t.Fatal("Invalid join led to a valid result")
		}
	}

	// Let's try a baseline case
	res, err := Join([]*ComplexArrayN{basicArray})
	if (res == nil || err != nil) {
		t.Fatal("Valid single join failed!")
	}

	if (!reflect.DeepEqual(res.Elements, []complex128{0,0,0,0}) || !reflect.DeepEqual(res.N, []uint{1,2,2})) {
		t.Fatal("Invalid, nonstandard join created!")
	}

	// Let's try wonkily shaped ones
	anotherArray, _ := NewArrayN([]uint{1,2})
	res, err = Join([]*ComplexArrayN{basicArray, anotherArray})
	if (res != nil || err == nil) {
		t.Fatal("Allowed to join misshaped arrays!")
	}

	// Now, let's see how this merges
	yetAnotherArray, _ := NewArrayN([]uint{2,2})
	yetAnotherArray.Elements = []complex128{1,2,3,4}

	res, err = Join([]*ComplexArrayN{yetAnotherArray,basicArray,yetAnotherArray})
	if (res == nil || err != nil) {
		t.Fatal("Valid nontrivial join failed!")
	}

	if (!reflect.DeepEqual(res.Elements, []complex128{1,2,3,4,0,0,0,0,1,2,3,4}) || !reflect.DeepEqual(res.N, []uint{3,2,2})) {
		t.Fatal("Invalid, nonstandard join created!")
	}
}

// Slices
func TestSlicing(t *testing.T) {
	basicArray, _ := NewArrayN([]uint{2,1,2})
	invalidValues := []([]uint){nil, {2, 0, 1}, {1, 0, 0, 3}}
	for _, v := range invalidValues {
		_, err1 := basicArray.Slice(v)

		if (err1 == nil) {
			t.Fatal("Errors not thrown properly for invalid creation")
		}
	}

	// Check that slices are actual copies, even in case of zero slices
	copiedArray, err := basicArray.Slice([]uint{})
	if (copiedArray == nil || err != nil) {
		t.Fatal("Single-unit copy failed")
	}
	if (&copiedArray == &basicArray || &copiedArray.N == &basicArray.N || &copiedArray.Elements == &basicArray.Elements) {
		t.Fatal("Non-genuine copy for Slice")
	}
	if (!reflect.DeepEqual(copiedArray.N, basicArray.N) || !reflect.DeepEqual(copiedArray.Elements, basicArray.Elements)) {
		fmt.Println(copiedArray.N)
		fmt.Println(basicArray.N)
		fmt.Println(copiedArray.Elements)
		fmt.Println(basicArray.Elements)
		t.Fatal("Mismatching contents for copies")
	}

	// Next, make a zero slice
	basicArray.Elements = []complex128{9,3,2,5}
	newSlice, _ := basicArray.Slice([]uint{1,0,0})
	if (!reflect.DeepEqual(newSlice.N, []uint{1}) || !reflect.DeepEqual(newSlice.Elements, []complex128{2})) {
		t.Fatal("Improper slicing!")
	}
	// Compare to how an incomplete slice behaves
	newSlice2, _ := basicArray.Slice([]uint{1})
	if (!reflect.DeepEqual(newSlice2.N, []uint{1,2}) || !reflect.DeepEqual(newSlice2.Elements, []complex128{2,5})) {
		t.Fatal("Improper slicing!")
	}
	// Compare to how an incomplete slice behaves
	newSlice3, _ := basicArray.Slice([]uint{1,0})
	if (!reflect.DeepEqual(newSlice3.N, []uint{2}) || !reflect.DeepEqual(newSlice3.Elements, []complex128{2,5})) {
		t.Fatal("Improper slicing!")
	}

	// How about a more nontrivial case?
	basicArray2, _ := NewArrayN([]uint{3,1,2,4,1,2}) // 48
	for i := uint(1); i <= basicArray2.Len(); i++ {
		basicArray2.Elements[i-1] = complex(float64(i), 0)
	}
	// Slice it!
	newSlice4, _ := basicArray2.Slice([]uint{2,0,1})
	if (!reflect.DeepEqual(newSlice4.N, []uint{4,1,2}) || !reflect.DeepEqual(newSlice4.Elements, []complex128{41,42,43,44,45,46,47,48})) {
		t.Fatal("Improper slicing (NS4)!")
	}
}

// Get() / Set()
func TestArrayGetSet(t *testing.T) {
	arr, err := NewArrayN([]uint{3,1,3})
	if (err != nil) {
		t.Fatal("Was unable to create an array")
	}

	if (!reflect.DeepEqual([]complex128{0,0,0,0,0,0,0,0,0}, arr.Elements)) {
		t.Fatal("Bad state, not zero-initialized")
	}

	// Attempt to set to a zero index or some other borked type
	invalidValues := []([]uint){nil, {1, 0, 2, 1}, {0, 1, 0}}
	for _, v := range invalidValues {
		_, err1 := arr.Get(v)
		err2 := arr.Set(v, 3)

		if (err1 == nil || err2 == nil) {
			t.Fatal("Errors not thrown properly for invalid set/get")
		}
	}

	// Right, invalid indexes do not work. Try setting
	arr.Set([]uint{}, 3) // Sets effectively at position zero
	arr.Set([]uint{1,0,2}, 6) // 1 * 1*3 = 3, +2 = 5

	if (!reflect.DeepEqual([]complex128{3,0,0,0,0,6,0,0,0}, arr.Elements)) {
		t.Fatal("Bad state, not zero-initialized")
	}

	res, err:= arr.Get([]uint{0})
	if (res != 3 || err != nil) {
		t.Fatal("Getting failed!")
	}
}

// Tests complex array appends
func TestArrayAppendComplex(t *testing.T) {
	arr, _ := NewArrayN([]uint{1})
	badArr, _ := NewArrayN([]uint{3,2})

	appendable := []float64{7,8,2,9,3,2,1} // Length is 7, but let's just append 3
	appendInterface := interface{}(appendable)
	trs := func(data interface{}, n uint) complex128 {return complex((data).([]float64)[n]+1, 0)}

	if err := badArr.Append(appendInterface, 3, trs); err == nil {
		t.Fatal("Invalid append did not return an error!")
	}

	if err := arr.Append(nil, 3, trs); err == nil {
		t.Fatal("Invalid append did not return an error!")
	}

	if err := arr.Append(appendInterface, 3, trs); err != nil {
		t.Fatal("Valid append failed with error: " + err.Error())
	}


	// Check validity
	if ((!reflect.DeepEqual(arr.Elements, []complex128{0,8,9,3})) || (!reflect.DeepEqual(arr.N, []uint{4}))) {
		fmt.Printf("arr: %v\nN: %v\n", arr.Elements, arr.N)
		t.Fatal("Append applied incorrectly!")
	}
}

// Tests simple array appends
func TestArrayAppendSimple(t *testing.T) {
	arr, _ := NewArrayN([]uint{1})
	badArr, _ := NewArrayN([]uint{3,2})

	// We can safely establish it is zero-initialized

	appendable := []complex128{4,3,1,0}
	if err := badArr.AppendSimple(&appendable); err == nil {
		t.Fatal("Invalid append did not return an error!")
	}

	if err := arr.AppendSimple(nil); err == nil {
		t.Fatal("Invalid append did not return an error!")
	}

	if err := arr.AppendSimple(&appendable); err != nil {
		t.Fatal("Valid append failed with error: " + err.Error())
	}

	// Zero length append should have no effect
	if err := arr.AppendSimple(&[]complex128{}); err != nil {
		t.Fatal("Valid append failed with error: " + err.Error())
	}


	//

	if ((!reflect.DeepEqual(arr.Elements, []complex128{0, 4,3,1,0})) || (!reflect.DeepEqual(arr.N, []uint{5}))) {
		fmt.Printf("arr: %v\nN: %v\n", arr.Elements, arr.N)
		t.Fatal("Append applied incorrectly!")
	}
}