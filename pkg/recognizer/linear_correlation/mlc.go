package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	MLC / Multi-Line Correlator main module; this should be used to find correlations in a set of points belonging to N different potential lines
 */

import (
	"errors"
	"time"
	"strings"
	"sort"
)

// Structure for found identifiers

type MLCFoundIdentifiers struct {
	Matches []MLCMatch
}

// Match structure

type MLCMatch struct {
	ID    uint64
	Score uint
}

// Result code type
type MLCResultCode uint


// Result codes
const (
	// User requested a conclusion, and with this return code, a list of valid results are provided. This can be only signaled once
	MLCRequestedResultExit MLCResultCode = iota
	// A strong result has been received, via the result channel, and user can now collect meaningful results by forcing a conclusion. This may be signaled multiple times.
	MLCStrongResultSignal MLCResultCode = iota
	// Timeout has been signaled by some SLC, or a midstep timeout expired. MLC is still running, but may not return appropriate results. This may be signaled multiple times.
	MLCTimeoutSignal MLCResultCode = iota
	// An error has been signaled by some SLC or otherwise caught. MLC is still running, but may not return appropriate results. This may be signaled multiple times.
	MLCErrorSignal MLCResultCode = iota
	// Some channel transaction stalled, and the MLC was required to exit asynchronously. This can be only signaled once.
	MLCAsynchTimeoutExit MLCResultCode = iota
	// A panic has been caught or otherwise an error has forced the MLC to exit asynchronously. This can be only signaled once.
	MLCAsynchErrorExit MLCResultCode = iota
)


// Return structure
type MLCReturnSignal struct {
	ResultCode MLCResultCode
	Matches    *MLCFoundIdentifiers
}

// Bundle structure for the runner; contains all necessary data to set up a MLC
type MLCData struct {
	controlChannel      (chan MLCChOpCode)
	ingestChannel       (chan MLCPoints)
	returnChannel       (chan MLCReturnSignal)
	midstepTimeout      time.Duration
	slcTimeout          time.Duration
	globalTimeout       time.Duration
	slcIngestBufferSize uint
	resControlBufSize   uint
	minPoints           F64AB
	minRange            F64AB
	ave                 F64AB
	expectedSlope       float64
	esr                 F64AB
	cr                  F64AB
	weakCutoff          uint
	strongCutoff        uint
}

// Holding structure for a channelized SLC
type MLCSLCHolder struct {
	ingestCh  chan ([]F64AB)
	controlCh chan SLCChOpcode
}

// Holding structure for points
type MLCPoints struct {
	ID     uint64
	Points []F64AB
}

// Opcode type
type MLCChOpCode uint

// Opcodes
const (
	// Exit without calculating a result; use in case of an abandoned transaction whose results no longer have a reasonable endpoint
	MLCPrematureExit MLCChOpCode = iota
	// Send the current results if any, and exit. This is the normal way to terminate a MLC with a successful query.
	MLCConcludeProcessing MLCChOpCode = iota
)

// Attempts to instantiate a new MLC/multi-line correlator. If successful, this returns two channels; a control channel, and an ingest channel.

// The points received from the ingest channel are multiplexed and directed to the appropriate single-line correlators. If a strong result is found, this is informed via the result channel.
// MLCs are by design very resilient and persistent; they will not terminate asynchronously unless in exceptional cases; more ideally, they will attempt to return any result, where available.
//
// There are several types of timeouts. Global timeout constrains the running time of the MLC; SLC timeout limits the runtime of a single SLC, and midstep timeout allows limits to be placed on how long certain channel operations may take before they can be skipped
// A note about the timeouts; the SLC timeouts should be longer than the global timeout, as to avoid a situation where they will spuriously expire before their due time. As with SLC, MLC timeouts are non-renewable: they present a firm upper bound on the execution time, and once that timeout is finished, both SLCs and MLCs may exit at any given moment.
//
// The timeouts should be treated as the equivalent of emergency barriers which will stop a runaway program, and not as routine flow control!
// Whileas the ingest channel is buffered, the control channel is not buffered; if one wants to assure that the ingest data is processed, one should attempt to feed it empty arrays. It doesn't result in any changes, but nevertheless forces them to be read
func StartNewMLC(ReturnChannel chan MLCReturnSignal, MidstepTimeout time.Duration, SLCTimeout time.Duration, GlobalTimeout time.Duration, GlobalIngestBufferSize uint, SLCIngestBufferSize uint, ResultAndControlBufSize uint, MinPoints F64AB, MinRange F64AB, AVE F64AB, ExpectedSlope float64, ESR F64AB, CR F64AB, WeakCutoff uint, StrongCutoff uint) (chan MLCPoints, chan MLCChOpCode, error) {
	// Initialize a data structure

	if (ReturnChannel == nil || GlobalIngestBufferSize < 1 || SLCIngestBufferSize < 1 || ResultAndControlBufSize < 1 || MidstepTimeout < 1 || GlobalTimeout < 1 || SLCTimeout < 1) {
		return nil, nil, errors.New("improper settings, ensure that a channel is provided for results, and that appropriate buffer sizes are defined")
	}

	ingestChannel := make(chan MLCPoints, GlobalIngestBufferSize)
	controlChannel := make(chan MLCChOpCode)
	correlator := MLCData{controlChannel, ingestChannel, ReturnChannel, MidstepTimeout, SLCTimeout, GlobalTimeout, GlobalIngestBufferSize, ResultAndControlBufSize, MinPoints, MinRange, AVE, ExpectedSlope, ESR, CR, WeakCutoff, StrongCutoff}

	go runMLC(&correlator)
	return ingestChannel, controlChannel, nil
}

// Main goroutine for MLCs
func runMLC(data *MLCData) {
	defer func() {
		if r := recover(); r != nil {
			sendResult(data.returnChannel, MLCReturnSignal{MLCAsynchErrorExit, nil}, data.midstepTimeout)
		}
	}()

	// Initialize a base map
	slcMap := make(map[uint64]*MLCSLCHolder)
	// Initialize a result map
	resultMap := make(map[uint64]*ChannelResult)
	// Ingest channel for results; ensure this has a small buffer
	returnIngestChannel := make(chan ChannelResult, data.resControlBufSize)
	// Ingest channel for control return/SLC messages; same property, ensure that buffering exists
	errorReturnCh := make(chan (ErrorReturnStruct), data.resControlBufSize)
	// Initialize a timeout
	boom := time.After(time.Second * (data.globalTimeout))
	// And start working!

	headProcessLoop:
	for {

		select {
		case res := <-returnIngestChannel:
			// We got a result! Mark this down to the map
			resultMap[res.ID] = &res
			// And signal it
			sendResult(data.returnChannel, MLCReturnSignal{MLCStrongResultSignal, nil}, data.midstepTimeout)
			continue headProcessLoop
		case controlSgnl := <-data.controlChannel:
			switch controlSgnl {
			case MLCConcludeProcessing:
				// Right. First, send a request to all parts to send states

				for _, v := range slcMap {
					if v == nil {
						continue
					}

					stateTimeout := time.After(data.midstepTimeout * time.Second)

					go func(_v *MLCSLCHolder) {
						select {
						case _v.controlCh <- SLCChnFlushIngestAndSendState:
							// OK, state request was sent.
						case <-stateTimeout:
							// Hmm.. timed out. Oh well, can't help it
						}
					}(v)
				}
				// Then, start pumping in the results as they arrive
				stateTimeout := time.After(data.midstepTimeout * time.Second)
				if (!(len(resultMap) >= len(slcMap))) {
					innerCollationLoop:
					for {
						select {
						case res := <-returnIngestChannel:
							// Do not weaken a score; we may have result maps already if we have strong scores, do not erase them!
							if (resultMap[res.ID] == nil || resultMap[res.ID].score < res.score) {
								resultMap[res.ID] = &res
							}

							if (len(resultMap) >= len(slcMap)) {
								// Our result map has the same amount, if not more results than the currently existing SLCs. Deem it as sufficient, and exit the loop prematurely
								break innerCollationLoop
							}
							continue
						case <-stateTimeout:
							// Time's up! No more waiting.
							break innerCollationLoop
						case <-boom:
							// Same, but this is a global timeout.
							break innerCollationLoop
						}
					}
				}
				// Now, we should have all necessary stuff - or if not, skip the rest.
				baseSlice := []MLCMatch{}
				for _, v := range resultMap {
					if (v.res == SLCWeakMatch || v.res == SLCStrongMatch) {
						baseSlice = append(baseSlice, MLCMatch{v.ID, v.score})
					}
				}

				// Sort
				sort.Slice(baseSlice, func(i int, j int) bool { return baseSlice[i].Score > baseSlice[j].Score })
				// Send
				sendResult(data.returnChannel, MLCReturnSignal{MLCRequestedResultExit, &MLCFoundIdentifiers{baseSlice}}, data.midstepTimeout)
				// Finally, attempt to exit SLCs
				sendChannelExits(slcMap)
				return
			case MLCPrematureExit:
				// Premature exit requested, force to quit
				// As a last hurrah, attempt to exit all SLCs
				// Finally, attempt to exit SLCs
				sendChannelExits(slcMap)
				return
			}
		case chErr := <-errorReturnCh:
			// Uh oh. We received an error!
			// Determine the type
			str := chErr.err.Error()
			wasTimeout := strings.Compare(str, "timeout") == 0
			if (wasTimeout) {
				// As select{} selects a valid channel randomly, it is possible that an error return channel with a timeout is chosen even if the invariant is followed and global timeout is longer. Or, perhaps the user selected purposefully lesser timeouts for SLCs which could lead to unpredictable stalling elsewhere.
				// Either or, we should handle this gracefully where possible
				sendResult(data.returnChannel, MLCReturnSignal{MLCTimeoutSignal, nil}, data.midstepTimeout)
			} else {
				sendResult(data.returnChannel, MLCReturnSignal{MLCErrorSignal, nil}, data.midstepTimeout)
			}
			// And to avoid holding onto a broken SLC, erase it from our map. It has already stopped and should emit no more signals
			delete(slcMap, chErr.ID)
			continue headProcessLoop
		case ingestPoints := <-data.ingestChannel:
			// We received points!
			tID := ingestPoints.ID
			points := ingestPoints.Points

			if (ingestPoints.Points == nil || len(ingestPoints.Points) < 1) {
				continue headProcessLoop
			}

			if (slcMap[tID] == nil) {
				newIngCh, newCtrlCh, err := GenerateChannelCorrelator(tID, returnIngestChannel, errorReturnCh, data.slcIngestBufferSize, data.slcTimeout, data.midstepTimeout, data.minPoints, data.minRange, data.ave, data.expectedSlope, data.esr, data.cr, data.weakCutoff, data.strongCutoff)
				if (err != nil) {
					// This failed for some reason.. this is not necessarily a critical fault, so just signal an error and discard points
					sendResult(data.returnChannel, MLCReturnSignal{MLCErrorSignal, nil}, data.midstepTimeout)
					continue headProcessLoop
				}
				slcMap[tID] = &MLCSLCHolder{newIngCh, newCtrlCh}
			}

			// Right. Ingest points to the SLC

			midstepBoom := time.After(data.midstepTimeout * time.Second)

			select {
			case slcMap[tID].ingestCh <- points:
				// OK, we ingested them
				break
			case <-midstepBoom:
				// Assume it is a timeout and signal it, this may later change if it is an error
				sendResult(data.returnChannel, MLCReturnSignal{MLCTimeoutSignal, nil}, data.midstepTimeout)
				break
			}

			// And we are done!
			continue
		case <-boom:
			// We clearly timed out. Attempt to send a signal as such, and exit
			sendResult(data.returnChannel, MLCReturnSignal{MLCAsynchTimeoutExit, nil}, data.midstepTimeout)
			return
		}
	}

}

// Send exit signals to all known channels
func sendChannelExits(slcMap map[uint64]*MLCSLCHolder) {
	for _, v := range slcMap {
		if v == nil {
			continue
		}

		go func(_v *MLCSLCHolder) {
			select {
			case _v.controlCh <- SLCChnExit:
				// OK, state request was sent.
			default:
				// We're not about to wait for this.
			}
		}(v)
	}
}

// Send results to a given channel
func sendResult(resultChannel (chan MLCReturnSignal), result MLCReturnSignal, timeout time.Duration) {
	escapeHatch := time.After(timeout * time.Second)

	select {
	case resultChannel <- result:
		// Result was successfully sent
		return
	case <-escapeHatch:
		// We have to escape, message did not go through
		return
	}
}
