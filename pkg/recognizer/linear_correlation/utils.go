package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Assorted utility methods for linear correlations
 */

import "fmt"

// Prints out a SLC's information
func PrintSLC(slc *SLCorrelator) {
	fmt.Printf("Found %v points, range min %v, max %v\n", slc.FoundPoints, slc.FoundRangeMin, slc.FoundRangeMax)
	fmt.Printf("Alternative points: %v\n", slc.AltPoints)
	fmt.Printf("Applying cutoff: %v\n", slc.AltPointCutoff)
	fmt.Printf("%v\n", slc.Points)
	fmt.Printf("Slope %v\n", slc.FoundSlope)
	fmt.Printf("Intercept %v\n", slc.FoundIntercept)
	fmt.Printf("Corrl %v\n", slc.FoundCorrl)
	fmt.Printf("Error %v\n", slc.FoundVSE)
	fmt.Printf("Score: <%v>\n", slc.LatestScore)
}