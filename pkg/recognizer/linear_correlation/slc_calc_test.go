package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SLC / Single-Line Correlator calculation tests; in this case, only multipoint tests
 */

import (
	"testing"
	"time"
)

var slcMulti *SLCorrelator
var slcMultiChan = make(chan ChannelResult)
var multiSamplePoints = []F64AB{ {0, 5}, {0,0}, {0, -5}, {0, 0.1}, {0.1,0.2}, {0.1, -0.2}, {0.1, 5}, {0.1, 0.25}, {0.2,0.4}, {0.2, 0.0}, {0.2, 100}, {0.2, 0.45}, {0.3,0.6}, {0.3, 0.7}, {0.3, 0.8}, {0.3, 0.9}, {0.3, 1.0}, {0.4,0.8}, {0.5,1}, {0.6,1.2}, {0.7, 1.4}, {0.8, 1.6}, {0.9, 1.8}, {1.0, 2.0}}

func TestMultiPoints(t *testing.T) {
	// Create a new SLC: points min 2, no minimum range but "1" suffices, average variance of error must be under 0.00005,
	slcMulti, _ = NewSLC(1234, slcMultiChan, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 1}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 50, 90)
	// Change the default coalescing limit to a small one
	slcMulti.AltPointCutoff = 64
	slcMulti.Reset()
	slcMulti.IngestPoints(multiSamplePoints)

	PrintSLC(slcMulti)
	hasKey := (slcMulti.AltPoints[0] != nil)
	if (slcMulti.LatestScore != 100 || !didReceiveStrongMarkMulti() || hasKey) {

		t.Fatal("Perfect match does not result in a good score, or coalescing did not take place!!")
	}

}

// Check that we received a strong score
func didReceiveStrongMarkMulti() bool {
	// Take 500 miliseconds as a time out; if it doesn't work with that, there's something borked
	boom := time.After(500 * time.Millisecond)

	select {
	case x := <- slcMultiChan:
		return x.ID == 1234 && x.res == SLCStrongMatch
	case <-boom:
		return false
	}
}