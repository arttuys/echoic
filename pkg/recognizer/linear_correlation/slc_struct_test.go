package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SLC structure and basic configuration tests
 */

import (
	"testing"
	"math"
	"fmt"
)

// Tests configuration; this ensures that both valid and invalid configuration options are accepted
func TestHasValidConfiguration(t *testing.T) {
	type ValF64AB struct {
		valid bool
		rn    F64AB
	}
	type ValFloat64 struct {
		valid bool
		rn    float64
	}

	type ValCutoffs struct {
		valid bool
		weak uint
		strong uint
	}

	// For each variable, we have an appropriate number of entities for the scenario; amount of loops is per the longest list
	// Particularly; index elements 1 should always be valid
	MinPoints := []ValF64AB{{true, F64AB{3.0, 10.0}},
		{true, F64AB{0.1, 5.5}},
		{false, F64AB{math.NaN(), 3.0}},
		{false, F64AB{3.0, math.Inf(0)}},
		{false, F64AB{4, 3.0}},
		{true, F64AB{-3.0, 3.0}}}

	MinRanges := []ValF64AB{{false, F64AB{0.0, -1.0}},
		{true, F64AB{-1.0, 3.0}},
		{false, F64AB{math.Inf(-1), -3.0}},
		{true, F64AB{0.0, 1.0}}}

	AMSEs := []ValF64AB{{true, F64AB{-3.0, 5.0}},
		{true, F64AB{0.3, 3.0}},
		{false, F64AB{0.0, -3.0}}}

	Slopes := []ValFloat64{{false, math.NaN()}, {true, 2.5}, {true, -3.2}, {false, math.Inf(0)}}

	SlopeRanges := []ValF64AB{{false, F64AB{math.NaN(), math.NaN()}}, {true, F64AB{0.3, 0.5}}}

	CorrlRanges := []ValF64AB{{true, F64AB{-0.75, -1.0}}, {true, F64AB{0.3, 0.6}}, {false, F64AB{0.1, math.NaN()}}}

	Cutoffs := []ValCutoffs{{false, 0, 0}, {true, 500, 900}, {false, 0, 300}, {false, 600, 1000}, {true, 1, 999}, {false, 300, 200}, {false, 400, 200}}

	Channels := [](chan ChannelResult){nil, make(chan ChannelResult)}

	// This is a bit messy, but.. it does at least ensure a large number of combinations
	for _, MinPoint := range MinPoints {
		for _, MinRange := range MinRanges {
			for _, AMSE := range AMSEs {
				for _, Slope := range Slopes {
					for _, SlopeRange := range SlopeRanges {
						for _, CorrlRange := range CorrlRanges {
							for _, Cutoff := range Cutoffs {
								for _, Channel := range Channels {
									isValid := MinPoint.valid && MinRange.valid && AMSE.valid && Slope.valid && SlopeRange.valid && CorrlRange.valid && Cutoff.valid && (Channel != nil)

									res, err := NewSLC(3, Channel, MinPoint.rn, MinRange.rn, AMSE.rn, Slope.rn, SlopeRange.rn, CorrlRange.rn, Cutoff.weak, Cutoff.strong)
									wasValid := (res != nil && err == nil)

									if (isValid != wasValid) {
										fmt.Println("Expected", isValid, "but got", wasValid)
										fmt.Println(MinPoint, MinRange, AMSE, Slope, SlopeRange, CorrlRange, Cutoff)
										fmt.Println(err)
										t.Fatal("Mismatch between expected result and returned result")

									}
								}
							}
						}
					}
				}
			}
		}
	}
}
