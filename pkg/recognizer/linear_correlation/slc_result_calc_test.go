package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SLC calculation tests; testing if various scoring functions follow expected invariants, if they behave properly in face of certain input, etc
 */

import (
	"testing"
	"fmt"
	"os"
	"sync"
	"math/rand"
	"math"
	"time"
)

var slc *SLCorrelator
var slcChan = make(chan ChannelResult)
var testMutex sync.RWMutex
var samplePoints = []F64AB{{0,0}, {0.1,0.2}, {0.2,0.4}, {0.3,0.6}, {0.4,0.8}, {0.5,1}, {0.6,1.2}, {0.7, 1.4}, {0.8, 1.6}, {0.9, 1.8}, {1.0, 2.0}}

func TestMain(m *testing.M) {
	// Create a new SLC: points min 2, no minimum range but "1" suffices, average variance of error must be under 0.00005,
	slc, _ = NewSLC(1337, slcChan, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 1}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 50, 90)
	os.Exit(m.Run())
}

// Test that an incomplete amount of points does not create a valid score
func TestIncompletePoints(t *testing.T) {
	testMutex.Lock()
	defer testMutex.Unlock()

	slc.Reset()
	slc.IngestPoints([]F64AB{{0,0}})

	PrintSLC(slc)

	if (slc.LatestScore != 0) {

		t.Fatal("Incomplete points did create a score!")
	}
}

// Check that we received a strong score
func didReceiveStrongMark() bool {
	// Take 500 miliseconds as a time out; if it doesn't work with that, there's something borked
	boom := time.After(500 * time.Millisecond)

	select {
		case x := <- slcChan:
			return x.ID == 1337 && x.res == SLCStrongMatch
		case <-boom:
			return false
	}
}

// Test that a basic line works; this should return a good result
func TestBasicLine(t *testing.T) {
	testMutex.Lock()
	defer testMutex.Unlock()

	slc.Reset()
	slc.IngestPoints(samplePoints)

	PrintSLC(slc)

	if (slc.LatestScore != 100 || !didReceiveStrongMark()) {

		t.Fatal("Perfect match does not result in a good score!")
	}
}

// Test that the change of intercept does not cause a rejection, if everything else is OK
func TestInterceptIrrelevant(t *testing.T) {
	var newPoints = make([]F64AB, len(samplePoints))
	for i, v := range samplePoints {
		newPoints[i] = F64AB{v.A, v.B + 3}
	}

	testMutex.Lock()
	defer testMutex.Unlock()
	slc.Reset()
	slc.IngestPoints(newPoints)

	PrintSLC(slc)

	if (slc.LatestScore != 100 || !didReceiveStrongMark()) {

		t.Fatal("Something caused a rejection")
	}
}

// Test that a systemic error, whilst reducing the correlation score, doesn't trigger the error variance check
func TestSystemicErrors(t *testing.T) {
	var newPoints = make([]F64AB, len(samplePoints))
	for i, v := range samplePoints {
		newPoints[i] = F64AB{v.A, v.B + (float64((-1 + (i % 2)*2)) / 2 / 3)} // Observe that this is proportionally stronger than the minor error!
	}

	testMutex.Lock()
	defer testMutex.Unlock()
	slc.Reset()
	slc.IngestPoints(newPoints)

	PrintSLC(slc)

	if (slc.LatestScore == 0 || didReceiveStrongMark()) {

		t.Fatal("Something caused a rejection, or did receive strong score!")
	}
}

// Test that minor errors do not cause a rejection. The score should be lower than for a systematic error!
func TestMinorErrors(t *testing.T) {
	var newPoints = make([]F64AB, len(samplePoints))
	for i, v := range samplePoints {
		newPoints[i] = F64AB{v.A, v.B + ((rand.Float64() - 0.5) / 3)}
	}

	testMutex.Lock()
	defer testMutex.Unlock()
	slc.Reset()
	slc.IngestPoints(newPoints)

	PrintSLC(slc)

	if (slc.LatestScore == 0 || didReceiveStrongMark()) {

		t.Fatal("Something caused a rejection, or did receive a strong unexpected score")
	}
}


// Test that an utterly wrong slope causes a failure
func TestWrongSlope(t *testing.T) {
	var newPoints = make([]F64AB, len(samplePoints))
	for i, v := range samplePoints {
		newPoints[i] = F64AB{v.A, v.B * 2}
	}

	testMutex.Lock()
	defer testMutex.Unlock()

	slc.Reset()
	slc.IngestPoints(newPoints)

	PrintSLC(slc)

	if (slc.LatestScore != 0 || didReceiveStrongMark()) {

		t.Fatal("High count of errors does not result in failure!")
	}
}

// Test that a very small clustering causes rejection
func TestBadRange(t *testing.T) {
	var newPoints = make([]F64AB, len(samplePoints))
	for i, v := range samplePoints {
		newPoints[i] = F64AB{v.A / 100, v.B}
	}

	testMutex.Lock()
	defer testMutex.Unlock()

	slc.Reset()
	slc.IngestPoints(newPoints)

	PrintSLC(slc)

	if (slc.LatestScore != 0 || didReceiveStrongMark()) {

		t.Fatal("Exceedingly small range did not cause failure")
	}
}

// Test that major errors in something that otherwise follows the line is rejected; this reduces correlation and therefore causes a failure
func TestCorrelationFail(t *testing.T) {
	var newPoints = make([]F64AB, len(samplePoints))
	for i, v := range samplePoints {
		newPoints[i] = F64AB{v.A, v.B + ((rand.Float64() - 0.5) * 2)}
	}

	testMutex.Lock()
	defer testMutex.Unlock()

	slc.Reset()
	slc.IngestPoints(newPoints)

	PrintSLC(slc)

	// Currently, the slope is calculated in relation to correction, and therefore does not exist if correlation fails
	if (!math.IsNaN(slc.FoundSlope)) {
		t.Fatal("Slope found! Correlation did not fail!")
	}

	if (slc.LatestScore != 0 || didReceiveStrongMark()) {

		t.Fatal("High count of errors does not result in failure!")
	}
}

// Tests variance in errors; this should fail due to the variance being too high
func TestVaryingErrors(t *testing.T) {
	var newPoints = make([]F64AB, len(samplePoints))
	for i, v := range samplePoints {
		rFactor := float64(i) / float64(len(samplePoints))
		newPoints[i] = F64AB{v.A, v.B + rFactor*((rand.Float64() - 0.5))}
	}

	testMutex.Lock()
	defer testMutex.Unlock()

	slc.Reset()
	slc.IngestPoints(newPoints)

	PrintSLC(slc)

	if (math.IsNaN(slc.FoundVSE)) {
		t.Fatal("Error was not calculated, failure reason something else than high error variance!")
	}
	if (slc.LatestScore != 0 || didReceiveStrongMark()) {

		t.Fatal("High count of varying errors does not result in failure!")
	}
}

// Tests the score scaling invariant; increase in raw score should also increase the final score
func TestFloatScoreInvariant(t *testing.T) {
	var score uint = 0

	for i := -0.03; i <= 1.5; i += 0.00121 {
		var oldScore uint = score
		score := floatToLogTenScore(i)
		//fmt.Printf("%v -> %v\n", i, score)
		if (oldScore > score) {
			t.Fatal("Invariant not followed; score does not increase with the input value!")
		}
	}
}

// Tests that state is sent and received properly
func TestSendState(t *testing.T) {
	testMutex.Lock()
	defer testMutex.Unlock()

	slc.Reset()
	slc.SendStateViaChannel()

	boom := time.After(500 * time.Millisecond)

	select {
		case i := <-slcChan:
			if (i.res != SLCInconclusive || i.ID != 1337) {
				t.Fatal("Returned something else than an inconclusive result with proper ID!")
			}
		case <-boom:
			// All good
	}
}

// Tests that the scaling functionality works appropriately, increasing scores when approaching the "suffc" edge
func TestRangeInvariant(t *testing.T) {
	a := scaleAndCalculateScore(0, 0.5, 0.1)
	b := scaleAndCalculateScore(0, 0.5, 0.3)

	if (b < a) {
		t.Fatal("Score decreased when it was supposed to increase!")
	}

	ia := scaleAndCalculateScore(0.1, -0.3, 0.1)
	ib := scaleAndCalculateScore(0.1, -0.3, -0.27)

	fmt.Println(ia)
	fmt.Println(ib)

	if (ib < ia) {
		t.Fatal("Score decreased when it was supposed to increase!")
	}
}