package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SLC result calculation functionality, as separated from the associated structures
 */

import (
	"math"
)

// An subset of the appropriate result structures, this is used for interim calculations and not stored on the final SLC
type SLCResultData struct {
	// How many points have been found, or 0 by default
	FoundPoints uint
	// The minimum of the range found, or NaN if not defined
	FoundRangeMin float64
	// The maximum of the range found, or NaN if not defined
	FoundRangeMax float64
	// Latest found slope, or NaN if not defined
	FoundSlope float64
	// Intercept, or NaN if not defined
	FoundIntercept float64
	// Found correlation, or NaN if not defined
	FoundCorrl float64
	// Found error, or NaN if not defined
	FoundVSE float64
	// Latest score, by default zero
	LatestScore uint
}

// Calculates a result structure from a set of points.
//
// It is expected that for all X-axis points X1...Xn, X1 <= X2 (a.k.a coordinates in X-axis always increase when their index increases)
func (slcOrig *SLCorrelator) PointsToResultStruct(points []F64AB) *SLCResultData {
	var slcRes SLCResultData

	// Reset to original properties
	slcRes.FoundRangeMin = math.NaN()
	slcRes.FoundRangeMax = math.NaN()
	slcRes.FoundPoints = 0
	slcRes.FoundSlope = math.NaN()
	slcRes.FoundIntercept = math.NaN()
	slcRes.FoundCorrl = math.NaN()
	slcRes.FoundVSE = math.NaN()
	slcRes.LatestScore = 0

	if (len(points) < 1) { // Zero points? No score!
		return &slcRes
	}

	slcRes.FoundRangeMin = points[0].A

	slcRes.FoundRangeMax = points[len(points)-1].A // This is safe; as per prior checks, this can only increase the value

	slcRes.FoundPoints = uint(len(points))

	if (!slcRes.hasProperPoints(slcOrig, &points)) {
		return &slcRes
	}

	avgs := averages(points)

	// Then attempt to update the correlation; if this returns false for an insufficient fit, exit here.
	corrlRes, psx, psy := slcRes.updateAndInterpretCorrelation(slcOrig, points, avgs)
	if (!corrlRes) {
		return &slcRes
	}

	// Alright, our correlation is also a sufficient fit. Let's check the regression next
	if (!slcRes.updateAndInterpretRegrLine(slcOrig, points, avgs, psx, psy)) {
		return &slcRes
	}

	// Our regression checks out as well! This is good
	// Final test: how the errors count?
	if (!slcRes.updateAndInterpretSSDE(slcOrig, points)) {
		return &slcRes
	}

	// Finally! We can recalculate the score
	slcRes.recalculateScore(slcOrig)

	return &slcRes
}

// Assuming that everything is properly calculated, recalculates the score
func (slcRes *SLCResultData) recalculateScore(slcOrig *SLCorrelator) {
	var score uint = 0
	score += scaleAndCalculateScore(slcOrig.MinimumPoints.A, slcOrig.MinimumPoints.B, float64(slcRes.FoundPoints))
	score += scaleAndCalculateScore(slcOrig.MinimumRange.A, slcOrig.MinimumRange.B, float64(slcRes.FoundRangeMax-slcRes.FoundRangeMin))
	score += scaleAndCalculateScore(slcOrig.ExpectedSlopeRange.B, slcOrig.ExpectedSlopeRange.A, math.Abs(slcRes.FoundSlope-slcOrig.ExpectedSlope))
	score += scaleAndCalculateScore(slcOrig.CorrelationRange.A, slcOrig.CorrelationRange.B, slcRes.FoundCorrl)
	score += scaleAndCalculateScore(slcOrig.AllowedAVE.B, slcOrig.AllowedAVE.A, slcRes.FoundVSE)

	slcRes.LatestScore = score / 5
}

// Naively calculates respective averages for A and B from a slice
func averages(points []F64AB) F64AB {

	if (len(points) == 0) {
		return F64AB{0, 0}
	}

	var xSum, ySum float64 = 0, 0
	for _, v := range (points) {
		xSum += v.A
		ySum += v.B
	}

	return F64AB{xSum / float64(len(points)), ySum / float64(len(points))}
}

// hasProperPoints() checks if the provided points are sufficient, both in number and range
func (slcRes *SLCResultData) hasProperPoints(slcOrig *SLCorrelator, points *[]F64AB) bool {
	if (float64(len(*points)) < slcOrig.MinimumPoints.A) {
		return false
	}
	diff := slcRes.FoundRangeMax - slcRes.FoundRangeMin

	return diff >= slcOrig.MinimumRange.A
}

// Determines the correlation coefficient, and intepretes it. If it is appropriate, a true boolean along with partially computed sample variances for X and Y are returned (eq: for all elements in variable Z, sum of (Z - AvgZ)^2). These can be used for an optimization further down the line
func (slcRes *SLCResultData) updateAndInterpretCorrelation(slcOrig *SLCorrelator, points []F64AB, avgs F64AB) (bool, float64, float64) {
	// What we calculate is a classic Pearson correlation coefficient for samples
	var rNumr, rDenmX, rDenmY float64 = 0, 0, 0
	for _, v := range points {
		var xAvg, yAvg = (v.A - avgs.A), (v.B - avgs.B)
		rNumr += xAvg * yAvg
		rDenmX += (xAvg * xAvg)
		rDenmY += (yAvg * yAvg)
	}

	rDenm := math.Sqrt(rDenmX) * math.Sqrt(rDenmY)
	foundCorrlCoeff := 0.0

	if (rDenm != 0) { // In the pathological case we have zero variance!
		foundCorrlCoeff = rNumr / rDenm
	}

	slcRes.FoundCorrl = foundCorrlCoeff

	// Now, we have a correlation coefficient. Let's determine if it is in the permissible range

	okPosition := false
	if (slcOrig.CorrelationRange.B >= slcOrig.CorrelationRange.A) {
		// We are going to a positive direction
		okPosition = foundCorrlCoeff >= slcOrig.CorrelationRange.A
	} else {
		// We are going into a negative direction
		okPosition = foundCorrlCoeff <= slcOrig.CorrelationRange.A
	}

	return okPosition, rDenmX, rDenmY
}

// Calculates a point as expected to be placed on a line
func (slcRes *SLCResultData) expectedLine(x float64) float64 {
	return slcRes.FoundIntercept + (x * slcRes.FoundSlope)
}

// Updates and interprets the scaled standard deviation of error
func (slcRes *SLCResultData) updateAndInterpretSSDE(slcOrig *SLCorrelator, points []F64AB) bool {
	// Check for a base case
	if (len(points) < 1) {
		return false // Nah.. impossible.
	}

	// Pass 1, calculate our errors first, squaring them
	errs := make([]float64, len(points))
	errAvg := 0.0
	for i, v := range points {
		err := v.B - slcRes.expectedLine(v.A)
		err = err * err
		errs[i] = err
		errAvg += err
	}

	errAvg /= float64(len(points))

	// Pass 2, calculate the variance
	variance := 0.0

	for _, v := range errs {
		val := (v - errAvg)
		variance += (val * val) / float64(len(errs))
	}

	// Pass 3: scale it towards the average
	scaled := 0.0
	if (errAvg >  0) {
		scaled = math.Sqrt(variance) / errAvg
	}

	slcRes.FoundVSE = scaled
	return scaled <= slcOrig.AllowedAVE.B
}

// Calculates the linear regression, and returns true if it is in the valid range
func (slcRes *SLCResultData) updateAndInterpretRegrLine(slcOrig *SLCorrelator, points []F64AB, avgs F64AB, psx float64, psy float64) bool {
	// This is the classic, simple linear regression. Classically, it is expressed in form Y = A + Bx
	// For A, the optimal value is AvgY - B*AvgX
	// For B, due to a convenient property, we can calculate it as "rxy * (sy / sx)"
	sx := math.Sqrt(psx / float64(len(points)))
	sy := math.Sqrt(psy / float64(len(points)))

	b := 0.0
	if (sx != 0) {
		b = slcRes.FoundCorrl * (sy / sx)
	}

	a := avgs.B - (b * avgs.A)

	slcRes.FoundIntercept = a
	slcRes.FoundSlope = b

	// Next, evaluate the difference
	diff := math.Abs(slcRes.FoundSlope - slcOrig.ExpectedSlope)
	// If the range is small enough, permit it.
	return diff < slcOrig.ExpectedSlopeRange.B
}

// Scales a float between 0 and 1 to a logarithmic score between 0 and 100, where 100 is the best possible score. Values are clamped to a range between 0 and 1
func floatToLogTenScore(scr float64) uint {
	if (math.IsNaN(scr) || math.IsInf(scr, 0)) {
		return 0
	}
	score := scr
	if (score > 1) {
		score = 1
	} else if (score < 0) {
		score = 0
	}

	// Scale to 32
	sc1024 := score * 32
	// We want the best scores to be hard to achieve, so:
	scDiff := 32 - sc1024
	rawLogInput := scDiff

	if (rawLogInput < 1) {
		rawLogInput = 1
	}

	// Calculate the logarithm
	log2 := math.Log2(rawLogInput)

	// And the result
	finalScore := (5.0 - log2) / 5.0 * 100.0
	if (finalScore < 0) {
		return 0
	} else if (finalScore > 100) {
		return 100
	} else {
		return uint(finalScore)
	}
}

// Scales a float value between two bounds, and returns a score
func scaleAndCalculateScore(min float64, suffc float64, state float64) uint {
	diff := suffc - min // Calculate the difference in the range
	sDiff := state - min

	return floatToLogTenScore(sDiff / diff)
}

