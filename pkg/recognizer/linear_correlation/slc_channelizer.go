package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SLC / Single-Line Correlator channelizer; an adapter for SLCs to be utilized via channels

 */

import (
	"time"
	"errors"
	"fmt"
)

// Structure for returning error messages with
type ErrorReturnStruct struct {
	ID  uint64
	err error
}

// Typedef for opcodes
type SLCChOpcode uint

// Opcodes
const (
	SLCChnExit                    SLCChOpcode = iota
	SLCChnFlushIngestAndSendState SLCChOpcode = iota
)

/**
   	GenerateChannelCorrelator() generates a new channel-based correlator; instead of a direct object reference, a set of channels will be returned
   	The first one is the ingest channel, where one should feed points to the correlator. The second one is the control channel, which allows one to execute various internal operations, e.g reset, state request and premature exit.

	Generation requires all the properties as a standard SLC would; in addition, one is expected to provide an error return channel. This channel will receive messages which indicate that the goroutine has terminated in some asynchronous way. This may be as mundane as a timeout, or it may be a panic manifesting due to some reason.
    One is also required to provide a timeout value; if the correlator does not receive any new points within a specified time of its instantiation or latest ingestion of points, it will automatically shut itself down to conserve memory space and potentially avoid GC loops (goroutine stalls on a channel which is unable to receive anything, preventing the SLC from being garbage collected).
	A timeout should be an abnormal situation; one should not rely on it regularly!

	The ingest channel has a freely chosen buffer size; this allows the pumping goroutine not to stall if for some reason the processing goroutine cannot take up the points immediately. The control channel, on the other hand, does not;
	this assures that once a sending operation is complete, the channelized SLC has received the request and is processing it. However, this limitation can be, if necessary, bypassed by using a goroutine to send a control channel message

	It is advisable to ensure both the return channel and result channels are buffered; failing that could result in control return messages not being delivered due to timing out! Delivery is not reattempted upon timeout, and it will be treated as if it were successfully delivered!

	--

	NOTE: Once a channelized SLC exits, both the ingest and control channels will stop responding. One is advised to implement a timeout mechanism, e.g to avoid a situation where the SLC has already timed out, but the client is hanging on waiting something to flush the ingest channel.
**/
func GenerateChannelCorrelator(Identifier uint64, ResultChannel chan ChannelResult, ErrorReturnChannel (chan ErrorReturnStruct), BufSize uint, Timeout time.Duration, MidstepTimeout time.Duration, MinPoints F64AB, MinRange F64AB, AVE F64AB, ExpectedSlope float64, ESR F64AB, CR F64AB, WeakCutoff uint, StrongCutoff uint) (chan ([]F64AB), chan SLCChOpcode, error) {
	if (ErrorReturnChannel == nil) {
		return nil, nil, errors.New("an error channel must be provided")
	}
	slc, err := NewSLC(Identifier, ResultChannel, MinPoints, MinRange, AVE, ExpectedSlope, ESR, CR, WeakCutoff, StrongCutoff)
	if (err != nil) {
		return nil, nil, err
	}

	// Initialize a channel for ingesting data
	ingestChannel := make(chan []F64AB, BufSize)
	// And a control channel;
	controlChannel := make(chan SLCChOpcode)
	// Launch the goroutine
	go processSLC(Identifier, slc, ingestChannel, controlChannel, ErrorReturnChannel, Timeout, MidstepTimeout)
	// And return the ingest channel
	return ingestChannel, controlChannel, nil
}

// Goroutine main for the channelizer for SLCs
func processSLC(ID uint64, slc *SLCorrelator, ingChannel (chan []F64AB), controlChn (chan SLCChOpcode), errorReturnCh (chan ErrorReturnStruct), timeout time.Duration, midstepTimeout time.Duration) {
	defer func() {
		if r := recover(); r != nil {
			sendErrorReturnMessage(ID, errorReturnCh, fmt.Sprintf("panicked (%v)", r), midstepTimeout)
		}
	}()

	// Initialize a timeout; after this timeout strikes, this channelized SLC will exit at a suitable opportunity
	boom := time.After(time.Second * timeout)

	loop:
	for {

		select {
		case controlSignal := <-controlChn:
			switch controlSignal {
			case SLCChnExit:
				// On request to exit, simply return.
				return
			case SLCChnFlushIngestAndSendState:
				// Ask the correlator to send its status, and wait for new commands after. Before the state send is done, the ingest buffer is flushed
				ingestFlush:
				for {
					select {
						case in := <-ingChannel:
							slc.IngestPoints(in)
						default:
							break ingestFlush
					}
				}
				slc.SendStateViaChannel()
				continue loop
			}
		case in := <-ingChannel:
			// Received new data from the ingest channel
			slc.IngestPoints(in)
			continue loop
		case <-boom:
			// Woopsie. We timed out. Let's signal that to the holder of the error channel and exit
			sendErrorReturnMessage(ID, errorReturnCh, "timeout", midstepTimeout)
			return;
		}
	}
}

// Helper function for safely sending return messages; if the sending channel hangs for some reason, this exits quietly
func sendErrorReturnMessage(ID uint64,  errorReturnCh (chan ErrorReturnStruct), msg string, timeout time.Duration) {
	escapeHatch := time.After(timeout * time.Second)
	select {
	case errorReturnCh <- ErrorReturnStruct{ID, errors.New(msg)}:
		// Ok, we managed to send our message
	case <-escapeHatch:
		// In the event that our control message times out, simply exit quietly.
	}
}
