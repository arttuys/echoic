package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SLC / Single-Line Correlator interface; ingesting data, interfacing with the calculation module, coalescing results into a single result
 */

import (
	"math"
	"time"
	"sort"
)

type ResultIndexPair struct {
	res *SLCResultData
	indexes	 []uint
}

// IngestPoints() appends new points to the SLC. It is ESSENTIAL that all data provided is ascending in X-axis/A, and that both X and Y are defined. Anything else will cause this function to panic!
// It is guaranteed that after a call with a non-empty, valid set of points, a score of either 0 or more will be available.

// Note that the score may be non-monotonic. Scores are re-evaluated only once after appending the points, and that is the only point where a signal may be sent in case of a strong match.
func (slc *SLCorrelator) IngestPoints(points []F64AB) {
	if (slc == nil || points == nil) {
		panic("Called with nil SLC or nil points")
	} else if (len(points) == 0) {
		return // No points to add, no need to take a lock
	}

	// First, actually append our points
	slc.RWMutex.Lock()
	defer slc.RWMutex.Unlock()
	validatePoints(slc.FoundRangeMin, slc.FoundRangeMax, points)
	slc.appendPointsAndDetermineState(points)

	// Now, check if we have a strong match
	if (slc.Match() == SLCStrongMatch) {
		// If so, inform about it
		sendStateInternal(slc.Channel, slc.Identifier, slc.Match(), slc.LatestScore)
	}
}

// Asks the SLC to send its status via a channel, to the specified channel
func (slc *SLCorrelator) SendStateViaChannel() {
	slc.RWMutex.RLock()
	defer slc.RWMutex.RUnlock()

	sendStateInternal(slc.Channel, slc.Identifier, slc.Match(), slc.LatestScore)
}

// Sends a result state. This timeouts within 10 seconds if a result is unable to be delivered
func sendStateInternal(ch (chan ChannelResult), id uint64, status SLCMatchResult, score uint) {
	go func(_ch (chan ChannelResult), _id uint64, _status SLCMatchResult, _score uint) {
		escapeHatch := time.After(5 * time.Second)
		select {
		case ch <- ChannelResult{id, status, score}:
			// Sent successfully
			return
		case <-escapeHatch:
			// Escaped
		}
	}(ch, id, status, score)
}

// Returns the current match type
func (slc *SLCorrelator) Match() SLCMatchResult {
	if (slc == nil) {
		panic("Called with a nil SLC")
	}

	if (slc.LatestScore >= uint(slc.SLCStrongCutoff)) {
		return SLCStrongMatch
	} else if (slc.LatestScore >= uint(slc.SLCWeakCutoff)) {
		return SLCWeakMatch
	} else {
		return SLCInconclusive
	}
}

// validatePoints() validates a given set of points, and panics if it doesn't follow optimization invariants, a.k.a must be monotonic
func validatePoints(min float64, max float64, points []F64AB) {
	// Pass 1; check that all values are properly defined and ascend
	var foundMax = math.NaN()
	for _, v := range points {
		if (math.IsNaN(v.B) || math.IsInf(v.B, 0)) {
			panic("Y axis values undefined")
		}
		if (math.IsNaN(v.A) || math.IsInf(v.A, 0)) {
			panic("X axis values undefined")
		}
		if (math.IsNaN(foundMax)) {
			foundMax = v.A
		} else if (foundMax > v.A) {
			panic("Non-ascending value found in X-axis")
		} else {
			foundMax = v.A // Not a NaN, but not a nonascending value either.
		}
	}

	// Now, foundMax is the largest number we've found
	// And as ascending is verified, foundMin is the smallest we have found
	foundMin := points[0].A

	// Now, that the ascending property has been verified, check that we are within bounds; minimum
	if (!math.IsNaN(min) && !math.IsNaN(max)) {
		if (max > foundMin) {
			panic("Points out of bounds; X misaligned")
		}
	}
	// OK, this seems to be all valid and nice.
}

// Reset() resets the SLC to a fresh state, as if it had just been instantiated
func (slc *SLCorrelator) Reset() {

	if (slc == nil) {
		panic("Called with a nil SLC")
	}

	slc.RWMutex.Lock()
	defer slc.RWMutex.Unlock()

	slc.Points = []F64AB{}
	slc.FoundRangeMin = math.NaN()
	slc.FoundRangeMax = math.NaN()
	slc.FoundPoints = 0
	slc.FoundSlope = math.NaN()
	slc.FoundIntercept = math.NaN()
	slc.FoundCorrl = math.NaN()
	slc.FoundVSE = math.NaN()
	slc.LatestScore = 0
}

// Counts the amount of subchoices currently possible; this is used to determine if points should be coalesced due to excess complexity
func (slc *SLCorrelator) possibleSubchoices() uint {
	subchoices := uint(0)

	for _, v := range slc.AltPoints {
		if (v == nil) {
			continue
		}

		lenV := len(v)
		if (lenV == 0) {
			continue
		} else if subchoices == 0 {
			subchoices = uint(lenV)
		} else {
			subchoices *= uint(lenV)
		}
	}

	return subchoices
}

// Recursively determines the best result available. It takes the first index in the slice of remaining options with alternative indexes, and returns the indexes of units selected (with 0 meaning that the primary point is the best), in the same order as they were originally provided
func (slc *SLCorrelator) recursiveResultFinder(altIndexesRemaining []uint, selectedIndexesStack []uint, selectedPointsMap map[uint]F64AB) ResultIndexPair {
	//fmt.Printf("Indexes %v, stack %v, map %v\n", altIndexesRemaining, selectedIndexesStack, selectedPointsMap)
	if len(altIndexesRemaining) == 0 {
		panic("no indexes remaining, undefined behavior!")
	}

	// Prepare necessary variables
	targetedIndex := altIndexesRemaining[0]
	possiblePoints := []F64AB{slc.Points[targetedIndex]}

	if (slc.AltPoints[targetedIndex] != nil) {
		possiblePoints = append(possiblePoints, slc.AltPoints[targetedIndex]...)
	}

	resultArr := make([]ResultIndexPair, 0)

	// Iterate and generate a list of proper structures
	for pi, p := range possiblePoints {
		newMap := make(map[uint]F64AB)
		for k, v := range selectedPointsMap {
			newMap[k] = v
		}

		newMap[targetedIndex] = p

		// Next, our action depends on our amount of indexes
		if (len(altIndexesRemaining) > 1) {
			// There's still nesting to be done
			remainingIndexes := altIndexesRemaining[1:]
			resultArr = append(resultArr, slc.recursiveResultFinder(remainingIndexes, append(selectedIndexesStack, uint(pi)), newMap))
		} else {
			// No nesting, resolve the final points ourself
			tempPoints := make([]F64AB, len(slc.Points))
			copy(tempPoints, slc.Points)

			for k, v := range newMap {
				tempPoints[k] = v
			}

			//fmt.Println("Asking for: ")
			//fmt.Println(tempPoints)
			points := slc.PointsToResultStruct(tempPoints)
			//fmt.Printf("Got %v\n", *points)
			resultArr = append(resultArr, ResultIndexPair{points, append(selectedIndexesStack, uint(pi))})
		}
	}

	// Now, sort these..
	sort.Slice(resultArr, func(i, j int) bool {
		return resultArr[i].res.LatestScore < resultArr[j].res.LatestScore
	})

	// Select the result..
	res := resultArr[len(resultArr) - 1]
	//fmt.Printf("Returning %v (%v)\n", res, *res.res)

	// And return the last one.
	return res
}

// Determines the best alternative score-wise, and if so needed, coalesces earlier points to avoid further branching
func (slc *SLCorrelator) determineBestAlternativeAndCoalesce() *SLCResultData {
	if (len(slc.AltPoints) == 0) {
		return slc.PointsToResultStruct(slc.Points)
	}

	// All right, we need to determine the proper order.
	sortedKeys := make([]uint, 0)
	for k, v := range slc.AltPoints {
		if (v != nil) {
			sortedKeys = append(sortedKeys, k)
		}
	}

	// Sort the keys!
	sort.Slice(sortedKeys, func(i, j int) bool {
		return sortedKeys[i] < sortedKeys[j] // Standard, no specialties here
	})

	// Get the appropriate results
	pair := slc.recursiveResultFinder(sortedKeys, []uint{}, make(map[uint]F64AB))

	// Unpack
	res := pair.res
	indexes := pair.indexes

	// Coalesce points, if required
	for (slc.possibleSubchoices() > slc.AltPointCutoff && slc.possibleSubchoices() != 0 && len(indexes) > 0 && len(sortedKeys) > 0) {
		replaceableIndex := sortedKeys[0]
		newValIndex := indexes[0]

		// Replace if needed
		if (newValIndex > 0) {
			slc.Points[replaceableIndex] = slc.AltPoints[replaceableIndex][newValIndex-1]
		}

		// Delete the key entirely
		delete(slc.AltPoints, replaceableIndex)

		// Adjust indexes appropriately
		sortedKeys = sortedKeys[1:]
		indexes = indexes[1:]
	}

	return res
}

// Updates the internal state; this updates the found ranges, slopes, et al - and if everything is fine, calls the procedure which calculates the final score
func (slc *SLCorrelator) appendPointsAndDetermineState(points []F64AB) {

	// Append to original points; they are now a part of our set
	// Be sure to also update the possible alternatives
	lastAppendedCoord := math.Inf(-1)
	for _, v := range points {
		if (math.IsInf(lastAppendedCoord, 0) || lastAppendedCoord < v.A || len(slc.Points) < 1) {
			// This is a new coordinate, append it
			lastAppendedCoord = v.A
			slc.Points = append(slc.Points, v)
		} else {
			altCord := uint(len(slc.Points) - 1)
			if (slc.AltPoints[altCord] == nil) {
				slc.AltPoints[altCord] = []F64AB{v}
			} else {
				slc.AltPoints[altCord] = append(slc.AltPoints[altCord], v)
			}
		}
	}
	// Determine the correct point set, and if necessary, coalesce points
	// This also determines the correct result

	// Calculate
	slcRes := slc.determineBestAlternativeAndCoalesce()

	// And update
	slc.FoundRangeMin = slcRes.FoundRangeMin
	slc.FoundRangeMax = slcRes.FoundRangeMax
	slc.FoundPoints = slcRes.FoundPoints
	slc.FoundSlope = slcRes.FoundSlope
	slc.FoundIntercept = slcRes.FoundIntercept
	slc.FoundCorrl = slcRes.FoundCorrl
	slc.FoundVSE = slcRes.FoundVSE
	slc.LatestScore = slcRes.LatestScore

}


