package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Tests for MLCs
 */

import (
	"testing"
	"time"
	"fmt"
)

// Induce an invalid SLC configuration, to test error handling there
func TestInduceInvalidSLCConfiguration(t *testing.T) {
	mlcReturnChannel := make(chan MLCReturnSignal, 5) // Buffered as requested
	// Weak cutoff has been swapped with the strong one
	ingestChannel, ctrlChannel, _ := StartNewMLC(mlcReturnChannel, 5, 20, 10, 5, 5, 5, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 0.5}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 99, 10)

	// Ingest samples
	samplePoints := []F64AB{{0,0}, {0.1,0.2}}
	ingestChannel <- MLCPoints{1234, samplePoints}

	boom := time.After(5 * time.Second)

	// We now expect to receive an error, as a SLC cannot be instantiated
	select {
	case ing:=<-mlcReturnChannel:
		if (ing.ResultCode != MLCErrorSignal) {
			t.Fatal("Expected an error signal!")
		}
		break
	case <-boom:
		t.Fatal("Did not receive expected signal!")
	}

	select {
	case ctrlChannel<-MLCPrematureExit:
		// OK
	case <-boom:
		t.Fatal("Did not ingest the exit signal!")
	}
}

// Induces a SLC timeout, to see it is properly caught
func TestInduceAsyncSLCTimeout(t *testing.T) {
	mlcReturnChannel := make(chan MLCReturnSignal, 5) // Buffered as requested
	ingestChannel, ctrlChannel, _ := StartNewMLC(mlcReturnChannel, 5, 2, 10, 5, 5, 5, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 0.5}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 10, 99)

	samplePoints := []F64AB{{0,0}, {0.1,0.2}}
	ingestChannel <- MLCPoints{1234, samplePoints}

	// Wait a bit to induce a timeout
	time.Sleep(3 * time.Second)

	boom := time.After(5 * time.Second)



	select {
	case ing:=<-mlcReturnChannel:
		if (ing.ResultCode != MLCTimeoutSignal) {
			t.Fatal("Expected a timeout signal")
		}
		break
	case <-boom:
		t.Fatal("Did not receive expected signal!")
	}

	// Now, send an exit signal
	select {
	case ctrlChannel<-MLCPrematureExit:
		// OK
	case <-boom:
		t.Fatal("Did not ingest the exit signal!")
	}
}

// Induce an asynchronous timeout for the MLC
func TestInduceAsyncGlobalTimeout(t *testing.T) {
	mlcReturnChannel := make(chan MLCReturnSignal, 5) // Buffered as requested
	StartNewMLC(mlcReturnChannel, 5, 10, 1, 5, 5, 5, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 0.5}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 10, 99)

	// Wait a bit..
	time.Sleep(2 * time.Second)

	boom := time.After(5 * time.Second)
	select {
	case ing:=<-mlcReturnChannel:
		if (ing.ResultCode != MLCAsynchTimeoutExit) {
			t.Fatal("Expected an asynch timeout exit")
		}
		break
	case <-boom:
		t.Fatal("Did not receive expected signal!")
	}
}

// Purposefully feeds a MLC bad data, and induces a panic in a SLC
func TestCrashingSLC(t *testing.T) {
	mlcReturnChannel := make(chan MLCReturnSignal, 5) // Buffered as requested
	ingestChannel, ctrlChannel, _ := StartNewMLC(mlcReturnChannel, 5, 15, 10, 5, 5, 5, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 0.5}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 10, 99)

	samplePoints := []F64AB{{0,0}, {0.1,0.2}}
	// Ingest two copies of this.
	ingestChannel <- MLCPoints{1234, samplePoints}
	ingestChannel <- MLCPoints{1234, samplePoints}
	// This should now result in an error signal due to a panic
	boom := time.After(10 * time.Second)
	select {
	case ing:=<-mlcReturnChannel:
		if (ing.ResultCode != MLCErrorSignal) {
			t.Fatal("Expected an error signal!")
		}
		break
	case <-boom:
		t.Fatal("Did not receive expected signal!")
	}

	// Now, send in an exit request

	select {
		case ctrlChannel<-MLCPrematureExit:
			// OK
	case <-boom:
		t.Fatal("Did not ingest the exit signal!")
	}

}

// Simple MLC test for three lines; one strong, one weak, one no match
func TestSimpleMLC(t *testing.T) {
	// 	slc, _ = NewSLC(1337, slcChan, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 0.0005}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 50, 90)
	mlcReturnChannel := make(chan MLCReturnSignal, 5) // Buffered as requested
	samplePointsA := []F64AB{{0,0}, {0.1,0.2}, {0.2,0.4}, {0.3,0.6}, {0.4,0.8}, {0.5,1}, {0.6,1.2}, {0.7, 1.4}, {0.8, 1.6}, {0.9, 1.8}, {1.0, 2.0}}
	samplePointsB := []F64AB{{2,0}, {2.1,0.23}, {2.2,0.7}, {2.3,0.6}, {2.4,0.8}, {2.5,1.1}, {2.6,1.2}, {2.7, 1.4}, {2.8, 1.6}, {2.9, 1.8}, {3.0, 2.0}}
	samplePointsC := []F64AB{{0,3-0}, {0.1,3-0.2}, {0.2,3-0.4}, {0.3,3-0.6}, {0.4,3-0.8}, {0.5,3-1}, {0.6,3-1.2}, {0.7, 3-1.4}, {0.8, 3-1.6}, {0.9, 3-1.8}, {1.0, 3-2.0}}

	ingestChannel, ctrlChannel, _ := StartNewMLC(mlcReturnChannel, 5, 15, 10, 5, 5, 5, F64AB{2,10}, F64AB{0, 1}, F64AB{0, 3}, 2, F64AB{0, 0.5}, F64AB{0.85, 0.9}, 10, 99)


	ingestChannel <- MLCPoints{2468, samplePointsB[:3]}
	ingestChannel <- MLCPoints{3331, samplePointsC[:1]}
	ingestChannel <- MLCPoints{1234, samplePointsA[:4]}
	ingestChannel <- MLCPoints{2468, samplePointsB[3:]}
	ingestChannel <- MLCPoints{1234, samplePointsA[4:]}
	ingestChannel <- MLCPoints{3331, samplePointsC[1:]}
	// To assure that the ingestion has went through, create spurious zero points over the buffer size. Once this clears, everything worthwhile is being processed
	for i :=0; i < 5; i++ {
		ingestChannel <- MLCPoints{0, []F64AB{}}
	}
	time.Sleep(1 * time.Second)
	ctrlChannel <- MLCConcludeProcessing

	// Give it a bit of rest..


	// Now; as the first point ingestion is a direct match, it should have signaled

	boom := time.After(10 * time.Second)

	select {
		case ing:=<-mlcReturnChannel:
			if (ing.ResultCode == MLCRequestedResultExit) {
				t.Fatal("Strong signal did not propagate appropriately!")
			}
			if (ing.ResultCode != MLCStrongResultSignal) {
				t.Fatal("Expected a strong result signal!")
			}

		case <-boom:
			t.Fatal("Did not receive expected signal!")
	}


	// Next, a strong match
	select {
	case ing:=<-mlcReturnChannel:
		fmt.Println(ing)
		if (ing.Matches != nil) {
			fmt.Println(*ing.Matches)
		}
		if (ing.ResultCode == MLCStrongResultSignal) {
			t.Fatal("Test mistuned, 2nd signal too strong!")
		}
		if (ing.ResultCode != MLCRequestedResultExit) {
			t.Fatal("Expected an exit signal!")
		}
		if (ing.Matches == nil) {
			t.Fatal("Should have matches!")
		}

		matches := ing.Matches
		if (len(matches.Matches) != 2) {
			t.Fatal("Matches are of improper length")
		}
		if (matches.Matches[0].ID != 1234) {
			t.Fatal("Most precise fitting should be first")
		}
		if (matches.Matches[1].ID != 2468) {
			t.Fatal("Most precise fitting should be first")
		}

	case <-boom:
		t.Fatal("Did not receive expected signal!")
	}
}
