package linear_correlation

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SLC / Single-Line Correlator, basic structures and definitions
 */

import (
	"sync"
	"errors"
	"math"
	"fmt"
)

type SLCMatchResult uint

// Result types
const (
	/*
		SLCInconclusive indicates at the end of a stream that one cannot draw a conclusion that the provided points show a reasonable linear correlation
	 */
	SLCInconclusive SLCMatchResult = iota
	/*
		SLCWeakMatch indicates that while a linear correlation seems to form, it can perhaps be supplanted by a stronger conclusion somewhere
	 */
	SLCWeakMatch SLCMatchResult = iota
	/*
		SLCStrongMatch indicates that there is a strong, apparent linear correlation. This may also be returned in response to streamed data; this is useful to prematurely stop streaming in data if a conclusion can be already drewn
	 */
	SLCStrongMatch SLCMatchResult = iota
)

/**
	For practical purposes, it is necessary to define a maximum limit for possible choices. This is called the "coalescing limit"; if there are more choices than the coalescing limit would allow, points deposited first will start to be locked into position to reduce the numer of possibilities to evaluate.
 */
const DefaultCoalescingLimit = 550

/**
	A single-line correlator will continuously measure the stream of points fed in, and and attempts to see if they form a neat line. It may signal using a channel at any point when it determines that the match is strong enough.
	One may also compare scores, normalized between 0-100, if they are available; if some metric is too low, the entire score will be 0

	A caveat; this stream's X values MUST be monotonic; any other case will go against the assumptions required for certain optimizations. There is a best-effort check to panic if such a case is caught. However, this invariant should be relatively easy to guarantee for streamed data, assuming X is some time unit for the stream and Y is freely selected.
	Another caveat: in case of strict failure (latest score 0, same as if no points have been applied), there may possibly be partial results available, but it should be checked case-by-case. The precise availability of those is undefined

 */

 // Basic 2-value float type

// Channel result structure
type ChannelResult struct{ID uint64; res SLCMatchResult; score uint}

// Basic SLC structure
type SLCorrelator struct {
	// SLCs are designed to be thread-safe from the start; a guarantee will be provided that at least for SLC itself, no written changes will take place without a write lock. It is also advisable to use a read lock where possible, if one needs to asynchronously view data
	RWMutex    sync.RWMutex
	// Identifier that will be sent over a channel, if a strong match is found
	Identifier uint64
	// Channel is where such results are sent. One should assure that this channel is NOT closed until it is assured that there are no SLCs active that could send things to them.
	Channel    (chan ChannelResult)
	// List of current points
	Points     []F64AB
	// Point alternatives for indexes, if required
	AltPoints map[uint]([]F64AB)
	////////////////////////////////////////////////////
	// How many points have been found, or 0 by default
	FoundPoints uint
	// The minimum of the range found, or NaN if not defined
	FoundRangeMin float64
	// The maximum of the range found, or NaN if not defined
	FoundRangeMax float64
	// Latest found slope, or NaN if not defined
	FoundSlope float64
	// Intercept, or NaN if not defined
	FoundIntercept float64
	// Found correlation, or NaN if not defined
	FoundCorrl float64
	// Found error, or NaN if not defined
	FoundVSE float64
	// Latest score, by default zero
	LatestScore uint
	////////////////////////////////////////////////////
	/*
		For all following: A indicates a minimum, B total sufficience/perfection. It is not invalid to have parameters outside the possible range, but side effects can either include inability to have full points, or conversely a guaranteed minimum score

		MinimumPoints indicates the minimum sample required; anything below that will return SLCInconclusive due to the inability to draw rigorous conclusions not easily shaken.
		MinimumRange indicates the same in terms of the breadth required in samples X coordinates; points must span at least this length, minimum A to maximally scoring B. If points are allowed to cluster into an excessively small spot, it could also result in spurious matches. This forces scores to even out slightly before allowing potential matches to take place.

		For following, inverse applies: A indicates a perfect score, whileas B indicates the maximum limit tolerated

		AllowedAVE indicates the scaled standard deviation of error allowed. Effectively, this measures how much the errors vary in proportion to the average error. If this is too high, it hints to an unpredictable change in hits. This is useful in quickly determining how "skewed" or non-linear our data is; if this is high, our data does not accurately follow the expected timeline, and therefore could be spurious in some was
	 */
	MinimumPoints F64AB
	MinimumRange  F64AB
	AllowedAVE    F64AB
	/*
		For following: A indicates a penalty-free range, B indicates maximum divergence

		ExpectedSlope indicates the expected slope of the line; this could have slight variance in case of misaligned samples, or maybe a stretched song
		ExpectedSlopeRange indicates the allowed variance; changes in a penalty-free range are not penalized, but changes beyond max will result in a fail
	 */
	ExpectedSlope      float64
	ExpectedSlopeRange F64AB
	/*
		CorrelationRange indicates the allowed range for correlation; A is the "bad end", and B is the "good end"; if past the good end in the direction of change, no more points beyond perfect will be assessed; respectively, falling under the bad end will lead to a strict failure
	*/
	CorrelationRange F64AB
	/**
		Weak and Strong cutoffs indicate the score limits for various classifications; Strong in particular is special that if such a correlation is found, streaming will be interrupted to alert the user of such an event
	*/
	SLCWeakCutoff   uint
	SLCStrongCutoff uint
	/*
		One may provide alternative points in a single ingest batch. As the amount of combinations can rise quite rapidly, there does exist a cutoff for alternatives, after which points will start to be coalesced into the currently most optimal combinations. This should not be ordinarily modified, as this can potentially cause extreme, exponential-level growth.
	 */
	 AltPointCutoff uint
}

// Checks that an AB value is valid
func validAB(prop F64AB, allowNonMonotonic bool) error {
	if math.IsInf(prop.A, 0) || math.IsInf(prop.B, 0) {
		return errors.New("floats must not be infinite")
	}
	if math.IsNaN(prop.A) || math.IsNaN(prop.B) {
		return errors.New("floats must not be NaN")
	}
	if ((!allowNonMonotonic) && prop.B < prop.A) {
		return errors.New(fmt.Sprintf("B must be equal or greater than A (%v)", prop))
	}

	return nil
}

// HasValidConfiguration checks if all of the properties are valid. This will lock a read mutex
func (slc *SLCorrelator) HasValidConfiguration() error {
	if (slc == nil) {
		panic("Called with nil SLC")
	}

	slc.RWMutex.RLock()
	// First, define which properties are expected to have which features
	monotonicProperties := [](F64AB){slc.MinimumPoints, slc.MinimumRange, slc.AllowedAVE, slc.ExpectedSlopeRange}
	notMonotonicProprerties := [](F64AB){slc.CorrelationRange}

	if (slc.Channel == nil) {
		slc.RWMutex.RUnlock()
		return errors.New("channel is nil")
	}

	// Check them
	for _, v := range monotonicProperties {
		res := validAB(v, false)
		if (res != nil) {
			slc.RWMutex.RUnlock()
			return res
		}
	}

	for _, v := range notMonotonicProprerties {
		res := validAB(v, true)
		if (res != nil) {
			slc.RWMutex.RUnlock()
			return res
		}
	}

	// Check slopes separately
	if math.IsInf(slc.ExpectedSlope, 0) || math.IsNaN(slc.ExpectedSlope) {
		slc.RWMutex.RUnlock()
		return errors.New("slope must be a number")
	}

	// And cutoffs too
	cutoffs := []uint{slc.SLCWeakCutoff, slc.SLCStrongCutoff}

	for _, v := range cutoffs {
		if v <= 0 || v >= 1000 {
			slc.RWMutex.RUnlock()
			return errors.New("cutoffs invalid or impossible")
		}
	}

	if slc.SLCStrongCutoff < slc.SLCWeakCutoff {
		slc.RWMutex.RUnlock()
		return errors.New("cutoffs must be monotonic")
	}

	slc.RWMutex.RUnlock()
	return nil
}

// NewSLC initializes a new SLC, as per properties specified. If the configuration is valid, it will succeed; otherwise a failure will result
func NewSLC(Identifier uint64, Channel chan ChannelResult, MinPoints F64AB, MinRange F64AB, AVE F64AB, ExpectedSlope float64, ESR F64AB, CR F64AB, WeakCutoff uint, StrongCutoff uint) (*SLCorrelator, error) {
	baseStruct := SLCorrelator{sync.RWMutex{}, Identifier, Channel,[]F64AB{}, make(map[uint]([]F64AB)), 0, math.NaN(), math.NaN(),  math.NaN(), math.NaN(), math.NaN(), math.NaN(),0,MinPoints, MinRange, AVE, ExpectedSlope,
								ESR, CR, WeakCutoff, StrongCutoff, DefaultCoalescingLimit}

	if err := baseStruct.HasValidConfiguration(); err == nil {
		return &baseStruct, nil
	} else {
		return nil, err
	}
}
