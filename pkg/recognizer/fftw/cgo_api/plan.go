package cgo_api

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Simple FFTW bindings, to allow leveraging the powerful capabilities of the library for FFT and IFFT calculations

 */

// #include <complex.h>
// #include <fftw3.h>
// #include <stdlib.h>
// #include <string.h>
import "C"
import (
	"fmt"
	"sync"
	"runtime"
	"unsafe"
	"errors"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"reflect"
)

var threadsInited = false

var planAlterMutex sync.Mutex

// InitFFTWThreads() initialized FFTW3 for multithreading. This must be called BEFORE calling any other routines, or they will panic. This function may also panic in the unlikely circumstance that multithreading initialization fails
func InitFFTWThreads() {
	planAlterMutex.Lock()
	defer planAlterMutex.Unlock()
	if (!threadsInited) {
		res := C.fftw_init_threads()
		if (res == 0) { // See: http://www.fftw.org/fftw3_doc/Usage-of-Multi_002dthreaded-FFTW.html#Usage-of-Multi_002dthreaded-FFTW - a zero value is expected for panic!
			fmt.Println(res)
			panic("FFTW thread initialization failed!")
		}

		numThreads := runtime.NumCPU()
		C.fftw_plan_with_nthreads(C.int(numThreads))
		threadsInited = true
	}

}

// Trigger a panic if threads have not been initialized
func panicIfNotInited() {
	if (!threadsInited) {
		panic("please initialize FFTW first before using any FFTW API methods!")
	}
}

// SaveFFTWWisdomAsString() extracts the current wisdom collected by FFTW; this is effectively a constantly improving set of data that will help FFTW speed up its actions where available
func SaveFFTWWisdomAsString() string {
	panicIfNotInited()

	planAlterMutex.Lock()
	defer planAlterMutex.Unlock()
	//////
	cString := C.fftw_export_wisdom_to_string()
	goString := C.GoString(cString)
	C.free(unsafe.Pointer(cString))
	//////


	return goString
}

// LoadFFTWWisdomFromString() loads so-called "wisdom" for FFT; this can give substantial speed advantages
func LoadFFTWWisdomFromString(wisdom string) bool {
	if (wisdom == "") {
		return false
	}
	panicIfNotInited()

	planAlterMutex.Lock()
	defer planAlterMutex.Unlock()
	//////
	cString := C.CString(wisdom)
	res := C.fftw_import_wisdom_from_string(cString)
	C.free(unsafe.Pointer(cString))
	//////

	return res == 1
}

// LoadFFTWSystemWisdom() attempts to load the system-wide wisdom data; this may not necessarily exist
func LoadFFTWSystemWisdom() bool {
	panicIfNotInited()

	planAlterMutex.Lock()
	defer planAlterMutex.Unlock()
	//////
	res := C.fftw_import_system_wisdom()
	//////

	return res == 1
}

//////////////////////

type FFTWPlan struct {
	bufferIn       *C.fftw_complex
	bufferOut      *C.fftw_complex
	bufSize        uintptr
	Dims           []uint
	plan           C.fftw_plan
	transformMutex sync.Mutex
	invalidated    bool
}

func NewFFTWPlan(shape []uint, forward bool) (*FFTWPlan, error) {
	// Check the validity of the shape first
	if shape == nil || len(shape) == 0 {
		return nil, errors.New("shape must be nonempty")
	}

	var totalSize uint = 1
	for _, el := range shape {
		if (el < 1) {
			return nil, errors.New("all dimensions must be nonzero")
		}

		totalSize = narrays.SafeMult(totalSize, el)
	}

	// OK, copy that to a C.int array
	shapeC := make([]C.int, len(shape))
	for i, e := range shape {
		shapeC[i] = C.int(e)
	}

	/// REMEMBER - not even fftw_alloc is thread-safe! Execution is the absolutely only thing that is thread-safe

	// Try to allocate a buffer full of complex numbers
	planAlterMutex.Lock()
	defer planAlterMutex.Unlock()
	bufferAllocIn := C.fftw_alloc_complex(C.size_t(totalSize))
	if bufferAllocIn == nil {
		return nil, errors.New("was unable to allocate an input buffer for the FFTW plan")
	}
	bufferAllocOut := C.fftw_alloc_complex(C.size_t(totalSize))
	if bufferAllocOut == nil {
		C.fftw_free(unsafe.Pointer(bufferAllocIn))
		return nil, errors.New("was unable to allocate an output buffer for the FFTW plan")
	}

	// Okay, we succeeded in our allocation.
	// Let's build our base structure
	var (
		dataRank = C.int(len(shapeC))
		bufIn    = (*C.fftw_complex)(unsafe.Pointer(bufferAllocIn))
		bufOut   = (*C.fftw_complex)(unsafe.Pointer(bufferAllocOut))
	)
	var sign int
	if forward {
		sign = -1
	} else {
		sign = 1
	}

	planResult := C.fftw_plan_dft(dataRank, &shapeC[0], bufIn, bufOut, C.int(sign), C.FFTW_PATIENT) // Observation: FFTW documentation does not specify that the shape array should be kept in memory as an allocation, and therefore it is considered safe to use a Go pointer for it.

	if (planResult == nil) {
		C.fftw_free(unsafe.Pointer(bufferAllocOut))
		C.fftw_free(unsafe.Pointer(bufferAllocIn))
		return nil, errors.New("failed to create FFTW DFT plan")
	}

	// Allright, everything is done smoothly.
	var planStruct FFTWPlan
	planStruct.bufferIn = bufferAllocIn
	planStruct.bufferOut = bufferAllocOut
	planStruct.bufSize = uintptr(totalSize) * unsafe.Sizeof(C.fftw_complex(0))
	planStruct.Dims = shape
	planStruct.invalidated = false
	planStruct.plan = planResult

	//c_string := C.fftw_sprint_plan(planResult)
	//go_string := C.GoString(c_string)
	//C.free(unsafe.Pointer(c_string))
	//fmt.Println(go_string)
	return &planStruct, nil
}

func (p *FFTWPlan) Close() {
	if (p == nil) {
		panic("Closing attempted on a nil plan")
	}

	p.transformMutex.Lock()
	planAlterMutex.Lock()

	defer planAlterMutex.Unlock()
	defer p.transformMutex.Unlock()
	if (p.invalidated) {
		return // This is already closed, OK.
	}

	C.fftw_destroy_plan(p.plan)
	C.fftw_free(unsafe.Pointer(p.bufferIn))
	C.fftw_free(unsafe.Pointer(p.bufferOut))
	p.invalidated = true // This is no longer valid



}

func (p *FFTWPlan) DoDFFTForArray(arr *narrays.ComplexArrayN) error {
	if (p == nil) {
		panic("DFFT attempted on a nil plan")
	}

	if (arr == nil) {
		return errors.New("array must be non-nil")
	}

	p.transformMutex.Lock()
	defer p.transformMutex.Unlock()

	if (p.invalidated) {
		return errors.New("this plan has already been invalidated")
	}
	if !reflect.DeepEqual(p.Dims, arr.N) {
		return errors.New("mismatched dimensions between FFTW plan and desired matrix")
	}

	// First, copy the contents
	elementsStartPtr := unsafe.Pointer(&(arr.Elements[0]))
	C.memcpy(unsafe.Pointer(p.bufferIn), elementsStartPtr, C.size_t(p.bufSize))
	// Call the function..
	C.fftw_execute(p.plan)
	// And shovel the data back
	C.memcpy(elementsStartPtr, unsafe.Pointer(p.bufferOut), C.size_t(p.bufSize))
	// We're done!
	return nil
}
