package cgo_api

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Tests for FFTW bindings
 */

import (
	"testing"
	"os"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"fmt"
)

func TestMain(m *testing.M) {
	InitFFTWThreads()
	os.Exit(m.Run())
}

// This is hard to test otherwise.. so just check that this doesn't crash anything
func TestSystemWisdom(t *testing.T) {
	LoadFFTWSystemWisdom()
}

func TestCompiledWithOpenMP(t *testing.T) {
	if (CompiledWithOpenMP()) {
		fmt.Println("Compiled and ran with OpenMP")
	} else {
		fmt.Println("Compiled and ran with standard multithreading")
	}
}

// Test other basic wisdom
func TestBasicWisdom(t *testing.T) {
	// Pure nonsense should fail
	invalidValues := []string{"", "abcdef"}
	for _, v := range invalidValues {
		if (LoadFFTWWisdomFromString(v)) {
			t.Fatal("Loaded invalid wisdom!")
		}
	}

	if (!LoadFFTWWisdomFromString(SaveFFTWWisdomAsString())) {
		t.Fatal("Failed to load fresh, saved wisdom!")
	}
}

// Test creating a FFTW plan
func TestNewFFTWPlan(t *testing.T) {
	// Test nonsense values first
	invalidValues := []([]uint){nil, {}, {2, 6, 3, 0, 2, 1}}
	for _, v := range invalidValues {
		res, err := NewFFTWPlan(v, true)
		res2, err2 := NewFFTWPlan(v, false)

		if ((res != nil || err == nil) || (res2 != nil || err2 == nil)) {
			t.Fatal("NewFFTWPlan accepted invalid values")
		}
	}

	// Try with a valid value
	validDim := []uint{16}
	arr, _ := narrays.NewArrayN([]uint{16})
	misshapedArr, _ := narrays.NewArrayN([]uint{16, 1})

	plan, err := NewFFTWPlan(validDim, true)
	if (err != nil) {
		t.Fatal("Failed to allocate a valid plan")
	}

	invalidArrs := [](*narrays.ComplexArrayN){nil, misshapedArr}
	for _, v := range invalidArrs {
		err = plan.DoDFFTForArray(v)
		if (err == nil) {
			plan.Close()
			t.Fatal("Did work on an invalid complex array")
		}
	}

	// Let's do a simple test for behavior
	for i := range arr.Elements {
		arr.Elements[i] = 3
	}

	// Execute a FFT transform
	err = plan.DoDFFTForArray(arr)
	if (err != nil) {
		plan.Close()
		t.Fatal("Failed to do a valid transform!")
	}

	// The transform is unnormalized, so it should be multiplied by N. Also, the first element should be the average strength of the signal
	if (!(arr.Elements[0] == complex(3*16, 0))) {
		plan.Close()
		t.Fatal("Unexpected results!")
	}

	// Rest of them should be zero
	for _, v := range arr.Elements[1:] {
		if (v != complex(0, 0)) {
			plan.Close()
			t.Fatal("Non-zero elements found in a simple transform!")
		}
	}

	// Allright, now let's tod it in reverse
	plan2, err := NewFFTWPlan(validDim, false)

	if (err != nil) {
		plan.Close()
		t.Fatal("Failed to create an inverse transformation")
	}

	// Let's change it a little bit
	arr.Elements[0] = 4 * 16

	if (plan2.DoDFFTForArray(arr) != nil) {
		plan.Close()
		plan2.Close()
		t.Fatal("Failed to complete a valid tranformation")
	}

	// Inverse transformation retains the scale of the upscaled forward transform, so...
	for _, v := range arr.Elements {
		if (v != complex(4*16, 0)) {
			plan.Close()
			plan2.Close()
			t.Fatal("Invalid elements found in an inverse transform")
		}
	}

	// OK!
	plan.Close()
	plan.Close() // 2nd call should not panic or do anything else untoward
	plan2.Close()

	// Try to use an invalidated plan
	err = plan.DoDFFTForArray(arr)
	if (err == nil) {
		t.Fatal("Successfully did an operation for an invalidated plan!")
	}
}
