package channelizer

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	FFTW channelization tests
 */

import (
	"testing"
	"time"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fftw/cgo_api"
)

// Test FFTW channel creation, usage, and destruction
func TestGenerateFFTWChannel(t *testing.T) {
	// Check that threads are initialized
	cgo_api.InitFFTWThreads()

	validDim := []uint{16}
	// Exceptionally, do not have a buffer! This allows us to test a timeout
	outCh := make(chan FFTWResult)
	// Attempt to generate a channel

	inCh, controlCh, err := GenerateFFTWChannel(5, 2, outCh, validDim, true)
	if (err != nil) {
		t.Fatal("Channel creation failed!")
	}

	boom := time.After(10 * time.Second)

	// Initialize a simple test array
	arr, _ := narrays.NewArrayN([]uint{16})
	for i := range arr.Elements {
		arr.Elements[i] = 6
	}

	// Send it in
	select {
		case inCh<-arr:
			// OK
			case <-boom:
				t.Fatal("Did not ingest array in time!")
	}

	// Receive output
	select {
		case res:=<-outCh:
			if res.ResultCode != FFTWResSuccessful {
				t.Fatal("Failed to get a correct result!")
			}
			arr := res.Data
			// As copied from the basic FFTW tests
			// The transform is unnormalized, so it should be multiplied by N. Also, the first element should be the average strength of the signal
			if (!(arr.Elements[0] == complex(6*16, 0))) {
				t.Fatal("Unexpected results!")
			}

			// Rest of them should be zero
			for _, v := range arr.Elements[1:] {
				if (v != complex(0, 0)) {
					t.Fatal("Non-zero elements found in a simple transform!")
				}
			}
		case <-boom:
			t.Fatal("Did not receive result!")
	}

	// Next, test problematic cases
	badShapeArr, _ := narrays.NewArrayN([]uint{4,4})
	badInputs := []*narrays.ComplexArrayN{nil, badShapeArr}

	for _, v := range badInputs {
		// Ingest
		select {
		case inCh<-v:
			// OK
		case <-boom:
			t.Fatal("Did not ingest array in time!")
		}

		// Retrieve output
		select {
		case res:=<-outCh:
			if res.ResultCode != FFTWResInvalidInput {
				t.Fatal("Failed to get a result marking incorrect input!")
			}
		case <-boom:
			t.Fatal("Did not receive result!")
		}
	}

	// This time, test a timeout; send in an array, but do not retrieve the output!
	// Send it in
	select {
	case inCh<-arr:
		// OK
	case <-boom:
		t.Fatal("Did not ingest array in time!")
	}

	// Sleep as to expire the short output timeout
	time.Sleep(4 * time.Second)

	// Finally, ingest freeing call
	select {
		case controlCh<-FFTWChFree:
			// OK!
		case <-boom:
			t.Fatal("Did not ingest exit signal!")
	}
}
