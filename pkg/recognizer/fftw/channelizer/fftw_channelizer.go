package channelizer

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	FFTW channelizer - channel-based asynchronous interface for FFTW
 */

import (
	"gitlab.com/arttuys/echoic/pkg/recognizer/fftw/cgo_api"
	"errors"
	"time"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
)

type FFTWChOpCode uint
type FFTWResCode uint

// Opcodes
const (
	FFTWChFree FFTWChOpCode = iota
)

// Result codes
const (
	// Latest ingestion was successful, and along with this result code, a transformed structure follows
	FFTWResSuccessful FFTWResCode = iota
	// Latest ingestion was of an invalid format (or nil), and failed. No structure is provided with this result code.
	FFTWResInvalidInput FFTWResCode = iota
	// The channelized FFTW has experienced an internal error, and has exited. No further signals from this channel can result
	FFTWResPrematureExit FFTWResCode = iota
)

// Result structure
type FFTWResult struct {
	ResultCode FFTWResCode
	Data       *narrays.ComplexArrayN
}

// Generates a FFTW channel. This ingests points, and spits out (I)FFTs of them in a predefined format.
// In addition to the standard plan properties, one is expected to configure the ingest buffer size (how much waiting items there can be before blocking), and output timeout (if output is unable to be sent, how long this FFTW channel should wait?)
//
// Do note that unlike MLCs or SLCs, FFTW channels DO NOT timeout asynchronously! Whileas MLCs and SLCs are meant to be relatively short-lived, a FFTW channel is designed to be reusable part due to the possible high initialization cost, part due to the lack of an apparent "end" condition unlike with MLCs/SLCs. Therefore, one should ALWAYS be assured that if a FFTW channel is not required, it is appropriately disposed of.
// However, a FFTW channel MAY timeout if the output is not retrieved in a prescribed time, as to not stall something else down the line. Order is guaranteed though; first in will be the first out, if it is outputted at all
func GenerateFFTWChannel(ingestBufSize uint, outputTimeout time.Duration, outCh chan FFTWResult, shape []uint, forward bool) (chan *narrays.ComplexArrayN, chan FFTWChOpCode, error) {
	if (outCh == nil || ingestBufSize < 1 || outputTimeout < 1) {
		return nil, nil, errors.New("output channel must be defined, and ingest buffer size larger than 1")
	}

	// Attempt to create a plan
	plan, err := cgo_api.NewFFTWPlan(shape, forward)

	// Error out if failed
	if (err != nil) {
		return nil, nil, err
	}

	// Success!
	ingestCh := make(chan *narrays.ComplexArrayN, ingestBufSize)
	controlCh := make(chan FFTWChOpCode)

	go runFFTW(ingestCh, controlCh, outCh, outputTimeout, plan)
	return ingestCh, controlCh, nil
}

// Sends a result safely, timing out if appropriate
func sendRes(outCh chan FFTWResult, res FFTWResult, outputTimeout time.Duration) {
	boom := time.After(outputTimeout * time.Second)

	select {
		case outCh<-res:
			return
		case <-boom:
			return
	}
}

// Main goroutine for running a FFTW channel
func runFFTW(inCh chan *narrays.ComplexArrayN, controlCh chan FFTWChOpCode, outCh chan FFTWResult, outputTimeout time.Duration, plan *cgo_api.FFTWPlan) {
	defer func() {
		if r := recover(); r != nil {
			boom := time.After(time.Second * outputTimeout)
			select {
			case outCh <- FFTWResult{FFTWResPrematureExit, nil}:
				// OK
			case <-boom:
				// Timeout, force majeure..
			}
		}
	}()
	defer plan.Close() // Upon exit, attempt to close the plan

	for {
		select {
		case points := <-inCh:
			if (points == nil) {
				sendRes(outCh, FFTWResult{FFTWResInvalidInput, nil}, outputTimeout)
				continue
			}

			// Attempt to make a copy. If this fails, send an invalid input signal
			pointsCopy, err := points.Slice([]uint{})
			if (err != nil) {
				sendRes(outCh, FFTWResult{FFTWResInvalidInput, nil}, outputTimeout)
				continue
			}

			// Attempt to FFT it
			err = plan.DoDFFTForArray(pointsCopy)
			if (err != nil) {
				sendRes(outCh, FFTWResult{FFTWResInvalidInput, nil}, outputTimeout)
				continue
			}

			// All good? Good!
			sendRes(outCh, FFTWResult{FFTWResSuccessful, pointsCopy}, outputTimeout)
			continue
		case cmd := <-controlCh:
			switch cmd {
			case FFTWChFree:
				// Exit for channel release
				return
			}
		}
	}
}
