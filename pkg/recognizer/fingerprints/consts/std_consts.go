package consts

import (
	"gitlab.com/arttuys/echoic/pkg/recognizer/linear_correlation"
)

// Sampling options
const StandardSampleRate = 10000       // Standard sampling rate for songs et al
const StandardFPBlockSamples = 20480   // How many blocks one fingerprint should contain? This is not necessarily the same as above!
const StandardSubtimeDivisor = 2 // This indicates the substep fraction; when taking longer than 1-block fingerprints, we may do substeps if possible; this is the fraction that is used
const StandardDiscreteBlocksDivisor = StandardFPBlockSamples / StandardSubtimeDivisor // This takes account the fact that the block generation actually generates the double of blocks!
const StandardTimeSegmentPerFPBlock = (float64(StandardFPBlockSamples) / float64(StandardSampleRate)) / float64(StandardSubtimeDivisor) // Approximately what amount in time does a single block match? In this case, effectively a half, as every 2nd
const StandardSampleSize = 2048        // How many samples one DFFTed sequence should have?
const StandardSamplesPerFrameHop = 1024 // How many samples we should hop at a time?
// Fingerprinting options, block level
const StandardMinimumRelativePower = 0.5     // When compared to the average of its immediate surroundings, how much of a relative power by minimum it should have, when compared on a log10 scale? This is in addition to requiring the strength to be same or higher than each of its immediate neighbors
const StandardPowerSortFuzz = 0.000001         // When ordering by power, with how large steps one should fuzz it
const StandardMaxTrackBlockFingerprints = 7 // What is the maximum amount of fingerprint blocks we should return, as sorted by the match power when we are analyzing a full track?
const StandardMaxSampleBlockFingerprints = 12 // What is the maximum amount of fingerprint blocks we should return, as sorted by match power, when we are analyzing an user-provided sample? This could be higher than track fingerprints, as chances are, noise would not ruin all fingerprints
const StandardTimeBands = 19                  // How many time bands we have in terms of forming hashes?
const TimeBandDistMin = 3                     // Excluding the starting band, how close may the starting time be?
const TimeBandDistMax = 11                    // Excluding the starting band, how far may the later end be?
const StandardFreqBands = 510                 // How many frequency bands we have in terms of forming hashes?
const FreqBandMax = 400                       // How many frequency bands is the maximum distance between two points?
const FreqBandMin = 50						  // How many frequency bands is the distance by minimum?

// Fingerprinting options, track level
const IntersongMaxMatch = 0.08           	// What is the absolute cross-matching limit? If some fingerprint matches more than this percentage of blocks, it is discarded as utterly unworthy
const IntersongAcceptableBlocks = 10 		// In short tracks, a raw percentage can cause extremely hard-to-match fingerprints. Always accept some fingerprint if it is seen in less than this amount of blocks, even if the percentage would be lower
const DecisivityHighMark = 0.33		 	// Decisivity is calculated by dividing the total of distances between block matches with the amount of total blocks minus the count of blocks matched. Higher this number is, more spread out the matches are. This allows fine-tuning of crossmatch tolerance; it may be more acceptable to have multiple matches clustered in close proximity than spread out
// Standard fingerprint MLC/SLC options. It is worthwhile to note that all timeout times are given in seconds!
const StandardFPRMidstepTimeout = 20 // How long we should wait mid-operation for something?
const StandardFPRMLCTimeout = 150 // How long it takes for a MLC to time out? This will be also used as the standard FP processor timeout, so the ingested audio should be of a substantially shorter length
const StandardFPRSLCTimeout = 2 * StandardFPRMLCTimeout // How long it takes for a SLC to time out on its own?
const StandardFPRTime = 60 // How long a standard fingerprint sample would be?
const StandardFPRMaxSamplesIngested = StandardFPRTime * StandardSampleRate // How many samples will the fingerprinting channel ingest, before calling it a stop, implicitly forcing a conclusion, and starting to ignore the ingested data?
var StandardFPRMinPoints = linear_correlation.F64AB{2, StandardFPRTime / 6 * 4} // Require 4 fingerprints per 6 seconds
var StandardFPRMinRange = linear_correlation.F64AB{1.0, StandardFPRTime / 6 * 1} // Allow at max 1 seconds range per every 6 seconds of the standard fingerprint sample, and require a small nontrivial period of time
var StandardFPRAVE = linear_correlation.F64AB{5, 10000} // Allow a very high range of errors.
var StandardFPRExpSlope = 1.0 // Naturally, expect one-to-one slopes
var StandardFPRESR = linear_correlation.F64AB{0.07, 0.20} // Do not expect perfectly correct slopes, but do not allow excessive variation either
var StandardFPRCR = linear_correlation.F64AB{0.25, 0.85} // Allow relatively low correlation rates, and again, do not demand pixel-perfect correlation
const StandardFPRWeakCutoff = 40 // Require a reasonable number of points to pass a weak cutoff
const StandardFPRStrongCutoff = 80 // Require high certainty for a strong cutoff
// Buffer options
const StandardFPRChBuffer = 10 // Internal channel buffers have this size; as they do not transport raw audio data, do not require a large queue
const StandardFPRAudioChBuffer = 3 * 20480 // Audio data, however, should have a larger buffer
const StandardFPRAudioStepOutMultipler = 1.5 // If we receive audio very quickly, it may be a worthwhile to have a step-out provision; if we have received more than enough data, we forcefully step back to the main select, to ensure that we have a chance to receive other signals as well