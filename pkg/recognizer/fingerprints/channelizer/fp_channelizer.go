package channelizer

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Fingerprinter channelization - a bridge between fingerprinting and channel-based concurrency
 */

import (
	"gitlab.com/arttuys/echoic/pkg/recognizer/linear_correlation"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/consts"
	"gitlab.com/arttuys/echoic/pkg/model/db"
	"errors"
	"time"
	"gitlab.com/arttuys/echoic/pkg/recognizer/sox"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"os"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fftw/cgo_api"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/fp_gen"
	"fmt"
)

type FPPOpcode uint

const (
	// Exit prematurely, without collecting results
	FPPrematureExit FPPOpcode = iota
	// Exit and collect results
	FPConcludeProcessing FPPOpcode = iota
)

type FPPResultCode uint

const (
	// This is signaled when the sample limit is fulfilled; raw audio will no longer be considered but will instead be discarded if delivered.
	FPPBufferOverrunReqConclusion FPPResultCode = iota
	// The receiver no longer attempt to send in audio data, and should instead conclude
	FPPShouldRequestConclusion FPPResultCode = iota
	// FPP exited, and returned a set of results alongside
	FPPResultExit FPPResultCode = iota
	// Sort of an "oops" signal; this signal signifies that something unusual happened which may have affected results, but wasn't something that would compromise the entire run
	// This should be reported to the user
	FPPAbnormalityDetected FPPResultCode = iota
	// FPP exited due to a timeout, and will no longer respond to any channels
	FPPTimeoutExit FPPResultCode = iota
	// FPP exited due to a critical error, and will no longer respond to any channels
	FPPErrorExit FPPResultCode = iota
)

type FPPReturnSignal struct {
	ResultCode FPPResultCode
	Matches    *linear_correlation.MLCFoundIdentifiers
}

type FPProcessor struct {
	RawAudioIngestChannel (chan float64)
	ControlCh             (chan FPPOpcode)
	ingestSampleRate      uint
	sqlConn               *db.FPSQLConn
	midstepTimeout        time.Duration
	mlcPointIgest         (chan linear_correlation.MLCPoints)
	mlcOpcodeIngest       (chan linear_correlation.MLCChOpCode)
	mlcReturnSignal       (chan linear_correlation.MLCReturnSignal)
	fpReturnSignal        (chan FPPReturnSignal)
	fftwPlan              *cgo_api.FFTWPlan
}

func NewStandardFPProcessor(sqlBackend *db.FPSQLConn, baseSoxConfig *sox.RunnerConfig, ingestSampleRate uint, returnCh (chan FPPReturnSignal)) (*FPProcessor, error) {
	if (sqlBackend == nil || ingestSampleRate < consts.StandardSampleRate || ingestSampleRate > consts.StandardSampleRate*10 || baseSoxConfig == nil || returnCh == nil) {
		return nil, errors.New("Improper parameters provided for fingerprinting")
	}
	// First, create a MLC
	mlcRetChnl := make(chan linear_correlation.MLCReturnSignal, 10)
	audioIngestChannel := make(chan float64, consts.StandardFPRAudioChBuffer)
	// Control channels are not buffered, to ensure that once a channel send clears, it has gone through and is being addressed
	controlCh := make(chan FPPOpcode)
	// Attempt to create a channelized DFFT structure
	fftwPlan, fftwErr := cgo_api.NewFFTWPlan([]uint{consts.StandardSampleSize}, true)
	if (fftwErr != nil) {
		return nil, fftwErr
	}
	mlcPointIngestCh, mlcOpcodeIngestCh, err := linear_correlation.StartNewMLC(mlcRetChnl, consts.StandardFPRMidstepTimeout, consts.StandardFPRSLCTimeout, consts.StandardFPRMLCTimeout, consts.StandardFPRChBuffer*2, consts.StandardFPRChBuffer, consts.StandardFPRChBuffer*2, consts.StandardFPRMinPoints, consts.StandardFPRMinRange, consts.StandardFPRAVE, consts.StandardFPRExpSlope, consts.StandardFPRESR, consts.StandardFPRCR, consts.StandardFPRWeakCutoff, consts.StandardFPRStrongCutoff)
	if (err != nil) {
		// Attempt to terminate the FFTW plan
		fftwPlan.Close()
		return nil, err
	}

	fppStruct := FPProcessor{audioIngestChannel, controlCh, ingestSampleRate, sqlBackend, consts.StandardFPRMidstepTimeout, mlcPointIngestCh, mlcOpcodeIngestCh, mlcRetChnl, returnCh, fftwPlan}

	// Alright, everything seems to be in order. Start the processing, and return the appropriate data
	go fppStruct.runFPP()
	return &fppStruct, nil
}

func (processor *FPProcessor) runFPP() {
	globalTimeout := time.After(time.Second * consts.StandardFPRSLCTimeout)
	AudioStepOutMark := int(float64(processor.ingestSampleRate) * consts.StandardFPRAudioStepOutMultipler)
	collectedAudioSamples := uint64(0)                                     // How many samples in total we have collected?
	sentShouldConcludeWarning := false                                     // Have we sent an overrun warning
	waitingForConclusionInformation := false                               // Are we waiting for the final score to be sent? This stops sample data from being ingested
	sentAbnormalityWarning := false                                        // Have we yet sent an abnormality warning
	awaitingRawBuffer := make([]float64, 0, consts.StandardFPBlockSamples) // Ensure that there is at least capacity for a sample
	var awaitingTransformedBuffer *narrays.ComplexArrayN = nil
	awaitingDFFTSlices := make([]*narrays.ComplexArrayN, 0)
	// Timing variables
	xPosTimeStep := float64(consts.StandardFPBlockSamples) / float64(consts.StandardSampleRate)
	currentXTime := 0.0
	// Let's build the appropriate SOX ingest command as well
	// We shall integrate normalization to this as well, to improve the
	var soxConfig, _ = sox.NewSoxConfig("/usr/bin/sox", os.TempDir())
	soxIngestParam := sox.BuildSoxDataParam("raw", processor.ingestSampleRate, "floating-point", 64, 1, sox.LittleEndian, "-")
	soxOutputParam := sox.BuildSoxDataParam("raw", consts.StandardSampleRate, "floating-point", 64, 1, sox.LittleEndian, "-")
	soxCommand := soxConfig.BuildSoxCommand([]string{}, soxIngestParam, soxOutputParam, []string{})

	// Ensure that our plan terminates when our function expires
	defer processor.fftwPlan.Close()
	defer func() {
		if r := recover(); r != nil {
			// Last ditch attempt; recover from an error
			processor.sendRes(FPPReturnSignal{FPPErrorExit, nil})
		}
	}()
//	defer func() {
//		fmt.Println("Exited from FPP")
//	}()
	for {
		select {
		// We can receive samples from our ingest channel..
		case sampleIn := <-processor.RawAudioIngestChannel:

			//fmt.Println("Ingesting data")

			if (waitingForConclusionInformation) {
				continue // Skip this sample, we are waiting for a concluding score and should no longer take sample data
			}
			// Check first if we are over the correct amount
			if (collectedAudioSamples > consts.StandardFPRMaxSamplesIngested) {
				if (!sentShouldConcludeWarning) {
					processor.sendRes(FPPReturnSignal{FPPBufferOverrunReqConclusion, nil})
				}
				sentShouldConcludeWarning = true
				continue
			}
			// No? OK, start counting
			collectedAudioSamples++
			collectedOnThisStep := 0
			// Data is being ingested. Append the first part..
			awaitingRawBuffer = append(awaitingRawBuffer, sampleIn)
			// .. then attempt to flush this, until we run out, have more than the step out mark, or collected enough points
			ingestWaitLoop:
			for (collectedAudioSamples <= consts.StandardFPRMaxSamplesIngested && collectedOnThisStep <= AudioStepOutMark) {
				select {
				case sampleInX := <-processor.RawAudioIngestChannel:
					awaitingRawBuffer = append(awaitingRawBuffer, sampleInX)
					collectedAudioSamples++
					collectedOnThisStep++
				default:
					// OK, nothing to ingest
					//fmt.Println("Breaking...")
					break ingestWaitLoop
				}
			}

			//fmt.Println("Collected all we can get now..")
			//fmt.Printf("Length %v\n", len(awaitingRawBuffer))

			// We will require at least a sane mimimum of points to execute a conversion from the raw buffer
			// If this is not filled, continue
			if (len(awaitingRawBuffer) < AudioStepOutMark) {
				continue
			}

			// Okay. Now, let's process the necessary data from the raw buffer
			resNarr, convErr := sox.TransformDataWithSoxToComplexArr(soxCommand, awaitingRawBuffer, consts.StandardFPRMidstepTimeout)
			if awaitingTransformedBuffer == nil {
				// Empty, replace entirely
				awaitingTransformedBuffer = resNarr
			} else if (resNarr != nil) {
				// Nonempty, add
				awaitingTransformedBuffer.Append(resNarr, resNarr.Len(), func(i interface{}, u uint) complex128 {
					return i.(*narrays.ComplexArrayN).Elements[u]
				})
			}

			//fmt.Println("Got the transformed buffer")
			//fmt.Printf("Length %v\n", awaitingTransformedBuffer.Len())

			// Flush the raw buffer
			awaitingRawBuffer = make([]float64, 0, consts.StandardFPBlockSamples)

			// Send in an abnormality warning in case we received unusual results
			if ((resNarr == nil || convErr != nil) && !sentAbnormalityWarning) {
				//fmt.Println(convErr)
				processor.sendAbnormalityIfNeeded(&sentAbnormalityWarning)
			}

			if (awaitingTransformedBuffer != nil) {
				// Now, slice and process repeatedly until we run out of slots
				for (awaitingTransformedBuffer.Len() >= consts.StandardFPBlockSamples) {
					// Slice up the audio
					firstSlice, _ := awaitingTransformedBuffer.Slice1D(0, consts.StandardFPBlockSamples)
					rest, _ := awaitingTransformedBuffer.Slice1D(consts.StandardFPBlockSamples, awaitingTransformedBuffer.Len()-consts.StandardFPBlockSamples)

					if (firstSlice == nil || rest == nil) {
						// .. right, this is entirely unexpected. Something went very badly wrong. Crash gracefully still
						//fmt.Println("Null slices, stopping")
						processor.stopMLCPrematurely()
						processor.sendRes(FPPReturnSignal{FPPErrorExit, nil})
						return
					}

					// Attempt to ingest it to the DFFT array
					awaitingDFFTSlices = append(awaitingDFFTSlices, firstSlice)
					// Reset our current slice
					awaitingTransformedBuffer = rest
				}

				//fmt.Println("Ingesting DFFT slices..")
				//fmt.Printf("Length %v\n", len(awaitingDFFTSlices))

				// Go through each FFT slice, fingerprint them, and ingest the data to a MLC
				for _, v := range awaitingDFFTSlices {
					// Generate fingerprints
					fprints, err := fp_gen.DataArrToFingerprintSet(v, processor.fftwPlan, false)
					if (err != nil || fprints == nil || len(fprints) < 1 || fprints[0] == nil) {
						processor.sendAbnormalityIfNeeded(&sentAbnormalityWarning)
					} else {
						// This should not return more than one set..
						fprintList := fprints[0]
						// Right, ask from SQL what these are
						mlcPoints, sqlErr := processor.sqlConn.HashListToMLCPoints(currentXTime, fprintList)
						//fmt.Printf("Got SQL points %v\n", mlcPoints)
						if (sqlErr != nil || mlcPoints == nil) {
							processor.sendAbnormalityIfNeeded(&sentAbnormalityWarning)
						} else {
							for _, mlcPointSet := range mlcPoints {
								// Try to send these to the MLC
								midstepBoom := time.After(time.Second * processor.midstepTimeout)
								select {
								case <-midstepBoom:
									processor.sendAbnormalityIfNeeded(&sentAbnormalityWarning)
								case processor.mlcPointIgest <- mlcPointSet:
									//fmt.Println("\tSuccessfully ingested to MLC!")
									// OK!
								}
								// Increase the time slot after we're done
								currentXTime += xPosTimeStep
							}
						}
					}
				}

				// Now that we've completed this, let's just zero out the array
				awaitingDFFTSlices = make([]*narrays.ComplexArrayN, 0)
			}
			//fmt.Println("OK!")
			continue
			// We have received something from the MLC!
		case mlcRes := <-processor.mlcReturnSignal:
			//fmt.Println("Received message from MLC")
			// Check what we have
			switch (mlcRes.ResultCode) {
			case linear_correlation.MLCAsynchErrorExit:
				//fmt.Println("Asynch error exit!")
				// Uh oh, asynchronous error. This is not good, we should exit
				processor.sendRes(FPPReturnSignal{FPPErrorExit, nil})
				return
			case linear_correlation.MLCAsynchTimeoutExit:
				//fmt.Println("Asynch timeout exit!")
				// Well.. our MLC timed out, so we should do that as well
				processor.sendRes(FPPReturnSignal{FPPTimeoutExit, nil})
				return
			case linear_correlation.MLCTimeoutSignal:
				fallthrough
			case linear_correlation.MLCErrorSignal:
				// Something unusual happened; report this as an abnormality
				processor.sendAbnormalityIfNeeded(&sentAbnormalityWarning)
			case linear_correlation.MLCStrongResultSignal:
				// We have a strong result.
				if (!sentShouldConcludeWarning) {
					processor.sendRes(FPPReturnSignal{FPPShouldRequestConclusion, nil})
				}
				sentShouldConcludeWarning = true
				// We originally locked ingestions, but as now the scores are never weakened on result requests if a strong result has been sent, we can leave it on for the chance that we get more brilliant results
				//waitingForConclusionInformation = true
			case linear_correlation.MLCRequestedResultExit:
				// We are now ready to exit.
				processor.sendRes(FPPReturnSignal{FPPResultExit, mlcRes.Matches})
				return
			default:
				continue // We don't know what this is, ignore it
			}
		case ffpOpcode := <-processor.ControlCh:
			//fmt.Println("Received message from user")
			// Check what we request
			switch (ffpOpcode) {
			case FPPrematureExit:
				// We are requesting to exit prematurely
				processor.stopMLCPrematurely()
				// Exit was on request, so do not signal separately
				return
			case FPConcludeProcessing:
				// Try to inform MLC of the results being desired
				midstepBoom := time.After(time.Second * processor.midstepTimeout)
				select {
				case <-midstepBoom:
					processor.sendAbnormalityIfNeeded(&sentAbnormalityWarning)
					// In either case, do not accept further samples
					fmt.Println("Stalled on attempting to inform a MLC")
				case processor.mlcOpcodeIngest <- linear_correlation.MLCConcludeProcessing:
					// OK! Now mark us as waiting for conclusion information

				}

				waitingForConclusionInformation = true
			default:
				continue
			}
		case <-globalTimeout:
			// Hm. We timed out.
			//fmt.Println("Uh oh, global timeout!")
			processor.stopMLCPrematurely()
			processor.sendRes(FPPReturnSignal{FPPTimeoutExit, nil})
			return

		}
	}
}

// Attempt to stop the MLC prematurely, without collecting results
func (processor *FPProcessor) stopMLCPrematurely() {
	//fmt.Println("Exiting from MLC prematurely")
	boom := time.After(time.Second * processor.midstepTimeout)

	select {
	case processor.mlcOpcodeIngest <- linear_correlation.MLCPrematureExit:
		return
	case <-boom:
		return
	}
}

// Sends an abnormality indication if not already sent
func (processor *FPProcessor) sendAbnormalityIfNeeded(bv *bool) {
	if (!*bv) {
		//fmt.Println("Abnormality detected, warning...")
		processor.sendRes(FPPReturnSignal{FPPAbnormalityDetected, nil})
		*bv = true
	}
}

// Sends a result safely, timing out if appropriate
func (processor *FPProcessor) sendRes(res FPPReturnSignal) {
	boom := time.After(time.Second * processor.midstepTimeout)

	select {
	case processor.fpReturnSignal <- res:
		return
	case <-boom:
		return
	}
}
