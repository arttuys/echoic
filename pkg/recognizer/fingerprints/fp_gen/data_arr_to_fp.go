package fp_gen

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Raw float data -> fingerprint generation functions
 */

import (
	"errors"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fftw/cgo_api"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/consts"
	"math"
	"sort"
)

// Calculates the multipler for a Hanning window function; this improves the frequency resolution
func HanningWindow(n uint, N uint) float64 {
	baseSine := math.Sin((math.Pi * float64(n)) / float64(N - 1))

	return baseSine * baseSine
}

/*
	DataArrToFingerprintSet() takes a set of data and a valid FFTW plan, and composes a fingerprint (or multiple fingerprints, per single block) of that data.

	Do observe that this also does stepping smaller than the FP block size; at this current implementation, half-steps are done and therefore double the blocks effectively returned.

	There is a special parameter that if used, will bypass deduplication and lowers the amount of fingerprints collected
 */
func DataArrToFingerprintSet(data *narrays.ComplexArrayN, plan *cgo_api.FFTWPlan, dedup bool) (map[uint64]([]uint64), error) {
	if data.Len() < consts.StandardFPBlockSamples {
		return nil, errors.New("not able to form a valid sample from an incomplete part")
	}

	maxLen := data.Len() - (data.Len() % consts.StandardFPBlockSamples)
	fpBlocksAvailable := maxLen / consts.StandardFPBlockSamples
	// Per-block fingerprints
	fpBlockMap := make(map[uint64]([]uint64))
	// In which blocks this fingerprint has been seen?
	fpInBlocks := make(map[uint64]([]uint64))
	for i := float64(0); (float64(fpBlocksAvailable) - i) >= 1; i += 1.0 / float64(consts.StandardSubtimeDivisor) {
		// For each block, generate a set of sub-block fingerprints
		subBlockDFFTs := make([]*narrays.ComplexArrayN, 0)

		// Do this subframe by subframe
		for j := uint(0); (consts.StandardSamplesPerFrameHop * j) <= (consts.StandardFPBlockSamples - consts.StandardSampleSize); j++ {
			//fmt.Printf("Frame hop %v\n", j)
			sliceStart := uint(float64(i)*float64(consts.StandardFPBlockSamples))+j*consts.StandardSamplesPerFrameHop
			//fmt.Printf("Slicing: %v/%v, len %v\n", sliceStart, data.Len(), consts.StandardSampleSize)
			rawSlice, err := data.Slice1D(sliceStart, consts.StandardSampleSize)
			if (err != nil) {
				return nil, err
			}

			// Window this slice appropriately
			for ri, rv := range rawSlice.Elements {
				rawSlice.Elements[ri] = complex(real(rv) * HanningWindow(uint(ri), consts.StandardSampleSize), 0)
			}

			// DFFT it
			err = plan.DoDFFTForArray(rawSlice)
			if (err != nil) {
				return nil, err
			}

			// Good, stack it to the slice
			subBlockDFFTs = append(subBlockDFFTs, rawSlice)
		}

		// Join them
		joinedDFFTs, err := narrays.Join(subBlockDFFTs)

		if (err != nil) {
			return nil, err
		}

		// Get a fingerprint
		fpLimit := consts.StandardMaxSampleBlockFingerprints
		if (dedup) {
			fpLimit = consts.StandardMaxTrackBlockFingerprints
		}
		fp, err := DFFTToFingerprint(joinedDFFTs, uint(fpLimit))
		if (err != nil) {
			return nil, err
		}
		fpBlockMap[uint64(i * consts.StandardSubtimeDivisor)] = fp

		// Register it on the map denoting in which blocks a certain fingerprint has been seen
		for _, v := range fp {
			blockArr := fpInBlocks[v]
			if (blockArr == nil) {
				blockArr = make([]uint64, 0)
			}

			blockArr = append(blockArr, uint64(i * consts.StandardSubtimeDivisor))
			fpInBlocks[v] = blockArr
		}
	}

	//fmt.Println(fpInBlocks)

	// We now have a raw match map, which may contain a lot of duplicated, poorly matched fingerprints
	// If we so need and we have enough blocks for it, deduplicate it to only return fingerprints which are, well, better
	if (dedup && len(fpBlockMap) > 1) {
		// Calculate the maximum amount of total matches available
		maxMatch := uint(float64(len(fpBlockMap)) * consts.IntersongMaxMatch)
		if (maxMatch < consts.IntersongAcceptableBlocks) {
			maxMatch = consts.IntersongAcceptableBlocks
		} else if (maxMatch < 1) {
			maxMatch = 1 // Ensure that at least one matches
		}
		// Calculate block-specific decisivity ratio
		decisivityForFingerprint := make(map[uint64]float64)
		for k, v := range fpInBlocks {
			decisivity := 0.0 // By default, it is zero
			if len(v) > 1 {
				// Sort the slice in order
				sort.Slice(v, func(i, j int) bool {
					return v[i] < v[j]
				})
				decisivityTot := 0.0
				for ind := 1; ind < len(v); ind++ {
					decisivityTot += float64(v[ind] - v[ind-1])
				}

				// Decisivity is that divided by the amount of blocks minus blocks matched
				decisivity = decisivityTot / float64(len(fpBlockMap) - len(v))
			}

			decisivityForFingerprint[k] = decisivity
		}
		//fmt.Println(decisivityForFingerprint)
		for k, v := range fpBlockMap {
			newFPS := make([]uint64, 0)
			for _, ffp := range v {
				// Limit a max
				if uint(len(fpInBlocks[ffp])) <= maxMatch && decisivityForFingerprint[ffp] <= consts.DecisivityHighMark{
					// This is relatively unique, and decisive enough
					newFPS = append(newFPS, ffp)
				}
			}

			fpBlockMap[k] = newFPS
		}

	}

	//fmt.Println(fpBlockMap)

	return fpBlockMap, nil

}
