package fp_gen

import (
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"errors"
	"math"
	"sort"
	"fmt"
	"crypto/sha256"
	"encoding/binary"
	"bytes"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/consts"
)

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	DFFT array -> Fingerprint generation code and associated methods for a single block
 */

/*
	There is a large aḿount of bins in the raw DFFT array. We shall coalesce them to be a bit more manageable for analysis purposes.
	Do note that these are entirely separate from the bands - bands only affect the hash selection, not peak finding!

	We currently take 2048 samples, which at 10khz sample rate is roughly 4.88hz per bin

	- Ignore 0-20hz, low significance and hardly audible on its own
	- Coalesce range 20-1000hz into ~9.6hz bands
	- 1020hz -> 3020hz into ~4.88hz
	- 3000hz -> 5000hz into ~9.6hz
 */
var CoalescingDefs = []BinCoalescingDef{ {0, 4}, {2, 510}}

// As we want to coalesce some bins for their low or lacking meaning, define a structure for that
// Zero bins-to-merge means that these bins ought to be skipped entirely due to meaninglessness
type BinCoalescingDef struct {
	binsToMerge uint
	times uint
}

// A structure for a local maxima
type LocalMaxima struct {
	Power    float64
	Pos      narrays.XY
	FreqBand uint
	TimeBand uint
}

// A pair of points for hashing
type PointPair struct {
	a LocalMaxima
	b LocalMaxima
}

// fuzzFloat() fuzzes a given float to the specified precision
func fuzzFloat(fuzzfactor float64, val float64) float64 {
	return val - (math.Mod(val, fuzzfactor)) + (fuzzfactor / 2)
}

// findLocalPeaks() goes through a grid, calculates which points are local maxima, and returns a list of them. It also filters them according to the minimum proportional strength required, and fills appropriate bin information
func findLocalPeaks(orig *narrays.ComplexArrayN) ([]LocalMaxima) {
	//fmt.Printf("Finding local peaks for: %v\n", orig.N)
	maxima := make([]LocalMaxima, 0)
	freqDivisor := orig.N[1] / consts.StandardFreqBands
	if freqDivisor == 0 {
		freqDivisor = 1
	}
	timeDivisor := orig.N[0] / consts.StandardTimeBands
	if (timeDivisor == 0) {
		timeDivisor = 1
	}
	// First, find local maximas
	for y := uint(0); y < orig.N[0]; y++ {
		innerItemCheckLoop:
		for x := uint(0); x < orig.N[1]; x++ {
			rInd, _ := orig.RawIndex([]uint{y,x})
			val := orig.Elements[rInd]
			if (val == 0) {
				continue
			}
			dPowList := make([]float64, 0)
			for dy := -1; dy <= 1; dy++ {
				for dx := -1; dx <= 1; dx++ {
					var tx, ty int = int(x)+dx, int(y)+dy
					// If our coordinate is invalid, skip it
					if (tx < 0 || ty < 0 || uint(tx) >= orig.N[1] || uint(ty) >= orig.N[0]) {
						continue
					}
					// If we are in the center or a diagonal, which is not OK; just ignore this case
					if ((dx == 0 && dy == 0) || dx == dy || dx == (-1 * dy)) {
						continue
					}
					drInd, _ := orig.RawIndex([]uint{uint(ty),uint(tx)})
					dVal := orig.Elements[drInd]
					// If we have an invalid neighbor with a larger value, skip this entire point
					if real(dVal) >= real(val) {
						continue innerItemCheckLoop
					}

					dPowList = append(dPowList, real(dVal))
				}
			}
			var logPow float64
			if (len(dPowList) > 0) {
				// Calculate the average
				dSum := 0.0
				for _, v := range dPowList {
					dSum += v
				}

				dSum /= float64(len(dPowList))

				// We have now assured that our center value is inevitably same or higher than the average of neighboring values.
				// However, it is perhaps possible that our dSum is zero, in which case it'll inevitably meet our criteria
				if (dSum != 0) {
					logScale := math.Log10(real(val) / dSum)
					// It is reasonable to require a measurable power factor
					if (!math.IsNaN(logScale) && !math.IsInf(logScale, 0)) {
						if (logScale > consts.StandardMinimumRelativePower) {
							logPow = logScale
						} else {
							continue
						}
					}
				}
			}
			// All match? Good!
			// OK, this matches!
			maxima = append(maxima, LocalMaxima{logPow, narrays.XY{X: x, Y: y}, x / freqDivisor, y / timeDivisor})
		}
	}

	//fmt.Println(maxima)
	return maxima
}

// Maps peaks to a list of pairs of points which could be valid lines
func peaksToLineStarts(peaks *[]LocalMaxima, fingerprintLimit uint) []PointPair {
	// First, map per starting time point
	maximaMap := make(map[uint]([]LocalMaxima))

	for _, v := range *peaks {
		i := v.TimeBand

		if maximaMap[i] == nil {
			maximaMap[i] = make([]LocalMaxima, 0)
		}

		maximaMap[i] = append(maximaMap[i], v)
	}

	pairList := make([]PointPair, 0)

	for k1, v := range maximaMap {
		// Ok, this should fit easily.
		for k2 := k1 + 1 + consts.TimeBandDistMin; k2 < consts.StandardTimeBands && k2 <= k1 + 1 + consts.TimeBandDistMax; k2++ {

			// OK, we have defined our time bands' validity. Let's next check the points
			for _, p1 := range v {
				for _, p2 := range maximaMap[k2] {
					freqBandDist := math.Abs(float64(p1.FreqBand) - float64(p2.FreqBand))
					if (freqBandDist > consts.FreqBandMax || freqBandDist < consts.FreqBandMin) {
						continue // Not OK.
					}

					// OK, this seems fine-ish.
					pairList = append(pairList, PointPair{p1, p2})
				}
			}
		}
	}

	// Finally, sort the list by power of both fingerprints, and return only the strongest matches, preferring longer lines to shorter ones
	sort.Slice(pairList, func(i, j int) bool {
		iPow := fuzzFloat(consts.StandardPowerSortFuzz, pairList[i].a.Power + pairList[i].b.Power)
		jPow := fuzzFloat(consts.StandardPowerSortFuzz, pairList[j].a.Power + pairList[j].b.Power)

		return iPow >= jPow
	})

	if (uint(len(pairList)) > fingerprintLimit) {
		retList := make([]PointPair, int(fingerprintLimit))
		copy(retList, pairList[0:int(fingerprintLimit)])
		return retList
	}

	return pairList
}

 /*
	DFFTToFingerprint() takes a 2D array, of which the column width should be the exact sample size, and amount of rows can be as long as liked
 	Then it attempts to calculate fingerprints, and returns them, if available.

 	Do note that these fingerprints only concern this specific block! It may be useful to do some further contextual filtering, which ensures that no spurious fingerprints matching several disparate parts of a track are stored, confusing the recognition tools

 	Based partially on the idea proposed in "An Industrial Strength Audio Search Algorithm" by Avery Li-Chun Wang of Shazam
 */
func DFFTToFingerprint(dfftdata *narrays.ComplexArrayN, fingerprintLimit uint) ([]uint64, error) {
	if dfftdata == nil || len(dfftdata.N) != 2 || dfftdata.N[1] != consts.StandardSampleSize {
		return []uint64{}, errors.New("improperly shaped data, we need an exactly specified number of complex values per DFFT grid")
	}

	// Clone the provided DFFT array
	dfftClone, _ := dfftdata.Slice([]uint{})

	// First, normalize it to the range
	for i, v := range dfftClone.Elements {
		dfftClone.Elements[i] = v / consts.StandardSampleSize
	}

	// Then, calculate the size of the coalesced grid
	coalescTot := uint(0)
	for _, v := range CoalescingDefs {
		if (v.binsToMerge != 0) {
			coalescTot += v.times
		}

	}

	// Attempt to allocate a new grid
	coalescedGrid, _ := narrays.NewArrayN([]uint{dfftClone.N[0], coalescTot})

	var rawDfftIndex, coalescIndex uint = 0, 0

	// Pass 1 - calculate coalesced magnitudes, one at a time
	for times := uint(1); times < dfftClone.N[0]; times++ {
	for _, v := range CoalescingDefs {
		if (v.binsToMerge == 0) { // If we want to skip some bins..
			rawDfftIndex += v.times //.. then do so the given amount
		} else {
			for i := uint(1); i <= v.times; i++ {
				var magnitude float64 = 0.0

				for j := uint(0); j < v.binsToMerge; j++ {
					mReal := real(dfftClone.Elements[rawDfftIndex])
					mImg := imag(dfftClone.Elements[rawDfftIndex])
					magnitude += math.Sqrt((mReal*mReal) + (mImg*mImg))
					rawDfftIndex++
				}

				magnitude /= float64(v.binsToMerge)
				coalescedGrid.Elements[coalescIndex] = complex(magnitude,0 )
				coalescIndex++
			}
		}
	}
	}
	// Pass 2, determine and filter local peaks
	peaks := findLocalPeaks(coalescedGrid)
	//fmt.Printf("Found peaks: %v\n", peaks)

	var fps = make([]uint64, 0)
	// Pass 3, form lines and generate fingerprints
	pairs := peaksToLineStarts(&peaks, fingerprintLimit)
	//fmt.Println(pairs)

	foundFps := make(map[uint64]bool)

	// Generate hashes
	for _, pair := range pairs {
		// Our hash should be a hash of the following:
		// - Initial frequency band
		// - End frequency band
		// - Approximate time distance in bands
		hashStr := fmt.Sprintf("IFB%v:EBD%v:TD%v", pair.a.FreqBand, pair.b.FreqBand, uint(pair.b.TimeBand - pair.a.TimeBand))
		//fmt.Println(hashStr)
		hashData := sha256.Sum256([]byte(hashStr))

		var uintRes uint64
		err := binary.Read(bytes.NewReader(hashData[0:8]), binary.LittleEndian, &uintRes)
		if err != nil {
			return []uint64{}, err
		}
		foundFps[uintRes] = true
	}

	for fp, _ := range foundFps {
		fps = append(fps, fp)
	}

	sort.Slice(fps, func(i, j int) bool {
		return fps[i] < fps[j]
	})

	return fps, nil
}
