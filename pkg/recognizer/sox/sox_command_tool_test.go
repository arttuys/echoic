package sox

import (
	"testing"
	"os"
	"time"
	"fmt"
)

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Baseline SOX runner tests; mostly concerned with ensuring various invariants work
 */

 // Test that obviously improper configuration options are not accepted
 func TestNewSoxConfig(t *testing.T) {
	badConfigs := [](struct{a string; b string}){{"/bin/bash", ""}, {"/usr/tmp", "/usr/bin/sox"}}

	for _, v := range badConfigs {
		_, err := NewSoxConfig(v.a, v.b)
		if err == nil {
			t.Fatal("Did not error with an invalid setup!")
		}
	}
 }

 // Test that the array transform works somewhat as expected. This assumes that there is a valid Sox config
 func TestSoxTransform(t *testing.T) {
	 var soxConfig, _ = NewSoxConfig("/usr/bin/sox", os.TempDir())
	 soxIngestParam := BuildSoxDataParam("raw", 1000, "floating-point", 64, 1, LittleEndian, "-")
	 soxOutputParam := BuildSoxDataParam("raw", 4000, "floating-point", 64, 1, LittleEndian, "-")
	 soxCommand := soxConfig.BuildSoxCommand([]string{}, soxIngestParam, soxOutputParam, []string{})

	 testArr := make([]float64, 1000)
	 for i, _ := range testArr {
	 	testArr[i] = (float64(i) / 999.0)
	 }

	 transformedArr, err := TransformDataWithSoxToComplexArr(soxCommand, testArr, time.Second * 5)
	 if (err != nil) {
	 	fmt.Println(err)
	 	t.Fatal("Failed to complete a run!")
	 }
	 fmt.Println(*transformedArr)
 }

 // Test that failures are handled gracefully
 func TestSoxFailure(t *testing.T) {
	 var soxConfig, _ = NewSoxConfig("/usr/bin/sox", os.TempDir())
	 soxIngestParam := BuildSoxDataParam("raw", 1000, "floating-point", 64, 1, LittleEndian, "nada-not-existing")
	 soxOutputParam := BuildSoxDataParam("raw", 4000, "floating-point", 64, 1, LittleEndian, "-")
	 soxCommand := soxConfig.BuildSoxCommand([]string{}, soxIngestParam, soxOutputParam, []string{})

	 testArr := make([]float64, 1000)
	 for i, _ := range testArr {
		 testArr[i] = (float64(i) / 999.0)
	 }

	 transformedArr, err := TransformDataWithSoxToComplexArr(soxCommand, testArr, time.Second * 5)
	 if (err == nil) {
		 t.Fatal("Completed a run!")
	 }
	 if (transformedArr != nil) {
		 fmt.Println(*transformedArr)
	 }

 }