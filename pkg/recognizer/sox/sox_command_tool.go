package sox

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	SOX runner; SOX is used to convert audio data into a standardized format. This module contains various helper methods to interface with the SOX binary
 */

import (
	"os"
	"errors"
	"fmt"
	"strconv"
	"io"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"encoding/binary"
	"math"
	"os/exec"
	"time"
	"bytes"
)

// Configuration structure for a SOX runner; this is an opaque structure designed not to be tampered with after it has been created
type RunnerConfig struct {
	progPath     string
	tempFilePath string
}

type Endianess uint

const (
	DontCareEndian Endianess = iota
	LittleEndian   Endianess = iota
	BigEndian      Endianess = iota
)

// IsValidSoxConfig() checks if the configuration seems to be tentatively valid; it checks the accessibility of the Sox executable and temporary file directory.
// Observe that it is not a complete check - it doesn't check if it can actually write to a temporary directory, or if the SOX program launches properly
func (sxrc *RunnerConfig) IsValidSoxConfig() error {
	execStat, execErr := os.Stat(sxrc.progPath)
	if (execErr != nil) {
		return errors.New(fmt.Sprintf("unable to check the SOX binary for path '%v': %v", sxrc.progPath, execErr.Error()))
	}
	if (execStat.IsDir()) {
		return errors.New("provided path for the SOX executable seems to point to a directory")
	}
	tfpStat, tfpErr := os.Stat(sxrc.tempFilePath)
	if (tfpErr != nil) {
		return errors.New(fmt.Sprintf("unable to check the SOX temp dir for path '%v': %v", sxrc.tempFilePath, tfpErr.Error()))
	}
	if (!tfpStat.IsDir()) {
		return errors.New("provided path for temporary files seems not to correspond to a directory")
	}

	return nil
}

// BuildSoxCommand() builds a whole Sox command, ready to be used with the `exec` package
func (sxrc *RunnerConfig) BuildSoxCommand(globalOpts []string, input []string, output []string, ops []string) []string {
	base := []string{sxrc.progPath}
	base = append(base, globalOpts...)
	base = append(base, input...)
	base = append(base, output...)
	base = append(base, ops...)

	return base
}

// BuildSoxDataParam() builds a set of parameters. If no filename is provided, stdin/stdout is assumed and format is assumed to be raw
func BuildSoxDataParam(datatype string, sampleRate uint, encoding string, bits uint, channels uint, endianess Endianess, filename string) []string {
	base := []string{}
	if (datatype != "") {
		base = append(base, "-t", datatype)
	} else if (filename == "") {
		base = append(base, "-t", "raw")
	}
	if (encoding != "") {
		base = append(base, "-e", encoding)
	}
	if (bits > 0) {
		base = append(base, "-b", strconv.FormatUint(uint64(bits), 10))
	}
	if (channels > 0) {
		base = append(base, "-c", strconv.FormatUint(uint64(channels), 10))
	}
	if (sampleRate > 0) {
		base = append(base, "-r", strconv.FormatUint(uint64(sampleRate), 10))
	}
	if (endianess == LittleEndian) {
		base = append(base, "-L")
	} else if (endianess == BigEndian) {
		base = append(base, "-B")
	}
	if (filename != "") {
		base = append(base, filename)
	} else {
		base = append(base, "-")
	}
	return base
}

// TransformDataWithSoxToComplexArr is a standard mechanism for using Sox to ingest audio data; it runs a Sox command, and saves the provided data into a complex array ready for ingestion. It is possible to optionally provide an array datastream.
// Some commands may not return readable data, so an array may be nil EVEN IF there is no error. An error generally indicates a lack of data, but some residual data may remain
func TransformDataWithSoxToComplexArr(soxCmd []string, input []float64, timeout time.Duration) (*narrays.ComplexArrayN, error) {
	soxExec := exec.Command(soxCmd[0], soxCmd[1:]...)
	soxOutput, err := soxExec.StdoutPipe()
	soxInput, err2 := soxExec.StdinPipe()
	// Launch our command
	err3 := soxExec.Start()

	if (err != nil) {
		return nil, err
	} else if (err2 != nil) {
		return nil, err2
	} else if (err3 != nil) {
		return nil, err3
	}

	var foundWriteErr error = nil
	if (input != nil) {
		for _, v := range input {
			// Attempt to write to an input stream
			buf := new(bytes.Buffer)
			err := binary.Write(buf, binary.LittleEndian, math.Float64bits(v))
			if (err != nil) {
				foundWriteErr = err
				break
			}

			// Everything good? Write to the ingest buffer
			soxInput.Write(buf.Bytes())
		}
	}
	// Close stdin as we have completed our writing
	soxInput.Close()

	if (foundWriteErr != nil) {
		return nil, foundWriteErr
	}

	// Attempt to read, we cannot wait before reading as this will destroy resources
	arrRes := ReadFloat64StreamIntoComplexArray(soxOutput, false, timeout)

	// Wait for exit
	exitCode := soxExec.Wait()
	if (exitCode != nil) {
		return nil, exitCode
	}

	return arrRes, nil
}

// Reads a float input stream into an array. This function may return a null if no outout is returned
func ReadFloat64StreamIntoComplexArray(reader io.Reader, bigEndian bool, timeout time.Duration) *narrays.ComplexArrayN {
	var arr *narrays.ComplexArrayN
	var readUint64 uint64

	var byteOrder binary.ByteOrder = binary.LittleEndian
	if (bigEndian) {
		byteOrder = binary.BigEndian
	}

	boom := time.After(timeout)

	for {
		select {
		case <-boom:
			break
		default:
			// OK
		}

		err := binary.Read(reader, byteOrder, &readUint64)
		if err == nil {
			floatRes := math.Float64frombits(readUint64)
			if (arr == nil) {
				arr, _ = narrays.NewArrayN([]uint{1})
				arr.Elements[0] = complex(floatRes, 0)
			} else {
				arr.Append(floatRes, 1, func(data interface{}, _x uint) complex128 { return complex(data.(float64), 0) })
			}
		} else {
			break
		}
	}

	return arr
}

// Initializes a new SOX configuration blob; if the provided settings don't seem to be valid, does not return a configuration object
func NewSoxConfig(ProgramPath string, TempFilePath string) (*RunnerConfig, error) {
	conf := RunnerConfig{ProgramPath, TempFilePath}
	if err := conf.IsValidSoxConfig(); err != nil {
		return nil, err
	} else {
		return &conf, nil
	}
}
