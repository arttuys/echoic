package test_files

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Sample-based tests.

	NOTE: These make an assumption that a SOX binary can be found at `/usr/bin/sox` for now - if that is not so
	in your system, be sure to symlink a binary there!

	Also ensure that this test is run with the correct working directory, a.k.a the same directory this file is in!
 */

import (
	"testing"
	"fmt"
	"runtime"
	"path/filepath"
	"os"
	"gitlab.com/arttuys/echoic/pkg/recognizer/sox"
	"os/exec"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fftw/cgo_api"
	"math/rand"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"math"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/arttuys/echoic/pkg/model/db"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/consts"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/fp_gen"
	"sort"
	"time"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/channelizer"
)

// Base folder of audio sample files
var audioSampleBaseFolder = ""
// File count
var fileCount = uint(12)
// Minimal match scoring not to trigger a warning
var minimalMatchCount = 1.0
// What fingerprints have been globally found?
var globallyFoundFingerprints = make(map[uint64](map[uint]bool))
// Global SOX command, for audio processing purposes
var soxConfig, _ = sox.NewSoxConfig("/usr/bin/sox", os.TempDir())
// Storage for baseline audio data
var baselineAudioArrays = make([]*narrays.ComplexArrayN, fileCount)

// Initializes temporary files
func InitializeTempFiles() bool {
	_, path, _, _ := runtime.Caller(0)
	if (path == "") {
		panic("Unable to locate our current path!")
	} else {
		dir, _ := filepath.Split(path)
		// Load in via a relative path
		audioSampleBaseFolder, _ = filepath.Abs(filepath.Join(dir, "..", "..", "test", "sample_audio"))
		if (audioSampleBaseFolder == "") {
			panic("Unable to locate our sample audio folder!")
		}

		fmt.Printf("OK, found our sample audio folder at: '%v'\n", audioSampleBaseFolder)
	}
	fmt.Print("Checking if baseline source material exists..")

	for i := uint(1); i <= fileCount; i++ {
		name := filepath.Join(audioSampleBaseFolder, fmt.Sprintf("B%v.ogg", i))
		if fstat, err := os.Stat(name); err != nil || (!fstat.Mode().IsRegular()) {
			panic(fmt.Sprintf("\nProblem: %v was not found or is a special file!", name))
		}
		fmt.Print(".")
	}
	fmt.Println(" OK!")

	// Next check that we have everything
	_, err := os.Stat(filepath.Join(audioSampleBaseFolder, "TMPFILES.lock"))
	if os.IsNotExist(err) {
		fmt.Println("Tempfile marking lockfile not found, generating temporary files for tests. Please stand by, this may take a while...")
		return false; // Not existent, this is OK
	} else if (err != nil) {
		panic(fmt.Sprintf("Error while checking for existing temporary files: %v", err.Error()))
	}

	fmt.Println("Temporary files found, no generation needed")
	return true
}

// Generate rough test files to hasten tests so that raw data is already prepared
func generateTestFiles(soxConfig *sox.RunnerConfig) {
	fmt.Println("Generating test files..")
	for i := uint(1); i <= fileCount; i++ {
		inName := filepath.Join(audioSampleBaseFolder, fmt.Sprintf("B%v.ogg", i))
		outBaselineName := filepath.Join(audioSampleBaseFolder, fmt.Sprintf("R%v.raw", i))

		inParams := sox.BuildSoxDataParam("", 0, "", 0, 0, 0, inName)
		outParams := sox.BuildSoxDataParam("raw", consts.StandardSampleRate, "floating-point", 64, 1, sox.LittleEndian, outBaselineName)

		soxCommand := soxConfig.BuildSoxCommand([]string{}, inParams, outParams, []string{})
		if err := exec.Command(soxCommand[0], soxCommand[1:]...).Run(); err != nil {
			panic(fmt.Sprintf("Command failed: %v", err))
		}
	}

	// Good! Test files have been prepared
	if err := exec.Command("touch", filepath.Join(audioSampleBaseFolder, "TMPFILES.lock")).Run(); err != nil {
		fmt.Printf("Was unable to touch a lockfile; these tests may work, but without a lockfile, temporary files will be regenerated on next run: %v", err)
	}
}

// Loads data files to RAM for faster usage
func loadRawDatafiles(soxConfig *sox.RunnerConfig) {
	for i := uint(1); i <= fileCount; i++ {
		// Let's start by ingesting appropriate data
		fmt.Printf("Ingesting data for B%v to test..\n", i)
		fileName := filepath.Join(audioSampleBaseFolder, fmt.Sprintf("R%v.raw", i))
		// Let's first test that these are approximately good
		soxCommand := soxConfig.BuildSoxCommand([]string{}, sox.BuildSoxDataParam("raw", consts.StandardSampleRate, "floating-point", 64, 1, sox.LittleEndian, fileName), sox.BuildSoxDataParam("raw", consts.StandardSampleRate, "floating-point", 64, 1, sox.LittleEndian, ""), []string{})
		soxExec := exec.Command(soxCommand[0], soxCommand[1:]...)
		soxInputStream, err := soxExec.StdoutPipe()
		if (err != nil || soxExec.Start() != nil) {
			panic("Failed to either create a stream, or start SOX!")
		}
		// Save this result
		resArr := sox.ReadFloat64StreamIntoComplexArray(soxInputStream, false, time.Second * 60)
		baselineAudioArrays[i-1] = resArr


		// Once we have finished, validate our testing array
		validateArray(resArr)
		// Okay? Good! Let's next create a plan

	}
}

// Calculate fingerprints from an array
func calculateFingerprints(data *narrays.ComplexArrayN, plan *cgo_api.FFTWPlan, t *testing.T, dedup bool) map[uint64]([]uint64) {
	fprints, err := fp_gen.DataArrToFingerprintSet(data, plan, dedup)
	if (err != nil) {
		t.Fatal(fmt.Sprintf("Something went wrong with the fingerprints (%v)!", err))
	}

	return fprints
}

// Validate that a raw array is valid on a casual glance
func validateArray(resArr *narrays.ComplexArrayN) {
	if (resArr == nil) {
		panic("Something failed reading, no array returned!")
	} else if (len(resArr.Elements) < 10000*60) {
		// All samples are longer than a single minute. Fail if not!
		panic("Something is wrong, read file is less than a minute!")
	}
}

// Writes a debug file containing information on blocks
func writeDebugFile(fprints *map[uint64]([]uint64), fname string, t *testing.T) {
	file, ferr := os.OpenFile(filepath.Join(audioSampleBaseFolder, fmt.Sprintf("T%v.txt", fname)), os.O_RDWR|os.O_APPEND|os.O_CREATE|os.O_TRUNC, 0644)
	if (ferr == nil) {

		for i, v := range *fprints {
			fmt.Fprintf(file, "Block %v:\n", i)
			for _, v2 := range v {
				fmt.Fprintf(file, "\t<%v>\n", v2)
			}

		}
		file.Close()
	} else {
		t.Fatal(fmt.Sprintf("Failed to generate a file '%v', due to '%v'", fname, ferr))
	}
}

// Marks a fingerprint for globally found tracks
func markSongForFprint(song_id uint, fp uint64) {
	songRec := globallyFoundFingerprints[fp]
	if (songRec == nil) {
		baseMap := make(map[uint]bool)
		baseMap[song_id] = true
		globallyFoundFingerprints[fp] = baseMap
	} else {
		songRec[song_id] = true
	}
}

// Commits an offset sanity check; that is, taken snippets from some track, it should be able to do some matching to the original track
func offsetSanityCheck(id uint, baseTrack *narrays.ComplexArrayN, plan *cgo_api.FFTWPlan, baseFprints map[uint64]([]uint64), tx *testing.T) {

	// Build a reverse matching map; from fingerprint to blocks
	revMap := make(map[uint64]([]uint64))

	for i, v := range baseFprints {
		for _, v2 := range v {
			if (revMap[v2] == nil) {
				revMap[v2] = make([]uint64, 0)
			}

			revMap[v2] = append(revMap[v2], i)
		}
	}

	//fmt.Println(revMap)

	foundCorrectMatches := 0.0
	maxBlocks := uint64(baseTrack.Len() / consts.StandardFPBlockSamples)
	//fmt.Println(baseTrack)
	for i := 1; i < int(maxBlocks); i++ {

		sliceStart := uint(float64(i) * consts.StandardFPBlockSamples) - uint(float64(consts.StandardFPBlockSamples) * rand.Float64())
		approxBlock := uint64(sliceStart / consts.StandardDiscreteBlocksDivisor)
		fmt.Printf("Taking a random slice; pos %v/%v, len %v (roughshodly block %v)\n", sliceStart, baseTrack.Len(), consts.StandardFPBlockSamples, approxBlock)
		slicedArr, serr := baseTrack.Slice1D(sliceStart, consts.StandardFPBlockSamples)
		if serr != nil {
			tx.Fatal(fmt.Sprintf("Error while slicing: %v", serr))
		}

		//fmt.Println(slicedArr)
		dfftSlice, err := fp_gen.DataArrToFingerprintSet(slicedArr, plan, false)
		if (err != nil) {
			tx.Fatal(fmt.Sprintf("Something went wrong with the fingerprint calculations! (%v)", err))
		}

		//writeDebugFile(&dfftSlice, fmt.Sprintf("%vFNGRPRT-%v", id, i), tx)

		partFoundCorrectMatches := 0.0
		preciseMatchScore := 0.0
		var lopsidedMatchesCount uint
		var lopsidedMatchesKeys map[uint64]bool


		for _, dfftBlock := range dfftSlice {
			// Scale the scoring so that for to receive a full score, only full matches should be counted
			addOffset := 1.0 / float64(len(dfftBlock)) / float64(len(dfftSlice))
			lopsidedMatchesCount = 0
			lopsidedMatchesKeys = make(map[uint64]bool)
			lopsidedAvgDistances := make([]float64, 0)
			partialPreciseMatchScore := 0.0
			preciseMatchCount := 0
			for _, fp := range dfftBlock {
				originBlocks := revMap[fp]

				// No records? Skip
				if (originBlocks == nil) {
					continue
				}

				for _, originBlock := range originBlocks {
					// Check how the match is - is it precise or imprecise?
					if originBlock >= (approxBlock-1) && originBlock <= (approxBlock+1) {
						preciseMatchCount++
						partialPreciseMatchScore += addOffset
						preciseMatchScore += addOffset
						foundCorrectMatches += addOffset
						partFoundCorrectMatches += addOffset
					} else {
						lopsidedMatchesCount++
						lopsidedMatchesKeys[originBlock] = true
						lopsidedAvgDistances = append(lopsidedAvgDistances, math.Abs(float64(originBlock) - float64(approxBlock)))
					}
				}
			}

			// Add it so that penalty assessed is dependent on how far the keys spread. If the spread is excessively large
			if (lopsidedMatchesCount > 0 && len(lopsidedMatchesKeys) > 0) {

				// Add mismatching keys to a slice
				mismatchSlice := make([]uint64, 0)
				for k, _ := range lopsidedMatchesKeys {
					mismatchSlice = append(mismatchSlice, k)
				}

				// Sort this slice
				sort.Slice(mismatchSlice, func(i, j int) bool {
					return mismatchSlice[i] < mismatchSlice[j]
				})

				// Calculate the average distance from the proper key
				var avgDistSum = 0.0
				for _, v := range lopsidedAvgDistances {
					avgDistSum += v
				}

				avgDistSum /= float64(len(lopsidedAvgDistances))
				// Calculate the standard deviation
				stdDevSum := 0.0
				for _, v := range lopsidedAvgDistances {
					stdDevSum += math.Pow(v - avgDistSum,2)
				}

				stdDevSum /= float64(len(lopsidedAvgDistances))
				stdDevSum = math.Pow(stdDevSum, 0.5)

				spreadFactor := float64(0.5) / (0.5 + float64(len(lopsidedMatchesKeys)))
				distFactor := float64(1.5) / (1.5 + (avgDistSum / float64(baseTrack.Len() / consts.StandardFPBlockSamples)) * 50)
				score := (float64(lopsidedMatchesCount) * addOffset) * spreadFactor * distFactor

				foundCorrectMatches += score
				partFoundCorrectMatches += score

				fmt.Printf("\t\tLopsided unique match blocks %v, precise matches %v, avg lopsided distance %v, standard deviation %v\n", len(lopsidedMatchesKeys), preciseMatchCount, avgDistSum, stdDevSum)
				fmt.Printf("\t\t\tMismatched keys for block %v: %v\n", approxBlock, mismatchSlice)
				if (score > 0) {
					fmt.Printf("\t\tPreicse-to-lopsided factor: %v\n", partialPreciseMatchScore / score)
				}
			}
		}

		fmt.Printf("\tTotal score %v, scored %v from precise matches, and scored %v from lopsided ones\n", partFoundCorrectMatches, preciseMatchScore, partFoundCorrectMatches-preciseMatchScore)
	}



	if foundCorrectMatches < minimalMatchCount {
		fmt.Printf("WARNING: Very few valid matches, unable to match properly; got %v, required %v!\n", foundCorrectMatches, minimalMatchCount)
	} else {
		fmt.Printf("Satisfied, found %v/%v\n", foundCorrectMatches, minimalMatchCount)
	}
}

func checkCrossmatchSanity(t *testing.T) {
	fmt.Println("Crosschecking fingerprints for excessive multi-song matching...")
	matchLens := make(map[uint64]uint)
	foundMax := 0
	for i, v := range globallyFoundFingerprints {
		matchLens[i] = uint(len(v))
		if len(v) > foundMax {
			foundMax = len(v)
		}
	}
	fmt.Printf("There were a total of %v fingerprints across %v test files, with fingerprints resolving to at max %v tracks\n", len(matchLens), fileCount, foundMax)
	fmt.Println("Fingerprint counts:")

	for i := foundMax; i >= 1; i-- {
		count := 0
		for _, v := range matchLens {
			if v == uint(i) {
				count++
			}
		}

		fmt.Printf("\t%v matched to %v tracks\n", count, i)
	}
}

func TestMain(m *testing.M) {
	if !InitializeTempFiles() {
		generateTestFiles(soxConfig)
	}

	loadRawDatafiles(soxConfig)
	fmt.Println("Pre-test preparations done, commencing tests..")
	os.Exit(m.Run())
}

// Tests the SQL data model and its basic workings
func TestSQLDataModel(t *testing.T) {
	// First, initialize a SQLite db
	filename := filepath.Join(audioSampleBaseFolder, "testdb.sqlite3")
	if stat, err := os.Stat(filename); stat == nil  {
		if os.IsNotExist(err) {
			// All is fine, it doesn't exist
		} else {
			t.Fatal("Something went wrong verifying the existence of the stat file!")
		}
	} else {
		os.Remove(filename)
	}

	sqldb, err := db.OpenAndPrepareSQLiteDB(filename)
	if (err != nil) {
		t.Fatal(fmt.Sprintf("Error while opening the SQL DB! (%v)", err))
	}

	// Our DB should be empty now. Test that the query returns no results
	resArr, err := sqldb.HashListToMLCPoints(0.3, []uint64{3565252,9913789,18391})
	if (err != nil || resArr == nil || len(resArr) > 0) {
		t.Fatal(fmt.Sprintf("Returned a nonempty result or an error! (%v, %v)", resArr, err))
	}
	sqldb.Close()
}

// Tests that the correct track seems to be recognized by the SQL DB mechanism
func sqlDbSanityCheck(i uint, sqlIndx uint64, baseTrack *narrays.ComplexArrayN, conn *db.FPSQLConn, t *testing.T) {
	fmt.Printf("Checking on track %v (SQL index %v)\n", i, sqlIndx)
	maximumOffset :=  int(baseTrack.Len()) - int(consts.StandardFPRMaxSamplesIngested)
	if (maximumOffset < 0) {
		maximumOffset = 0
	}

	offset := 0
	selectedLength := int(consts.StandardFPRMaxSamplesIngested)
	if (int(baseTrack.Len()) < selectedLength) {
		selectedLength = int(baseTrack.Len())
	}

	if (maximumOffset > 0) {
		offset = int(float64(maximumOffset) * rand.Float64())
		// Align to block edges
		//offset -= (offset % consts.StandardFPBlockSamples)
	}

	sliceStart := uint(offset)
	slicedArr, serr := baseTrack.Slice1D(sliceStart, uint(selectedLength))
	approxBlock := uint64(sliceStart / consts.StandardDiscreteBlocksDivisor)
	fmt.Printf("\tTaking a random slice; pos %v/%v, len %v (roughshodly block %v)\n", sliceStart, baseTrack.Len(), slicedArr.Len(), approxBlock)

	if (slicedArr == nil || serr != nil) {
		t.Fatal(fmt.Sprintf("Failed to take a slice: %v\n", serr))
	}

	// Ingest all data in.
	returnCh := make(chan channelizer.FPPReturnSignal, 5)
	fpProc, err := channelizer.NewStandardFPProcessor(conn, soxConfig, consts.StandardSampleRate, returnCh)
	if (err != nil) {
		t.Fatal(fmt.Sprintf("Failed to start a processor: %v\n", err))
	}

	boom := time.After(60 * time.Second)
	abnormalityFail := false
	fmt.Println("Ingesting data...")
	for _, v := range slicedArr.Elements {
		select {
			case message:=<-returnCh:
				//
				switch (message.ResultCode) {
				case channelizer.FPPAbnormalityDetected:
					fmt.Println("Abnormality detected, resuming with caveats..")
					abnormalityFail = true
				case channelizer.FPPBufferOverrunReqConclusion:
					fmt.Println("Buffer overran, we should stop now...")
					fallthrough
				case channelizer.FPPShouldRequestConclusion:
					fmt.Println("Requesting conclusion as per recognizer, skipping to buffer flush..")
					break
				default:
					t.Fatal("Unexpected signal! Something went wrong!")
				}
			default:
				// OK
		}

		select {
			case fpProc.RawAudioIngestChannel <- real(v):
				// OK
			case <-boom:
				if (abnormalityFail) {
					fmt.Println("Soft fail, abnormality was informed and no more information was sent")
					break
				} else {
					t.Fatal("Timed out without any information!")
				}
		}
	}

	// Wait until the raw elements have been processed
	fmt.Println("Waiting for the buffer to flush...")
	time.Sleep(5 * time.Second)

	fmt.Println("Concluding...")
	// Send a process conclusion message
	select {
		case fpProc.ControlCh<-channelizer.FPConcludeProcessing:
			// OK!
		case <-boom:
			t.Fatal("Timed out on concluding!")
	}

	canExit := false

	for !canExit {
		select {
		case res := <-returnCh:
			switch (res.ResultCode) {
			case channelizer.FPPTimeoutExit:
				fmt.Println("... exited, timeout")
				canExit = true
			case channelizer.FPPErrorExit:
				fmt.Println("... exited, error")
				canExit = true
			case channelizer.FPPResultExit:
				fmt.Println("... exited, concluded normally. Matches:")
				fmt.Println(res.Matches)
				canExit = true
			case channelizer.FPPAbnormalityDetected:
				fmt.Println("... abnormality detected!")
				abnormalityFail = true
			case channelizer.FPPBufferOverrunReqConclusion:
				fmt.Println("... over sample limit")
				fallthrough
			case channelizer.FPPShouldRequestConclusion:
				fmt.Println("... found strong score, stopping prematurely")
			}
		case <-boom:
			if (abnormalityFail) {
				fmt.Println("Soft fail, abnormality was informed and no more information arrived")
			} else {
				t.Fatal("Timed out without any information!")
			}
		}

	}

}

// Tests basic sample stream functionality, and that they seem to return more or less sane results.
// This first generates a set of fingerprints, and then commits both single- and multi-track tests
func TestSampleStream(t *testing.T) {

	filename := filepath.Join(audioSampleBaseFolder, "sample-streams.sqlite3")
	if stat, err := os.Stat(filename); stat == nil  {
		if os.IsNotExist(err) {
			// All is fine, it doesn't exist
		} else {
			t.Fatal("Something went wrong verifying the existence of the stat file for the sample stream database!")
		}
	} else {
		os.Remove(filename)
	}

	sqldb, err := db.OpenAndPrepareSQLiteDB(filename)
	if (err != nil) {
		t.Fatal(fmt.Sprintf("Error while opening the SQL DB! (%v)", err))
	}

	defer sqldb.Close()

	sqlIndexMap := make(map[uint]uint64)

	// OK! Let's run subtests.
	for i := uint(1); i <= fileCount; i++ {
		fmt.Printf("Testing fingerprints for B%v\n", i)
		resArr := baselineAudioArrays[i-1]
		// Okay? Good! Let's next create a plan
		plan, err := cgo_api.NewFFTWPlan([]uint{consts.StandardSampleSize}, true)
		if (err != nil) {
			t.Fatal("Something went wrong creating a plan!")
		}
		defer plan.Close()

		fmt.Println("\tGenerating and writing baseline fingerprints to a debug file..")
		// Calculate our basic fingerprints
		fprints := calculateFingerprints(resArr, plan, t, true)
		// Ingest our fingerprint data to the song marker
		for _, block := range fprints {
			for _, fp := range block {
				markSongForFprint(i, fp)
			}
		}
		// Write a baseline file
		writeDebugFile(&fprints, fmt.Sprintf("%vFNGR", i), t)

		// Attempt to insert this into the SQL DB
		indx, err := sqldb.InsertNewTrack(fmt.Sprintf("Test Track %v", i), "Me", fprints)
		if (err != nil) {
			t.Fatal(fmt.Sprintf("Failed to insert fingerprints into database (%v)", err))
		} else {
			fmt.Printf("\t\tInserted fingerprints into DB with index %v\n", indx)
			sqlIndexMap[i] = indx
		}

		// Test that it is found
		found, trackName, trackArtist, err := sqldb.FindTrackForID(indx)
		if (found != true) {
			t.Fatal("Did not find a track we just inserted!")
		} else if (err != nil) {
			t.Fatal(fmt.Sprintf("Error: %v", err))
		} else {
			fmt.Println(trackName)
			fmt.Println(trackArtist)

			if (trackName != fmt.Sprintf("Test Track %v", i) && trackArtist != "Me") {
				t.Fatal("Invalid data returned!")
			}
		}

		// Do an offset sanity check
		fmt.Println("Doing a sanity check for offset files, single track")
		offsetSanityCheck(i, resArr, plan, fprints, t)

	}

	// Next, SQL sanity tests
	fmt.Println("Running SQL tests..")
	for i := uint(1); i <= fileCount; i++ {
		resArr := baselineAudioArrays[i-1]
		sqlDbSanityCheck(i, sqlIndexMap[i], resArr, sqldb, t)
	}

	// Do cross-track matching and check how fingerprints go there
	checkCrossmatchSanity(t)
}
