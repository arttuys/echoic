package main

/*
 	Echoic - an interactive music identification tool

	Copyright (C) 2018 Arttu Ylä-Sahra

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Command-line interface for Echoic; for now, two basic functionalities are supported:
		- Ingestion; when a list of tracks with associated data in a simple format is given,
		- Recognition; recognizing a song from a given sample file

 */

import (
	"flag"
	"gitlab.com/arttuys/echoic/pkg/model/db"
	"log"
	"os"
	"bufio"
	"strings"
	"fmt"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fftw/cgo_api"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/consts"
	"gitlab.com/arttuys/echoic/pkg/recognizer/sox"
	"gitlab.com/arttuys/echoic/pkg/recognizer/narrays"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/fp_gen"
	"gitlab.com/arttuys/echoic/pkg/recognizer/fingerprints/channelizer"
	"time"
)

// How long we should wait for the recognizer to get its results?
var timeoutForRecognizer = 15

// Simple track structure
type SimpleTrack struct {
	Filename string
	Track string
	Artist string
}

// Found track data, as measured from fingerprinting
type FoundTrack struct {
	TrackID uint64
	TrackName string
	TrackArtist string
	TrackScore uint
}

// Baseline SOX command
var soxConfig, _ = sox.NewSoxConfig("/usr/bin/sox", os.TempDir())

// Returns the appropriate Sox command to extract a file into standard output, suitable for shoehorning into a complex array
func getSoxCommandForFile(inName string) []string {
	inParams := sox.BuildSoxDataParam("", 0, "", 0, 0, 0, inName)
	outParams := sox.BuildSoxDataParam("raw", consts.StandardSampleRate, "floating-point", 64, 1, sox.LittleEndian, "")

	soxCommand := soxConfig.BuildSoxCommand([]string{}, inParams, outParams, []string{})

	return soxCommand
}

// Takes a filename, and returns a standard-format stream if possible
func fileToArray(filename string) *narrays.ComplexArrayN {
	narray, err := sox.TransformDataWithSoxToComplexArr(getSoxCommandForFile(filename), []float64{}, consts.StandardFPRMidstepTimeout)
	if (err != nil) {
		log.Println(err)
		log.Println("Unreadable file, returning without data")
		return nil
	} else {
		return narray
	}
}

func retrieveFingerprinterResults(sqldb *db.FPSQLConn, boom <-chan time.Time, returnCh chan channelizer.FPPReturnSignal, fpProc *channelizer.FPProcessor, failedAbnormally bool) {
	abnormalityFail := failedAbnormally
	log.Printf("\tWaiting for %v seconds for the recognizer to flush its buffers and process data\n", timeoutForRecognizer)
	time.Sleep(time.Duration(timeoutForRecognizer) * time.Second)
	log.Println("\tAsking for results...")
	// Send a process conclusion message
	select {
	case fpProc.ControlCh<-channelizer.FPConcludeProcessing:
		// OK!
	case <-boom:
		log.Fatal("\tTimed out on retrieving results!")
	}

	canExit := false

	for !canExit {
		select {
		case res := <-returnCh:
			switch (res.ResultCode) {
			case channelizer.FPPTimeoutExit:
				log.Println("\tTimed out! No results available!")
				canExit = true
			case channelizer.FPPErrorExit:
				log.Println("\tStopped due to an error! No results available!")
				canExit = true
			case channelizer.FPPResultExit:
				log.Println("\tResults:")
				// We have records!
				trackList := make([]FoundTrack, 0, len(res.Matches.Matches))
				for _, v := range res.Matches.Matches {
					newTrack := FoundTrack{}
					newTrack.TrackID = v.ID
					newTrack.TrackScore = v.Score
					// Attempt to retrieve via SQL
					found, name, artist, err := sqldb.FindTrackForID(v.ID)
					if (!found) {
						newTrack.TrackName = "<NOT FOUND>"
						newTrack.TrackArtist = "<UNKNOWN>"
					} else if (err != nil) {
						newTrack.TrackName = fmt.Sprintf("<ERROR: %v>", err)
						newTrack.TrackArtist = "<UNKNOWN>"
					} else {
						newTrack.TrackName = name
						newTrack.TrackArtist = artist
					}

					trackList = append(trackList, newTrack)
				}
				log.Println(" ------------------ ")
				if (len(trackList) == 0) {
					log.Println("\t\tNo results were matched. This sample may be excessively noisy, or simply not found in the database. Try again?")
				} else {
					for _, v := range trackList {
						log.Printf("\t\tTrack I.D %v, score %v/100: '%v' by '%v'\n", v.TrackID, v.TrackScore, v.TrackName, v.TrackArtist)
					}
				}
				canExit = true
			case channelizer.FPPAbnormalityDetected:
				log.Println("\tRecognizer reported abnormality. This may indicate a problem, as this should not ordinarily happen.")
				abnormalityFail = true
			case channelizer.FPPBufferOverrunReqConclusion:
				log.Println("\tRecognizer reported that it has ran over the sample limit, and will not accept more data. It is irrelevant at this part of the process")
			case channelizer.FPPShouldRequestConclusion:
				log.Println("\tRecognizer reported that it has results. Request has been done, and waiting for results now.")
			}
		case <-boom:
			if (abnormalityFail) {
				log.Fatal("\tFailed due to an apparent crash; no information returned")
			} else {
				log.Fatal("\tTimed out. This may be an indication of a problem, or simply that no match was found during an extraordinarily long and slowly ingested sample")
			}
		}

	}
}

// Recognize; take a simple file, and read it from an array
func recognize(sqldb *db.FPSQLConn, tailArgs []string) {
	// First, check our params and attempt to open the file
	if len(tailArgs) != 1 {
		log.Fatal("Please provide a filename to read, cannot continue without")
	}

	log.Printf("Reading track '%v", tailArgs[0])
	// Attempt to read this track
	track := fileToArray(tailArgs[0])

	if (track == nil) {
		log.Fatal("\tUnable to read track, cannot attempt to recognize")
	}

	// Now. Initialize a channelized fingerprinter
	// Ingest all data in.
	returnCh := make(chan channelizer.FPPReturnSignal, 5)
	fpProc, err := channelizer.NewStandardFPProcessor(sqldb, soxConfig, consts.StandardSampleRate, returnCh)
	if (err != nil) {
		log.Fatal(fmt.Sprintf("Failed to start a processor: %v\n", err))
	}

	// This is quite verbatim
	boom := time.After((consts.StandardFPRMLCTimeout - 10) * time.Second)
	abnormalityFail := false
	log.Println("\tIngesting data into the recognizer, this may take a while...")

	// For each element..
	for _, v := range track.Elements {
		// First, check for possible return messages
		select {
		case message:=<-returnCh:
			//
			switch (message.ResultCode) {
			case channelizer.FPPAbnormalityDetected:
				log.Println("\tAbnormality detected, resuming with the caveat that results may not be accurate or complete..")
				abnormalityFail = true
			case channelizer.FPPBufferOverrunReqConclusion:
				log.Println("\tBuffer overrun, no more data accepted, stopping prematurely")
				break
			case channelizer.FPPShouldRequestConclusion:
				log.Println("\tStrong match found, stopping prematurely")
				break
			default:
				log.Fatal("\tUnexpected signal! Something went wrong!")
			}
		default:
			// OK
		}

		// Then, ingest data
		select {
		case fpProc.RawAudioIngestChannel <- real(v):
			// OK
		case <-boom:
			if (abnormalityFail) {
				log.Fatal("\tFailed due to an apparent crash; no information returned")
			} else {
				log.Fatal("\tTimed out. This may be an indication of a problem, or simply that no match was found during an extraordinarily long and slowly ingested sample")
			}
		}
	}

	// Our file-specific part is done.
	retrieveFingerprinterResults(sqldb, boom, returnCh, fpProc, abnormalityFail)

}

// Adds a simple track to the database. Takes a track specification, reads the data from a file, fingerprints it, and inserts it
func addTrack(sqldb *db.FPSQLConn, plan *cgo_api.FFTWPlan, trackData SimpleTrack) {
	// First, attempt to take the file
	narray := fileToArray(trackData.Filename)
	if (narray == nil) {
		log.Println("\t\tNo data returned or available, skipping to next track")
		return
	}
	log.Printf("\t\tRead track, %v samples", narray.Len())

	fprints, err := fp_gen.DataArrToFingerprintSet(narray, plan, true)
	if (err != nil) {
		log.Printf("\t\tUnable to generate fingerprints due to '%v', skipping", err)
		return
	}

	indx, err := sqldb.InsertNewTrack(trackData.Track, trackData.Artist, fprints)
	if (err != nil) {
		log.Printf("\t\tFailed to insert due to error '%v'", err)
	} else {
		log.Printf("\t\tInserted fingerprints into DB with index %v", indx)
	}
}

// Ingestion; read a list of files, and add them to the database by fingerprinting them and ingesting those fingerprints into the DB.
// This function reads a list of file specifications from a text file
//		filename|track|artist
//		# Hashes indicate comments
func ingest(sqldb *db.FPSQLConn, tailArgs []string) {
	// First, check our params and attempt to open the file
	if len(tailArgs) != 1 {
		log.Fatal("Please provide a filename to read, cannot continue without")
	}

	file, err := os.Open(tailArgs[0])
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	// Next, preparse data into a list. Prepare some space beforehand
	list := make([]SimpleTrack, 0, 10)

	log.Println("Reading file specifications..")
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		text := scanner.Text()
		if (len(strings.TrimSpace(text)) == 0 || strings.HasPrefix(text, "#")) {
			continue
		}

		// Attempt to split
		data := strings.Split(text, "|")
		if (len(data) != 3) {
			log.Fatal(fmt.Sprintf("\tMalformed line '%v', not splittable into a correct length", text))
		}

		// Check if the file is readable
		if _, err := os.Stat(data[0]); err != nil {
			log.Fatal(fmt.Sprintf("\tFile nonexistent or unreadable: %v", data[0]))
		}

		log.Printf("File %v, track %v from %v", data[0], data[1], data[2])
		list = append(list, SimpleTrack{data[0], data[1], data[2]})
	}

	if err := scanner.Err(); err != nil {
		log.Println(err)
		log.Fatal("Error with scanner, exiting prematurely.")
	}

	//////////////////////////////////
	log.Printf("We are ready to begin now, %v files.", len(list))

	// Initialize plan
	plan, err := cgo_api.NewFFTWPlan([]uint{consts.StandardSampleSize}, true)
	if (err != nil) {
		log.Fatal("Something went wrong creating a plan!")
	}
	defer plan.Close()

	for _, v := range list {
		log.Printf("\tIngesting %v, track %v, artist %v", v.Filename, v.Track, v.Artist)
		addTrack(sqldb, plan, v)
	}
}

func main() {
	dbPtr := flag.String("db", "db.sqlite", "database to use")
	cmdPtr := flag.String("action", "", "action (ingest/recognize)")
	flag.IntVar(&timeoutForRecognizer,"timeout", 15, "how long to wait for the recognizer to flush")
	flag.Parse()

	// Open the DB
	sqldb, err := db.OpenAndPrepareSQLiteDB(*dbPtr)
	if (err != nil) {
		log.Fatal("Was unable to open the database")
	}

	defer sqldb.Close()

	switch (*cmdPtr) {
	case "recognize":
		recognize(sqldb, flag.Args())
	case "ingest":
		ingest(sqldb, flag.Args())
	default:
		flag.Usage()
		log.Fatal("No valid action supplied, exiting")
	}
}
