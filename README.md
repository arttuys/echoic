# Echoic

## An interactive music recognition tool

We've all experienced that situation. You hear a tune.. and you are sure you've heard it sometime before. Perhaps you even have it in your own library. But what _is it?_

This tool is intended to remedy that, at least for one's personal music libraries. A tidy summer project, if one will; it is modestly functional and can do some recognition work, although some improvements are still definitely doable to at least curb hilarious false positives that sometimes occur.

It operates by hashing and later temporally correlating pairs of points, drawing on the idea described by _"An Industrial Strength Audio Search Algorithm"_ by Avery Li-Chun Wang. The program is highly tunable, although it has proven to be difficult to make it work absolutely reliably - so that's still work in progress.

# Requirements

For the time being, you need:

- a modern **Golang** installation, preferably 1.10 as tested by the main dev
- **FFTW** (Fastest Fourier Transform in the West) installed with necessary development dependencies (possibly **OpenMP** as well)
- **SOX** (Sound eXchange) as a binary executable, available for programs to execute. Development/library dependencies are not required

# Execution

`go run cmd/echoic_db/main.go` in the main project directory. This is the current tool, which can both ingest and recognize songs from a database. There is an auxillary tool in the `aux` folder for generating file lists for the ingestion. For documentation, see the source code.

# Testing

There are several testing files available alongside the actual modules. Be sure to observe that the automated recognition tests require that the working directory is correct - see `pkg/test_files/samples_test.go` for more information.

# License

Echoic itself is licensed under GNU AGPL v3 or later at your choice. Any dependencies are licensed with their own respective licenses.